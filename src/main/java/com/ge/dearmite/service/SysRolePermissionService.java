package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysRolePermission ;
import com.common.exception.*;
import utils.PageModel;

/**
* 角色权限
*  Created by autoGen Tools 
*/

public interface SysRolePermissionService  {

	 /**
	     * delete 角色权限
	     * @param rolepermissionid PKID
	     * @throws MyException
	     */
	 public void deleteSysRolePermissionByPK(java.lang.Integer rolepermissionid) throws MyException ;
	 
	  /**
	     * add 角色权限
	     * @param  sysRolePermission 角色权限
	     * @throws MyException
	     */
	  void addSysRolePermission (SysRolePermission  sysRolePermission)  throws MyException;
	  
	   /**
	     * edit 角色权限
	     * @param sysRolePermission 角色权限
	     * @throws MyException
	     */
	  void updateSysRolePermission (SysRolePermission   sysRolePermission)  throws MyException;
	    /**
	     * get 角色权限
	     * @param rolepermissionid  角色权限 PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysRolePermission getSysRolePermissionByPK(java.lang.Integer rolepermissionid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysRolePermission> querySysRolePermissionBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}