package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmUser ;
import com.common.exception.*;
import utils.PageModel;

/**
* UM用户表
*  Created by autoGen Tools 
*/

public interface UmUserService  {

	 /**
	     * delete UM用户表
	     * @param userCode PKID
	     * @throws MyException
	     */
	 public void deleteUmUserByPK(java.lang.String userCode) throws MyException ;
	 
	  /**
	     * add UM用户表
	     * @param  umUser UM用户表
	     * @throws MyException
	     */
	  void addUmUser (UmUser  umUser)  throws MyException;
	  
	   /**
	     * edit UM用户表
	     * @param umUser UM用户表
	     * @throws MyException
	     */
	  void updateUmUser (UmUser   umUser)  throws MyException;
	    /**
	     * get UM用户表
	     * @param userCode  UM用户表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmUser getUmUserByPK(java.lang.String userCode) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmUser> queryUmUserBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}