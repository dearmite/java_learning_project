package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmRole ;
import com.common.exception.*;
import utils.PageModel;

/**
* 角色表
*  Created by autoGen Tools 
*/

public interface UmRoleService  {

	 /**
	     * delete 角色表
	     * @param roleCode PKID
	     * @throws MyException
	     */
	 public void deleteUmRoleByPK(java.lang.String roleCode) throws MyException ;
	 
	  /**
	     * add 角色表
	     * @param  umRole 角色表
	     * @throws MyException
	     */
	  void addUmRole (UmRole  umRole)  throws MyException;
	  
	   /**
	     * edit 角色表
	     * @param umRole 角色表
	     * @throws MyException
	     */
	  void updateUmRole (UmRole   umRole)  throws MyException;
	    /**
	     * get 角色表
	     * @param roleCode  角色表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmRole getUmRoleByPK(java.lang.String roleCode) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmRole> queryUmRoleBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}