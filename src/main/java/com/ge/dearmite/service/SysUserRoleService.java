package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysUserRole ;
import com.common.exception.*;
import utils.PageModel;

/**
* 用户角色
*  Created by autoGen Tools 
*/

public interface SysUserRoleService  {

	 /**
	     * delete 用户角色
	     * @param userroleid PKID
	     * @throws MyException
	     */
	 public void deleteSysUserRoleByPK(java.lang.Integer userroleid) throws MyException ;
	 
	  /**
	     * add 用户角色
	     * @param  sysUserRole 用户角色
	     * @throws MyException
	     */
	  void addSysUserRole (SysUserRole  sysUserRole)  throws MyException;
	  
	   /**
	     * edit 用户角色
	     * @param sysUserRole 用户角色
	     * @throws MyException
	     */
	  void updateSysUserRole (SysUserRole   sysUserRole)  throws MyException;
	    /**
	     * get 用户角色
	     * @param userroleid  用户角色 PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysUserRole getSysUserRoleByPK(java.lang.Integer userroleid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysUserRole> querySysUserRoleBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}