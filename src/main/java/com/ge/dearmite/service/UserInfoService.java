package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UserInfo ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UserInfoService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	 public void deleteUserInfoByPK(java.lang.Integer id) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  userInfo InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addUserInfo (UserInfo  userInfo)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param userInfo InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateUserInfo (UserInfo   userInfo)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param id  InnoDB free: 11264 kB PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public UserInfo getUserInfoByPK(java.lang.Integer id) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UserInfo> queryUserInfoBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}