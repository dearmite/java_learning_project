package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmSystemType ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UmSystemTypeService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param sysTypeCode PKID
	     * @throws MyException
	     */
	 public void deleteUmSystemTypeByPK(java.lang.String sysTypeCode) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  umSystemType InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addUmSystemType (UmSystemType  umSystemType)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param umSystemType InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateUmSystemType (UmSystemType   umSystemType)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param sysTypeCode  InnoDB free: 11264 kB PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmSystemType getUmSystemTypeByPK(java.lang.String sysTypeCode) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmSystemType> queryUmSystemTypeBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}