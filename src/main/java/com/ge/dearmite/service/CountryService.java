package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . Country ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface CountryService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	 public void deleteCountryByPK(java.lang.Integer id) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  country InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addCountry (Country  country)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param country InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateCountry (Country   country)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param id  InnoDB free: 11264 kB PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public Country getCountryByPK(java.lang.Integer id) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<Country> queryCountryBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}