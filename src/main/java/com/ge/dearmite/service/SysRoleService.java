package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysRole ;
import com.common.exception.*;
import utils.PageModel;

/**
* sys_role
*  Created by autoGen Tools 
*/

public interface SysRoleService  {

	 /**
	     * delete sys_role
	     * @param roleid PKID
	     * @throws MyException
	     */
	 public void deleteSysRoleByPK(java.lang.Integer roleid) throws MyException ;
	 
	  /**
	     * add sys_role
	     * @param  sysRole sys_role
	     * @throws MyException
	     */
	  void addSysRole (SysRole  sysRole)  throws MyException;
	  
	   /**
	     * edit sys_role
	     * @param sysRole sys_role
	     * @throws MyException
	     */
	  void updateSysRole (SysRole   sysRole)  throws MyException;
	    /**
	     * get sys_role
	     * @param roleid  sys_role PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysRole getSysRoleByPK(java.lang.Integer roleid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysRole> querySysRoleBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}