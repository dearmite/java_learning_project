package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmRoleResource ;
import com.common.exception.*;
import utils.PageModel;

/**
* 角色资源关系表
*  Created by autoGen Tools 
*/

public interface UmRoleResourceService  {

	 /**
	     * delete 角色资源关系表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	 public void deleteUmRoleResourceByPK(java.lang.String pkUuid) throws MyException ;
	 
	  /**
	     * add 角色资源关系表
	     * @param  umRoleResource 角色资源关系表
	     * @throws MyException
	     */
	  void addUmRoleResource (UmRoleResource  umRoleResource)  throws MyException;
	  
	   /**
	     * edit 角色资源关系表
	     * @param umRoleResource 角色资源关系表
	     * @throws MyException
	     */
	  void updateUmRoleResource (UmRoleResource   umRoleResource)  throws MyException;
	    /**
	     * get 角色资源关系表
	     * @param pkUuid  角色资源关系表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmRoleResource getUmRoleResourceByPK(java.lang.String pkUuid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmRoleResource> queryUmRoleResourceBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}