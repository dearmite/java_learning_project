package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . RoomExam ;
import com.common.exception.*;
import utils.PageModel;

/**
* room_exam
*  Created by autoGen Tools 
*/

public interface RoomExamService  {

	 /**
	     * delete room_exam
	     * @param roomExamId PKID
	     * @throws MyException
	     */
	 public void deleteRoomExamByPK(java.lang.Integer roomExamId) throws MyException ;
	 
	  /**
	     * add room_exam
	     * @param  roomExam room_exam
	     * @throws MyException
	     */
	  void addRoomExam (RoomExam  roomExam)  throws MyException;
	  
	   /**
	     * edit room_exam
	     * @param roomExam room_exam
	     * @throws MyException
	     */
	  void updateRoomExam (RoomExam   roomExam)  throws MyException;
	    /**
	     * get room_exam
	     * @param roomExamId  room_exam PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public RoomExam getRoomExamByPK(java.lang.Integer roomExamId) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<RoomExam> queryRoomExamBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}