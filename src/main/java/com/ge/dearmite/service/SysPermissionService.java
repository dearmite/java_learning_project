package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysPermission ;
import com.common.exception.*;
import utils.PageModel;

/**
* 权限
*  Created by autoGen Tools 
*/

public interface SysPermissionService  {

	 /**
	     * delete 权限
	     * @param permissionid PKID
	     * @throws MyException
	     */
	 public void deleteSysPermissionByPK(java.lang.Integer permissionid) throws MyException ;
	 
	  /**
	     * add 权限
	     * @param  sysPermission 权限
	     * @throws MyException
	     */
	  void addSysPermission (SysPermission  sysPermission)  throws MyException;
	  
	   /**
	     * edit 权限
	     * @param sysPermission 权限
	     * @throws MyException
	     */
	  void updateSysPermission (SysPermission   sysPermission)  throws MyException;
	    /**
	     * get 权限
	     * @param permissionid  权限 PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysPermission getSysPermissionByPK(java.lang.Integer permissionid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysPermission> querySysPermissionBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}