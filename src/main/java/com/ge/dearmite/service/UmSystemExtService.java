package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmSystemExt ;
import com.common.exception.*;
import utils.PageModel;

/**
* 系统扩展属性表
*  Created by autoGen Tools 
*/

public interface UmSystemExtService  {

	 /**
	     * delete 系统扩展属性表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	 public void deleteUmSystemExtByPK(java.lang.String pkUuid) throws MyException ;
	 
	  /**
	     * add 系统扩展属性表
	     * @param  umSystemExt 系统扩展属性表
	     * @throws MyException
	     */
	  void addUmSystemExt (UmSystemExt  umSystemExt)  throws MyException;
	  
	   /**
	     * edit 系统扩展属性表
	     * @param umSystemExt 系统扩展属性表
	     * @throws MyException
	     */
	  void updateUmSystemExt (UmSystemExt   umSystemExt)  throws MyException;
	    /**
	     * get 系统扩展属性表
	     * @param pkUuid  系统扩展属性表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmSystemExt getUmSystemExtByPK(java.lang.String pkUuid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmSystemExt> queryUmSystemExtBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}