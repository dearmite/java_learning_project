package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmSystemType ;
import com.ge.dearmite.service.UmSystemTypeService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("umSystemTypeService")
public class UmSystemTypeServiceImpl  implements  UmSystemTypeService {
private static Logger logger = Logger.getLogger(UmSystemTypeServiceImpl.class);

	//=============================================
	@Autowired
	private  UmSystemTypeMapper umSystemTypeMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param sysTypeCode PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmSystemTypeByPK(java.lang.String sysTypeCode) throws MyException	 
	 {
		if (sysTypeCode == null) {
			throw new MyException("UmSystemType pk is not null");
		}
		
		try {
			umSystemTypeMapper.deleteByPrimaryKey(sysTypeCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmSystemType fail!");
		}
		 
	 }
	 
	 @Override
	  public UmSystemType getUmSystemTypeByPK(java.lang.String sysTypeCode) throws MyException
	  {
		if (sysTypeCode == null) {
			throw new MyException("UmSystemType pk is not null");
		}
		
		try {
			return  umSystemTypeMapper.selectByPrimaryKey(sysTypeCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmSystemType fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param umSystemType InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addUmSystemType (UmSystemType umSystemType)  throws MyException
	  {
		if (umSystemType == null) {
			throw new MyException("umSystemType is not null");
		}
		
		try {
			umSystemTypeMapper. insertSelective( umSystemType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  umSystemType InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateUmSystemType (UmSystemType umSystemType)  throws MyException
	{
			  
		if (umSystemType == null) {
			throw new MyException("umSystemType is not null");
		}
		
		try {
			umSystemTypeMapper. updateByPrimaryKeySelective( umSystemType);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmSystemType> queryUmSystemTypeBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmSystemType> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmSystemType> pageList = umSystemTypeMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmSystemType> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

