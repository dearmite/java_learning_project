package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmUser ;
import com.ge.dearmite.service.UmUserService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* UM用户表
*  Created by autoGen Tools 
*/
@Service("umUserService")
public class UmUserServiceImpl  implements  UmUserService {
private static Logger logger = Logger.getLogger(UmUserServiceImpl.class);

	//=============================================
	@Autowired
	private  UmUserMapper umUserMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete UM用户表
	     * @param userCode PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmUserByPK(java.lang.String userCode) throws MyException	 
	 {
		if (userCode == null) {
			throw new MyException("UmUser pk is not null");
		}
		
		try {
			umUserMapper.deleteByPrimaryKey(userCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmUser fail!");
		}
		 
	 }
	 
	 @Override
	  public UmUser getUmUserByPK(java.lang.String userCode) throws MyException
	  {
		if (userCode == null) {
			throw new MyException("UmUser pk is not null");
		}
		
		try {
			return  umUserMapper.selectByPrimaryKey(userCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmUser fail!");
		}
	  
	  }
	 
	  /**
	     * add UM用户表
	     * @param umUser UM用户表
	     * @throws MyException
	     */
	@Override
	public  void addUmUser (UmUser umUser)  throws MyException
	  {
		if (umUser == null) {
			throw new MyException("umUser is not null");
		}
		
		try {
			umUserMapper. insertSelective( umUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit UM用户表
	     * @param  umUser UM用户表
	     * @throws MyException
	     */
	@Override
	public  void updateUmUser (UmUser umUser)  throws MyException
	{
			  
		if (umUser == null) {
			throw new MyException("umUser is not null");
		}
		
		try {
			umUserMapper. updateByPrimaryKeySelective( umUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmUser> queryUmUserBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmUser> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmUser> pageList = umUserMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmUser> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

