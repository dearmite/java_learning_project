package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmSystemExt ;
import com.ge.dearmite.service.UmSystemExtService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 系统扩展属性表
*  Created by autoGen Tools 
*/
@Service("umSystemExtService")
public class UmSystemExtServiceImpl  implements  UmSystemExtService {
private static Logger logger = Logger.getLogger(UmSystemExtServiceImpl.class);

	//=============================================
	@Autowired
	private  UmSystemExtMapper umSystemExtMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 系统扩展属性表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmSystemExtByPK(java.lang.String pkUuid) throws MyException	 
	 {
		if (pkUuid == null) {
			throw new MyException("UmSystemExt pk is not null");
		}
		
		try {
			umSystemExtMapper.deleteByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmSystemExt fail!");
		}
		 
	 }
	 
	 @Override
	  public UmSystemExt getUmSystemExtByPK(java.lang.String pkUuid) throws MyException
	  {
		if (pkUuid == null) {
			throw new MyException("UmSystemExt pk is not null");
		}
		
		try {
			return  umSystemExtMapper.selectByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmSystemExt fail!");
		}
	  
	  }
	 
	  /**
	     * add 系统扩展属性表
	     * @param umSystemExt 系统扩展属性表
	     * @throws MyException
	     */
	@Override
	public  void addUmSystemExt (UmSystemExt umSystemExt)  throws MyException
	  {
		if (umSystemExt == null) {
			throw new MyException("umSystemExt is not null");
		}
		
		try {
			umSystemExtMapper. insertSelective( umSystemExt);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 系统扩展属性表
	     * @param  umSystemExt 系统扩展属性表
	     * @throws MyException
	     */
	@Override
	public  void updateUmSystemExt (UmSystemExt umSystemExt)  throws MyException
	{
			  
		if (umSystemExt == null) {
			throw new MyException("umSystemExt is not null");
		}
		
		try {
			umSystemExtMapper. updateByPrimaryKeySelective( umSystemExt);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmSystemExt> queryUmSystemExtBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmSystemExt> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmSystemExt> pageList = umSystemExtMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmSystemExt> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

