package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysUser2 ;
import com.ge.dearmite.service.SysUser2Service ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 用户
*  Created by autoGen Tools 
*/
@Service("sysUser2Service")
public class SysUser2ServiceImpl  implements  SysUser2Service {
private static Logger logger = Logger.getLogger(SysUser2ServiceImpl.class);

	//=============================================
	@Autowired
	private  SysUser2Mapper sysUser2Mapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 用户
	     * @param userid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysUser2ByPK(java.lang.Integer userid) throws MyException	 
	 {
		if (userid == null) {
			throw new MyException("SysUser2 pk is not null");
		}
		
		try {
			sysUser2Mapper.deleteByPrimaryKey(userid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysUser2 fail!");
		}
		 
	 }
	 
	 @Override
	  public SysUser2 getSysUser2ByPK(java.lang.Integer userid) throws MyException
	  {
		if (userid == null) {
			throw new MyException("SysUser2 pk is not null");
		}
		
		try {
			return  sysUser2Mapper.selectByPrimaryKey(userid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysUser2 fail!");
		}
	  
	  }
	 
	  /**
	     * add 用户
	     * @param sysUser2 用户
	     * @throws MyException
	     */
	@Override
	public  void addSysUser2 (SysUser2 sysUser2)  throws MyException
	  {
		if (sysUser2 == null) {
			throw new MyException("sysUser2 is not null");
		}
		
		try {
			sysUser2Mapper. insertSelective( sysUser2);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 用户
	     * @param  sysUser2 用户
	     * @throws MyException
	     */
	@Override
	public  void updateSysUser2 (SysUser2 sysUser2)  throws MyException
	{
			  
		if (sysUser2 == null) {
			throw new MyException("sysUser2 is not null");
		}
		
		try {
			sysUser2Mapper. updateByPrimaryKeySelective( sysUser2);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysUser2> querySysUser2BySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysUser2> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysUser2> pageList = sysUser2Mapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysUser2> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

