package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmResource ;
import com.ge.dearmite.service.UmResourceService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 资源表
*  Created by autoGen Tools 
*/
@Service("umResourceService")
public class UmResourceServiceImpl  implements  UmResourceService {
private static Logger logger = Logger.getLogger(UmResourceServiceImpl.class);

	//=============================================
	@Autowired
	private  UmResourceMapper umResourceMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 资源表
	     * @param resourceId PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmResourceByPK(java.lang.String resourceId) throws MyException	 
	 {
		if (resourceId == null) {
			throw new MyException("UmResource pk is not null");
		}
		
		try {
			umResourceMapper.deleteByPrimaryKey(resourceId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmResource fail!");
		}
		 
	 }
	 
	 @Override
	  public UmResource getUmResourceByPK(java.lang.String resourceId) throws MyException
	  {
		if (resourceId == null) {
			throw new MyException("UmResource pk is not null");
		}
		
		try {
			return  umResourceMapper.selectByPrimaryKey(resourceId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmResource fail!");
		}
	  
	  }
	 
	  /**
	     * add 资源表
	     * @param umResource 资源表
	     * @throws MyException
	     */
	@Override
	public  void addUmResource (UmResource umResource)  throws MyException
	  {
		if (umResource == null) {
			throw new MyException("umResource is not null");
		}
		
		try {
			umResourceMapper. insertSelective( umResource);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 资源表
	     * @param  umResource 资源表
	     * @throws MyException
	     */
	@Override
	public  void updateUmResource (UmResource umResource)  throws MyException
	{
			  
		if (umResource == null) {
			throw new MyException("umResource is not null");
		}
		
		try {
			umResourceMapper. updateByPrimaryKeySelective( umResource);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmResource> queryUmResourceBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmResource> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmResource> pageList = umResourceMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmResource> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

