package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysUser ;
import com.ge.dearmite.service.SysUserService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 用户
*  Created by autoGen Tools 
*/
@Service("sysUserService")
public class SysUserServiceImpl  implements  SysUserService {
private static Logger logger = Logger.getLogger(SysUserServiceImpl.class);

	//=============================================
	@Autowired
	private  SysUserMapper sysUserMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 用户
	     * @param userid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysUserByPK(java.lang.Integer userid) throws MyException	 
	 {
		if (userid == null) {
			throw new MyException("SysUser pk is not null");
		}
		
		try {
			sysUserMapper.deleteByPrimaryKey(userid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysUser fail!");
		}
		 
	 }
	 
	 @Override
	  public SysUser getSysUserByPK(java.lang.Integer userid) throws MyException
	  {
		if (userid == null) {
			throw new MyException("SysUser pk is not null");
		}
		
		try {
			return  sysUserMapper.selectByPrimaryKey(userid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysUser fail!");
		}
	  
	  }
	 
	  /**
	     * add 用户
	     * @param sysUser 用户
	     * @throws MyException
	     */
	@Override
	public  void addSysUser (SysUser sysUser)  throws MyException
	  {
		if (sysUser == null) {
			throw new MyException("sysUser is not null");
		}
		
		try {
			sysUserMapper. insertSelective( sysUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 用户
	     * @param  sysUser 用户
	     * @throws MyException
	     */
	@Override
	public  void updateSysUser (SysUser sysUser)  throws MyException
	{
			  
		if (sysUser == null) {
			throw new MyException("sysUser is not null");
		}
		
		try {
			sysUserMapper. updateByPrimaryKeySelective( sysUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysUser> querySysUserBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysUser> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysUser> pageList = sysUserMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysUser> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

