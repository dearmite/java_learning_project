package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysRolePermission ;
import com.ge.dearmite.service.SysRolePermissionService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 角色权限
*  Created by autoGen Tools 
*/
@Service("sysRolePermissionService")
public class SysRolePermissionServiceImpl  implements  SysRolePermissionService {
private static Logger logger = Logger.getLogger(SysRolePermissionServiceImpl.class);

	//=============================================
	@Autowired
	private  SysRolePermissionMapper sysRolePermissionMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 角色权限
	     * @param rolepermissionid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysRolePermissionByPK(java.lang.Integer rolepermissionid) throws MyException	 
	 {
		if (rolepermissionid == null) {
			throw new MyException("SysRolePermission pk is not null");
		}
		
		try {
			sysRolePermissionMapper.deleteByPrimaryKey(rolepermissionid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysRolePermission fail!");
		}
		 
	 }
	 
	 @Override
	  public SysRolePermission getSysRolePermissionByPK(java.lang.Integer rolepermissionid) throws MyException
	  {
		if (rolepermissionid == null) {
			throw new MyException("SysRolePermission pk is not null");
		}
		
		try {
			return  sysRolePermissionMapper.selectByPrimaryKey(rolepermissionid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysRolePermission fail!");
		}
	  
	  }
	 
	  /**
	     * add 角色权限
	     * @param sysRolePermission 角色权限
	     * @throws MyException
	     */
	@Override
	public  void addSysRolePermission (SysRolePermission sysRolePermission)  throws MyException
	  {
		if (sysRolePermission == null) {
			throw new MyException("sysRolePermission is not null");
		}
		
		try {
			sysRolePermissionMapper. insertSelective( sysRolePermission);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 角色权限
	     * @param  sysRolePermission 角色权限
	     * @throws MyException
	     */
	@Override
	public  void updateSysRolePermission (SysRolePermission sysRolePermission)  throws MyException
	{
			  
		if (sysRolePermission == null) {
			throw new MyException("sysRolePermission is not null");
		}
		
		try {
			sysRolePermissionMapper. updateByPrimaryKeySelective( sysRolePermission);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysRolePermission> querySysRolePermissionBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysRolePermission> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysRolePermission> pageList = sysRolePermissionMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysRolePermission> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

