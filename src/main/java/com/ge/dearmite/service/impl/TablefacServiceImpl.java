package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.Tablefac ;
import com.ge.dearmite.service.TablefacService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("tablefacService")
public class TablefacServiceImpl  implements  TablefacService {
private static Logger logger = Logger.getLogger(TablefacServiceImpl.class);

	//=============================================
	@Autowired
	private  TablefacMapper tablefacMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	@Override
	public void deleteTablefacByPK(java.lang.Integer id) throws MyException	 
	 {
		if (id == null) {
			throw new MyException("Tablefac pk is not null");
		}
		
		try {
			tablefacMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete Tablefac fail!");
		}
		 
	 }
	 
	 @Override
	  public Tablefac getTablefacByPK(java.lang.Integer id) throws MyException
	  {
		if (id == null) {
			throw new MyException("Tablefac pk is not null");
		}
		
		try {
			return  tablefacMapper.selectByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get Tablefac fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param tablefac InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addTablefac (Tablefac tablefac)  throws MyException
	  {
		if (tablefac == null) {
			throw new MyException("tablefac is not null");
		}
		
		try {
			tablefacMapper. insertSelective( tablefac);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  tablefac InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateTablefac (Tablefac tablefac)  throws MyException
	{
			  
		if (tablefac == null) {
			throw new MyException("tablefac is not null");
		}
		
		try {
			tablefacMapper. updateByPrimaryKeySelective( tablefac);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<Tablefac> queryTablefacBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<Tablefac> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<Tablefac> pageList = tablefacMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<Tablefac> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

