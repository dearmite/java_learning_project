package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.Country ;
import com.ge.dearmite.service.CountryService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("countryService")
public class CountryServiceImpl  implements  CountryService {
private static Logger logger = Logger.getLogger(CountryServiceImpl.class);

	//=============================================
	@Autowired
	private  CountryMapper countryMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	@Override
	public void deleteCountryByPK(java.lang.Integer id) throws MyException	 
	 {
		if (id == null) {
			throw new MyException("Country pk is not null");
		}
		
		try {
			countryMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete Country fail!");
		}
		 
	 }
	 
	 @Override
	  public Country getCountryByPK(java.lang.Integer id) throws MyException
	  {
		if (id == null) {
			throw new MyException("Country pk is not null");
		}
		
		try {
			return  countryMapper.selectByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get Country fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param country InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addCountry (Country country)  throws MyException
	  {
		if (country == null) {
			throw new MyException("country is not null");
		}
		
		try {
			countryMapper. insertSelective( country);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  country InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateCountry (Country country)  throws MyException
	{
			  
		if (country == null) {
			throw new MyException("country is not null");
		}
		
		try {
			countryMapper. updateByPrimaryKeySelective( country);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<Country> queryCountryBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<Country> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<Country> pageList = countryMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<Country> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

