package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmRoleResource ;
import com.ge.dearmite.service.UmRoleResourceService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 角色资源关系表
*  Created by autoGen Tools 
*/
@Service("umRoleResourceService")
public class UmRoleResourceServiceImpl  implements  UmRoleResourceService {
private static Logger logger = Logger.getLogger(UmRoleResourceServiceImpl.class);

	//=============================================
	@Autowired
	private  UmRoleResourceMapper umRoleResourceMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 角色资源关系表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmRoleResourceByPK(java.lang.String pkUuid) throws MyException	 
	 {
		if (pkUuid == null) {
			throw new MyException("UmRoleResource pk is not null");
		}
		
		try {
			umRoleResourceMapper.deleteByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmRoleResource fail!");
		}
		 
	 }
	 
	 @Override
	  public UmRoleResource getUmRoleResourceByPK(java.lang.String pkUuid) throws MyException
	  {
		if (pkUuid == null) {
			throw new MyException("UmRoleResource pk is not null");
		}
		
		try {
			return  umRoleResourceMapper.selectByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmRoleResource fail!");
		}
	  
	  }
	 
	  /**
	     * add 角色资源关系表
	     * @param umRoleResource 角色资源关系表
	     * @throws MyException
	     */
	@Override
	public  void addUmRoleResource (UmRoleResource umRoleResource)  throws MyException
	  {
		if (umRoleResource == null) {
			throw new MyException("umRoleResource is not null");
		}
		
		try {
			umRoleResourceMapper. insertSelective( umRoleResource);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 角色资源关系表
	     * @param  umRoleResource 角色资源关系表
	     * @throws MyException
	     */
	@Override
	public  void updateUmRoleResource (UmRoleResource umRoleResource)  throws MyException
	{
			  
		if (umRoleResource == null) {
			throw new MyException("umRoleResource is not null");
		}
		
		try {
			umRoleResourceMapper. updateByPrimaryKeySelective( umRoleResource);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmRoleResource> queryUmRoleResourceBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmRoleResource> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmRoleResource> pageList = umRoleResourceMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmRoleResource> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

