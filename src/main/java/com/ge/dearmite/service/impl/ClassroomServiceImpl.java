package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.Classroom ;
import com.ge.dearmite.service.ClassroomService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* classroom
*  Created by autoGen Tools 
*/
@Service("classroomService")
public class ClassroomServiceImpl  implements  ClassroomService {
private static Logger logger = Logger.getLogger(ClassroomServiceImpl.class);

	//=============================================
	@Autowired
	private  ClassroomMapper classroomMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete classroom
	     * @param roomid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteClassroomByPK(java.lang.Integer roomid) throws MyException	 
	 {
		if (roomid == null) {
			throw new MyException("Classroom pk is not null");
		}
		
		try {
			classroomMapper.deleteByPrimaryKey(roomid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete Classroom fail!");
		}
		 
	 }
	 
	 @Override
	  public Classroom getClassroomByPK(java.lang.Integer roomid) throws MyException
	  {
		if (roomid == null) {
			throw new MyException("Classroom pk is not null");
		}
		
		try {
			return  classroomMapper.selectByPrimaryKey(roomid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get Classroom fail!");
		}
	  
	  }
	 
	  /**
	     * add classroom
	     * @param classroom classroom
	     * @throws MyException
	     */
	@Override
	public  void addClassroom (Classroom classroom)  throws MyException
	  {
		if (classroom == null) {
			throw new MyException("classroom is not null");
		}
		
		try {
			classroomMapper. insertSelective( classroom);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit classroom
	     * @param  classroom classroom
	     * @throws MyException
	     */
	@Override
	public  void updateClassroom (Classroom classroom)  throws MyException
	{
			  
		if (classroom == null) {
			throw new MyException("classroom is not null");
		}
		
		try {
			classroomMapper. updateByPrimaryKeySelective( classroom);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<Classroom> queryClassroomBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<Classroom> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<Classroom> pageList = classroomMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<Classroom> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

