package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysRole ;
import com.ge.dearmite.service.SysRoleService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* sys_role
*  Created by autoGen Tools 
*/
@Service("sysRoleService")
public class SysRoleServiceImpl  implements  SysRoleService {
private static Logger logger = Logger.getLogger(SysRoleServiceImpl.class);

	//=============================================
	@Autowired
	private  SysRoleMapper sysRoleMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete sys_role
	     * @param roleid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysRoleByPK(java.lang.Integer roleid) throws MyException	 
	 {
		if (roleid == null) {
			throw new MyException("SysRole pk is not null");
		}
		
		try {
			sysRoleMapper.deleteByPrimaryKey(roleid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysRole fail!");
		}
		 
	 }
	 
	 @Override
	  public SysRole getSysRoleByPK(java.lang.Integer roleid) throws MyException
	  {
		if (roleid == null) {
			throw new MyException("SysRole pk is not null");
		}
		
		try {
			return  sysRoleMapper.selectByPrimaryKey(roleid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysRole fail!");
		}
	  
	  }
	 
	  /**
	     * add sys_role
	     * @param sysRole sys_role
	     * @throws MyException
	     */
	@Override
	public  void addSysRole (SysRole sysRole)  throws MyException
	  {
		if (sysRole == null) {
			throw new MyException("sysRole is not null");
		}
		
		try {
			sysRoleMapper. insertSelective( sysRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit sys_role
	     * @param  sysRole sys_role
	     * @throws MyException
	     */
	@Override
	public  void updateSysRole (SysRole sysRole)  throws MyException
	{
			  
		if (sysRole == null) {
			throw new MyException("sysRole is not null");
		}
		
		try {
			sysRoleMapper. updateByPrimaryKeySelective( sysRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysRole> querySysRoleBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysRole> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysRole> pageList = sysRoleMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysRole> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

