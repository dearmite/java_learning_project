package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysPermission ;
import com.ge.dearmite.service.SysPermissionService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 权限
*  Created by autoGen Tools 
*/
@Service("sysPermissionService")
public class SysPermissionServiceImpl  implements  SysPermissionService {
private static Logger logger = Logger.getLogger(SysPermissionServiceImpl.class);

	//=============================================
	@Autowired
	private  SysPermissionMapper sysPermissionMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 权限
	     * @param permissionid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysPermissionByPK(java.lang.Integer permissionid) throws MyException	 
	 {
		if (permissionid == null) {
			throw new MyException("SysPermission pk is not null");
		}
		
		try {
			sysPermissionMapper.deleteByPrimaryKey(permissionid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysPermission fail!");
		}
		 
	 }
	 
	 @Override
	  public SysPermission getSysPermissionByPK(java.lang.Integer permissionid) throws MyException
	  {
		if (permissionid == null) {
			throw new MyException("SysPermission pk is not null");
		}
		
		try {
			return  sysPermissionMapper.selectByPrimaryKey(permissionid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysPermission fail!");
		}
	  
	  }
	 
	  /**
	     * add 权限
	     * @param sysPermission 权限
	     * @throws MyException
	     */
	@Override
	public  void addSysPermission (SysPermission sysPermission)  throws MyException
	  {
		if (sysPermission == null) {
			throw new MyException("sysPermission is not null");
		}
		
		try {
			sysPermissionMapper. insertSelective( sysPermission);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 权限
	     * @param  sysPermission 权限
	     * @throws MyException
	     */
	@Override
	public  void updateSysPermission (SysPermission sysPermission)  throws MyException
	{
			  
		if (sysPermission == null) {
			throw new MyException("sysPermission is not null");
		}
		
		try {
			sysPermissionMapper. updateByPrimaryKeySelective( sysPermission);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysPermission> querySysPermissionBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysPermission> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysPermission> pageList = sysPermissionMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysPermission> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

