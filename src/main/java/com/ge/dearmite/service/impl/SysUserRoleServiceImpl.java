package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.SysUserRole ;
import com.ge.dearmite.service.SysUserRoleService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 用户角色
*  Created by autoGen Tools 
*/
@Service("sysUserRoleService")
public class SysUserRoleServiceImpl  implements  SysUserRoleService {
private static Logger logger = Logger.getLogger(SysUserRoleServiceImpl.class);

	//=============================================
	@Autowired
	private  SysUserRoleMapper sysUserRoleMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 用户角色
	     * @param userroleid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteSysUserRoleByPK(java.lang.Integer userroleid) throws MyException	 
	 {
		if (userroleid == null) {
			throw new MyException("SysUserRole pk is not null");
		}
		
		try {
			sysUserRoleMapper.deleteByPrimaryKey(userroleid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete SysUserRole fail!");
		}
		 
	 }
	 
	 @Override
	  public SysUserRole getSysUserRoleByPK(java.lang.Integer userroleid) throws MyException
	  {
		if (userroleid == null) {
			throw new MyException("SysUserRole pk is not null");
		}
		
		try {
			return  sysUserRoleMapper.selectByPrimaryKey(userroleid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get SysUserRole fail!");
		}
	  
	  }
	 
	  /**
	     * add 用户角色
	     * @param sysUserRole 用户角色
	     * @throws MyException
	     */
	@Override
	public  void addSysUserRole (SysUserRole sysUserRole)  throws MyException
	  {
		if (sysUserRole == null) {
			throw new MyException("sysUserRole is not null");
		}
		
		try {
			sysUserRoleMapper. insertSelective( sysUserRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 用户角色
	     * @param  sysUserRole 用户角色
	     * @throws MyException
	     */
	@Override
	public  void updateSysUserRole (SysUserRole sysUserRole)  throws MyException
	{
			  
		if (sysUserRole == null) {
			throw new MyException("sysUserRole is not null");
		}
		
		try {
			sysUserRoleMapper. updateByPrimaryKeySelective( sysUserRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<SysUserRole> querySysUserRoleBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<SysUserRole> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<SysUserRole> pageList = sysUserRoleMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<SysUserRole> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

