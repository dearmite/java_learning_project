package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmSystemUser ;
import com.ge.dearmite.service.UmSystemUserService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 系统用户表
*  Created by autoGen Tools 
*/
@Service("umSystemUserService")
public class UmSystemUserServiceImpl  implements  UmSystemUserService {
private static Logger logger = Logger.getLogger(UmSystemUserServiceImpl.class);

	//=============================================
	@Autowired
	private  UmSystemUserMapper umSystemUserMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 系统用户表
	     * @param sysUserId PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmSystemUserByPK(java.lang.String sysUserId) throws MyException	 
	 {
		if (sysUserId == null) {
			throw new MyException("UmSystemUser pk is not null");
		}
		
		try {
			umSystemUserMapper.deleteByPrimaryKey(sysUserId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmSystemUser fail!");
		}
		 
	 }
	 
	 @Override
	  public UmSystemUser getUmSystemUserByPK(java.lang.String sysUserId) throws MyException
	  {
		if (sysUserId == null) {
			throw new MyException("UmSystemUser pk is not null");
		}
		
		try {
			return  umSystemUserMapper.selectByPrimaryKey(sysUserId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmSystemUser fail!");
		}
	  
	  }
	 
	  /**
	     * add 系统用户表
	     * @param umSystemUser 系统用户表
	     * @throws MyException
	     */
	@Override
	public  void addUmSystemUser (UmSystemUser umSystemUser)  throws MyException
	  {
		if (umSystemUser == null) {
			throw new MyException("umSystemUser is not null");
		}
		
		try {
			umSystemUserMapper. insertSelective( umSystemUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 系统用户表
	     * @param  umSystemUser 系统用户表
	     * @throws MyException
	     */
	@Override
	public  void updateUmSystemUser (UmSystemUser umSystemUser)  throws MyException
	{
			  
		if (umSystemUser == null) {
			throw new MyException("umSystemUser is not null");
		}
		
		try {
			umSystemUserMapper. updateByPrimaryKeySelective( umSystemUser);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmSystemUser> queryUmSystemUserBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmSystemUser> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmSystemUser> pageList = umSystemUserMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmSystemUser> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

