package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmRole ;
import com.ge.dearmite.service.UmRoleService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 角色表
*  Created by autoGen Tools 
*/
@Service("umRoleService")
public class UmRoleServiceImpl  implements  UmRoleService {
private static Logger logger = Logger.getLogger(UmRoleServiceImpl.class);

	//=============================================
	@Autowired
	private  UmRoleMapper umRoleMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 角色表
	     * @param roleCode PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmRoleByPK(java.lang.String roleCode) throws MyException	 
	 {
		if (roleCode == null) {
			throw new MyException("UmRole pk is not null");
		}
		
		try {
			umRoleMapper.deleteByPrimaryKey(roleCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmRole fail!");
		}
		 
	 }
	 
	 @Override
	  public UmRole getUmRoleByPK(java.lang.String roleCode) throws MyException
	  {
		if (roleCode == null) {
			throw new MyException("UmRole pk is not null");
		}
		
		try {
			return  umRoleMapper.selectByPrimaryKey(roleCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmRole fail!");
		}
	  
	  }
	 
	  /**
	     * add 角色表
	     * @param umRole 角色表
	     * @throws MyException
	     */
	@Override
	public  void addUmRole (UmRole umRole)  throws MyException
	  {
		if (umRole == null) {
			throw new MyException("umRole is not null");
		}
		
		try {
			umRoleMapper. insertSelective( umRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 角色表
	     * @param  umRole 角色表
	     * @throws MyException
	     */
	@Override
	public  void updateUmRole (UmRole umRole)  throws MyException
	{
			  
		if (umRole == null) {
			throw new MyException("umRole is not null");
		}
		
		try {
			umRoleMapper. updateByPrimaryKeySelective( umRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmRole> queryUmRoleBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmRole> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmRole> pageList = umRoleMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmRole> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

