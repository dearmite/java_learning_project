package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmDepartment ;
import com.ge.dearmite.service.UmDepartmentService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 机构表
*  Created by autoGen Tools 
*/
@Service("umDepartmentService")
public class UmDepartmentServiceImpl  implements  UmDepartmentService {
private static Logger logger = Logger.getLogger(UmDepartmentServiceImpl.class);

	//=============================================
	@Autowired
	private  UmDepartmentMapper umDepartmentMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 机构表
	     * @param dptCde PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmDepartmentByPK(java.lang.String dptCde) throws MyException	 
	 {
		if (dptCde == null) {
			throw new MyException("UmDepartment pk is not null");
		}
		
		try {
			umDepartmentMapper.deleteByPrimaryKey(dptCde);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmDepartment fail!");
		}
		 
	 }
	 
	 @Override
	  public UmDepartment getUmDepartmentByPK(java.lang.String dptCde) throws MyException
	  {
		if (dptCde == null) {
			throw new MyException("UmDepartment pk is not null");
		}
		
		try {
			return  umDepartmentMapper.selectByPrimaryKey(dptCde);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmDepartment fail!");
		}
	  
	  }
	 
	  /**
	     * add 机构表
	     * @param umDepartment 机构表
	     * @throws MyException
	     */
	@Override
	public  void addUmDepartment (UmDepartment umDepartment)  throws MyException
	  {
		if (umDepartment == null) {
			throw new MyException("umDepartment is not null");
		}
		
		try {
			umDepartmentMapper. insertSelective( umDepartment);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 机构表
	     * @param  umDepartment 机构表
	     * @throws MyException
	     */
	@Override
	public  void updateUmDepartment (UmDepartment umDepartment)  throws MyException
	{
			  
		if (umDepartment == null) {
			throw new MyException("umDepartment is not null");
		}
		
		try {
			umDepartmentMapper. updateByPrimaryKeySelective( umDepartment);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmDepartment> queryUmDepartmentBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmDepartment> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmDepartment> pageList = umDepartmentMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmDepartment> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

