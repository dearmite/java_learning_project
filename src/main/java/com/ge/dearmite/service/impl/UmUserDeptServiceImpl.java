package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmUserDept ;
import com.ge.dearmite.service.UmUserDeptService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* UM用户机构关系表
*  Created by autoGen Tools 
*/
@Service("umUserDeptService")
public class UmUserDeptServiceImpl  implements  UmUserDeptService {
private static Logger logger = Logger.getLogger(UmUserDeptServiceImpl.class);

	//=============================================
	@Autowired
	private  UmUserDeptMapper umUserDeptMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete UM用户机构关系表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmUserDeptByPK(java.lang.String pkUuid) throws MyException	 
	 {
		if (pkUuid == null) {
			throw new MyException("UmUserDept pk is not null");
		}
		
		try {
			umUserDeptMapper.deleteByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmUserDept fail!");
		}
		 
	 }
	 
	 @Override
	  public UmUserDept getUmUserDeptByPK(java.lang.String pkUuid) throws MyException
	  {
		if (pkUuid == null) {
			throw new MyException("UmUserDept pk is not null");
		}
		
		try {
			return  umUserDeptMapper.selectByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmUserDept fail!");
		}
	  
	  }
	 
	  /**
	     * add UM用户机构关系表
	     * @param umUserDept UM用户机构关系表
	     * @throws MyException
	     */
	@Override
	public  void addUmUserDept (UmUserDept umUserDept)  throws MyException
	  {
		if (umUserDept == null) {
			throw new MyException("umUserDept is not null");
		}
		
		try {
			umUserDeptMapper. insertSelective( umUserDept);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit UM用户机构关系表
	     * @param  umUserDept UM用户机构关系表
	     * @throws MyException
	     */
	@Override
	public  void updateUmUserDept (UmUserDept umUserDept)  throws MyException
	{
			  
		if (umUserDept == null) {
			throw new MyException("umUserDept is not null");
		}
		
		try {
			umUserDeptMapper. updateByPrimaryKeySelective( umUserDept);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmUserDept> queryUmUserDeptBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmUserDept> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmUserDept> pageList = umUserDeptMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmUserDept> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

