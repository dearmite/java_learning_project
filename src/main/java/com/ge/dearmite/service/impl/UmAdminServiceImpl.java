package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmAdmin ;
import com.ge.dearmite.service.UmAdminService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("umAdminService")
public class UmAdminServiceImpl  implements  UmAdminService {
private static Logger logger = Logger.getLogger(UmAdminServiceImpl.class);

	//=============================================
	@Autowired
	private  UmAdminMapper umAdminMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmAdminByPK(java.lang.String pkUuid) throws MyException	 
	 {
		if (pkUuid == null) {
			throw new MyException("UmAdmin pk is not null");
		}
		
		try {
			umAdminMapper.deleteByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmAdmin fail!");
		}
		 
	 }
	 
	 @Override
	  public UmAdmin getUmAdminByPK(java.lang.String pkUuid) throws MyException
	  {
		if (pkUuid == null) {
			throw new MyException("UmAdmin pk is not null");
		}
		
		try {
			return  umAdminMapper.selectByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmAdmin fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param umAdmin InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addUmAdmin (UmAdmin umAdmin)  throws MyException
	  {
		if (umAdmin == null) {
			throw new MyException("umAdmin is not null");
		}
		
		try {
			umAdminMapper. insertSelective( umAdmin);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  umAdmin InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateUmAdmin (UmAdmin umAdmin)  throws MyException
	{
			  
		if (umAdmin == null) {
			throw new MyException("umAdmin is not null");
		}
		
		try {
			umAdminMapper. updateByPrimaryKeySelective( umAdmin);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmAdmin> queryUmAdminBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmAdmin> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmAdmin> pageList = umAdminMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmAdmin> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

