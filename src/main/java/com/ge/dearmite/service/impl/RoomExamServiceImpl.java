package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.RoomExam ;
import com.ge.dearmite.service.RoomExamService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* room_exam
*  Created by autoGen Tools 
*/
@Service("roomExamService")
public class RoomExamServiceImpl  implements  RoomExamService {
private static Logger logger = Logger.getLogger(RoomExamServiceImpl.class);

	//=============================================
	@Autowired
	private  RoomExamMapper roomExamMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete room_exam
	     * @param roomExamId PKID
	     * @throws MyException
	     */
	@Override
	public void deleteRoomExamByPK(java.lang.Integer roomExamId) throws MyException	 
	 {
		if (roomExamId == null) {
			throw new MyException("RoomExam pk is not null");
		}
		
		try {
			roomExamMapper.deleteByPrimaryKey(roomExamId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete RoomExam fail!");
		}
		 
	 }
	 
	 @Override
	  public RoomExam getRoomExamByPK(java.lang.Integer roomExamId) throws MyException
	  {
		if (roomExamId == null) {
			throw new MyException("RoomExam pk is not null");
		}
		
		try {
			return  roomExamMapper.selectByPrimaryKey(roomExamId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get RoomExam fail!");
		}
	  
	  }
	 
	  /**
	     * add room_exam
	     * @param roomExam room_exam
	     * @throws MyException
	     */
	@Override
	public  void addRoomExam (RoomExam roomExam)  throws MyException
	  {
		if (roomExam == null) {
			throw new MyException("roomExam is not null");
		}
		
		try {
			roomExamMapper. insertSelective( roomExam);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit room_exam
	     * @param  roomExam room_exam
	     * @throws MyException
	     */
	@Override
	public  void updateRoomExam (RoomExam roomExam)  throws MyException
	{
			  
		if (roomExam == null) {
			throw new MyException("roomExam is not null");
		}
		
		try {
			roomExamMapper. updateByPrimaryKeySelective( roomExam);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<RoomExam> queryRoomExamBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<RoomExam> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<RoomExam> pageList = roomExamMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<RoomExam> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

