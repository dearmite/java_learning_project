package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.HrUserDepartment ;
import com.ge.dearmite.service.HrUserDepartmentService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("hrUserDepartmentService")
public class HrUserDepartmentServiceImpl  implements  HrUserDepartmentService {
private static Logger logger = Logger.getLogger(HrUserDepartmentServiceImpl.class);

	//=============================================
	@Autowired
	private  HrUserDepartmentMapper hrUserDepartmentMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param emId PKID
	     * @throws MyException
	     */
	@Override
	public void deleteHrUserDepartmentByPK(java.lang.String emId) throws MyException	 
	 {
		if (emId == null) {
			throw new MyException("HrUserDepartment pk is not null");
		}
		
		try {
			hrUserDepartmentMapper.deleteByPrimaryKey(emId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete HrUserDepartment fail!");
		}
		 
	 }
	 
	 @Override
	  public HrUserDepartment getHrUserDepartmentByPK(java.lang.String emId) throws MyException
	  {
		if (emId == null) {
			throw new MyException("HrUserDepartment pk is not null");
		}
		
		try {
			return  hrUserDepartmentMapper.selectByPrimaryKey(emId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get HrUserDepartment fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param hrUserDepartment InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addHrUserDepartment (HrUserDepartment hrUserDepartment)  throws MyException
	  {
		if (hrUserDepartment == null) {
			throw new MyException("hrUserDepartment is not null");
		}
		
		try {
			hrUserDepartmentMapper. insertSelective( hrUserDepartment);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  hrUserDepartment InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateHrUserDepartment (HrUserDepartment hrUserDepartment)  throws MyException
	{
			  
		if (hrUserDepartment == null) {
			throw new MyException("hrUserDepartment is not null");
		}
		
		try {
			hrUserDepartmentMapper. updateByPrimaryKeySelective( hrUserDepartment);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<HrUserDepartment> queryHrUserDepartmentBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<HrUserDepartment> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<HrUserDepartment> pageList = hrUserDepartmentMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<HrUserDepartment> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

