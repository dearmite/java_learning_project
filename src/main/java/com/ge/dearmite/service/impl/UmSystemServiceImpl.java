package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmSystem ;
import com.ge.dearmite.service.UmSystemService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 系统表
*  Created by autoGen Tools 
*/
@Service("umSystemService")
public class UmSystemServiceImpl  implements  UmSystemService {
private static Logger logger = Logger.getLogger(UmSystemServiceImpl.class);

	//=============================================
	@Autowired
	private  UmSystemMapper umSystemMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 系统表
	     * @param sysCode PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmSystemByPK(java.lang.String sysCode) throws MyException	 
	 {
		if (sysCode == null) {
			throw new MyException("UmSystem pk is not null");
		}
		
		try {
			umSystemMapper.deleteByPrimaryKey(sysCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmSystem fail!");
		}
		 
	 }
	 
	 @Override
	  public UmSystem getUmSystemByPK(java.lang.String sysCode) throws MyException
	  {
		if (sysCode == null) {
			throw new MyException("UmSystem pk is not null");
		}
		
		try {
			return  umSystemMapper.selectByPrimaryKey(sysCode);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmSystem fail!");
		}
	  
	  }
	 
	  /**
	     * add 系统表
	     * @param umSystem 系统表
	     * @throws MyException
	     */
	@Override
	public  void addUmSystem (UmSystem umSystem)  throws MyException
	  {
		if (umSystem == null) {
			throw new MyException("umSystem is not null");
		}
		
		try {
			umSystemMapper. insertSelective( umSystem);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 系统表
	     * @param  umSystem 系统表
	     * @throws MyException
	     */
	@Override
	public  void updateUmSystem (UmSystem umSystem)  throws MyException
	{
			  
		if (umSystem == null) {
			throw new MyException("umSystem is not null");
		}
		
		try {
			umSystemMapper. updateByPrimaryKeySelective( umSystem);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmSystem> queryUmSystemBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmSystem> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmSystem> pageList = umSystemMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmSystem> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

