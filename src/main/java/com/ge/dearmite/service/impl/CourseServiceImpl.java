package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.Course ;
import com.ge.dearmite.service.CourseService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* course
*  Created by autoGen Tools 
*/
@Service("courseService")
public class CourseServiceImpl  implements  CourseService {
private static Logger logger = Logger.getLogger(CourseServiceImpl.class);

	//=============================================
	@Autowired
	private  CourseMapper courseMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete course
	     * @param courseid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteCourseByPK(java.lang.Integer courseid) throws MyException	 
	 {
		if (courseid == null) {
			throw new MyException("Course pk is not null");
		}
		
		try {
			courseMapper.deleteByPrimaryKey(courseid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete Course fail!");
		}
		 
	 }
	 
	 @Override
	  public Course getCourseByPK(java.lang.Integer courseid) throws MyException
	  {
		if (courseid == null) {
			throw new MyException("Course pk is not null");
		}
		
		try {
			return  courseMapper.selectByPrimaryKey(courseid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get Course fail!");
		}
	  
	  }
	 
	  /**
	     * add course
	     * @param course course
	     * @throws MyException
	     */
	@Override
	public  void addCourse (Course course)  throws MyException
	  {
		if (course == null) {
			throw new MyException("course is not null");
		}
		
		try {
			courseMapper. insertSelective( course);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit course
	     * @param  course course
	     * @throws MyException
	     */
	@Override
	public  void updateCourse (Course course)  throws MyException
	{
			  
		if (course == null) {
			throw new MyException("course is not null");
		}
		
		try {
			courseMapper. updateByPrimaryKeySelective( course);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<Course> queryCourseBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<Course> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<Course> pageList = courseMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<Course> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

