package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UmSysUserRole ;
import com.ge.dearmite.service.UmSysUserRoleService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* 系统用户角色表
*  Created by autoGen Tools 
*/
@Service("umSysUserRoleService")
public class UmSysUserRoleServiceImpl  implements  UmSysUserRoleService {
private static Logger logger = Logger.getLogger(UmSysUserRoleServiceImpl.class);

	//=============================================
	@Autowired
	private  UmSysUserRoleMapper umSysUserRoleMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete 系统用户角色表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUmSysUserRoleByPK(java.lang.String pkUuid) throws MyException	 
	 {
		if (pkUuid == null) {
			throw new MyException("UmSysUserRole pk is not null");
		}
		
		try {
			umSysUserRoleMapper.deleteByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UmSysUserRole fail!");
		}
		 
	 }
	 
	 @Override
	  public UmSysUserRole getUmSysUserRoleByPK(java.lang.String pkUuid) throws MyException
	  {
		if (pkUuid == null) {
			throw new MyException("UmSysUserRole pk is not null");
		}
		
		try {
			return  umSysUserRoleMapper.selectByPrimaryKey(pkUuid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UmSysUserRole fail!");
		}
	  
	  }
	 
	  /**
	     * add 系统用户角色表
	     * @param umSysUserRole 系统用户角色表
	     * @throws MyException
	     */
	@Override
	public  void addUmSysUserRole (UmSysUserRole umSysUserRole)  throws MyException
	  {
		if (umSysUserRole == null) {
			throw new MyException("umSysUserRole is not null");
		}
		
		try {
			umSysUserRoleMapper. insertSelective( umSysUserRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit 系统用户角色表
	     * @param  umSysUserRole 系统用户角色表
	     * @throws MyException
	     */
	@Override
	public  void updateUmSysUserRole (UmSysUserRole umSysUserRole)  throws MyException
	{
			  
		if (umSysUserRole == null) {
			throw new MyException("umSysUserRole is not null");
		}
		
		try {
			umSysUserRoleMapper. updateByPrimaryKeySelective( umSysUserRole);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UmSysUserRole> queryUmSysUserRoleBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UmSysUserRole> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UmSysUserRole> pageList = umSysUserRoleMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UmSysUserRole> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

