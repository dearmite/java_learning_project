package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.RoomCourse ;
import com.ge.dearmite.service.RoomCourseService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* room_course
*  Created by autoGen Tools 
*/
@Service("roomCourseService")
public class RoomCourseServiceImpl  implements  RoomCourseService {
private static Logger logger = Logger.getLogger(RoomCourseServiceImpl.class);

	//=============================================
	@Autowired
	private  RoomCourseMapper roomCourseMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete room_course
	     * @param roomCourseId PKID
	     * @throws MyException
	     */
	@Override
	public void deleteRoomCourseByPK(java.lang.Integer roomCourseId) throws MyException	 
	 {
		if (roomCourseId == null) {
			throw new MyException("RoomCourse pk is not null");
		}
		
		try {
			roomCourseMapper.deleteByPrimaryKey(roomCourseId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete RoomCourse fail!");
		}
		 
	 }
	 
	 @Override
	  public RoomCourse getRoomCourseByPK(java.lang.Integer roomCourseId) throws MyException
	  {
		if (roomCourseId == null) {
			throw new MyException("RoomCourse pk is not null");
		}
		
		try {
			return  roomCourseMapper.selectByPrimaryKey(roomCourseId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get RoomCourse fail!");
		}
	  
	  }
	 
	  /**
	     * add room_course
	     * @param roomCourse room_course
	     * @throws MyException
	     */
	@Override
	public  void addRoomCourse (RoomCourse roomCourse)  throws MyException
	  {
		if (roomCourse == null) {
			throw new MyException("roomCourse is not null");
		}
		
		try {
			roomCourseMapper. insertSelective( roomCourse);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit room_course
	     * @param  roomCourse room_course
	     * @throws MyException
	     */
	@Override
	public  void updateRoomCourse (RoomCourse roomCourse)  throws MyException
	{
			  
		if (roomCourse == null) {
			throw new MyException("roomCourse is not null");
		}
		
		try {
			roomCourseMapper. updateByPrimaryKeySelective( roomCourse);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<RoomCourse> queryRoomCourseBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<RoomCourse> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<RoomCourse> pageList = roomCourseMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<RoomCourse> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

