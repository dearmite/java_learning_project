package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.UserInfo ;
import com.ge.dearmite.service.UserInfoService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@Service("userInfoService")
public class UserInfoServiceImpl  implements  UserInfoService {
private static Logger logger = Logger.getLogger(UserInfoServiceImpl.class);

	//=============================================
	@Autowired
	private  UserInfoMapper userInfoMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	@Override
	public void deleteUserInfoByPK(java.lang.Integer id) throws MyException	 
	 {
		if (id == null) {
			throw new MyException("UserInfo pk is not null");
		}
		
		try {
			userInfoMapper.deleteByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete UserInfo fail!");
		}
		 
	 }
	 
	 @Override
	  public UserInfo getUserInfoByPK(java.lang.Integer id) throws MyException
	  {
		if (id == null) {
			throw new MyException("UserInfo pk is not null");
		}
		
		try {
			return  userInfoMapper.selectByPrimaryKey(id);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get UserInfo fail!");
		}
	  
	  }
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param userInfo InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void addUserInfo (UserInfo userInfo)  throws MyException
	  {
		if (userInfo == null) {
			throw new MyException("userInfo is not null");
		}
		
		try {
			userInfoMapper. insertSelective( userInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param  userInfo InnoDB free: 11264 kB
	     * @throws MyException
	     */
	@Override
	public  void updateUserInfo (UserInfo userInfo)  throws MyException
	{
			  
		if (userInfo == null) {
			throw new MyException("userInfo is not null");
		}
		
		try {
			userInfoMapper. updateByPrimaryKeySelective( userInfo);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<UserInfo> queryUserInfoBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<UserInfo> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<UserInfo> pageList = userInfoMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<UserInfo> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

