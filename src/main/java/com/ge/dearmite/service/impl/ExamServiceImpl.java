package com.ge.dearmite.service.impl;


import java.util.*;
import com.ge.dearmite.po.Exam ;
import com.ge.dearmite.service.ExamService ;
import utils.PageModel;
import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

import com.ge.dearmite.dao.*;

/**
* exam
*  Created by autoGen Tools 
*/
@Service("examService")
public class ExamServiceImpl  implements  ExamService {
private static Logger logger = Logger.getLogger(ExamServiceImpl.class);

	//=============================================
	@Autowired
	private  ExamMapper examMapper;
	
	
	
	
	//=============================================
	
	 /**
	     * delete exam
	     * @param examid PKID
	     * @throws MyException
	     */
	@Override
	public void deleteExamByPK(java.lang.Integer examid) throws MyException	 
	 {
		if (examid == null) {
			throw new MyException("Exam pk is not null");
		}
		
		try {
			examMapper.deleteByPrimaryKey(examid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("delete Exam fail!");
		}
		 
	 }
	 
	 @Override
	  public Exam getExamByPK(java.lang.Integer examid) throws MyException
	  {
		if (examid == null) {
			throw new MyException("Exam pk is not null");
		}
		
		try {
			return  examMapper.selectByPrimaryKey(examid);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("get Exam fail!");
		}
	  
	  }
	 
	  /**
	     * add exam
	     * @param exam exam
	     * @throws MyException
	     */
	@Override
	public  void addExam (Exam exam)  throws MyException
	  {
		if (exam == null) {
			throw new MyException("exam is not null");
		}
		
		try {
			examMapper. insertSelective( exam);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("add catalog fail!");
		}
	  
	  }
	  
	   /**
	     * edit exam
	     * @param  exam exam
	     * @throws MyException
	     */
	@Override
	public  void updateExam (Exam exam)  throws MyException
	{
			  
		if (exam == null) {
			throw new MyException("exam is not null");
		}
		
		try {
			examMapper. updateByPrimaryKeySelective( exam);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new MyException("update catalog fail!");
		}
	}
	
	  @SuppressWarnings("unchecked")
	public PageModel<Exam> queryExamBySearch(Map<String, Object> conditionMap, Integer pageNum, Integer pageSize)
	{
		Map<String, Object> pageMap = new HashMap<String, Object>();
		Page<Exam> page = PageHelper.startPage(pageNum,pageSize);
		 
		 List<Exam> pageList = examMapper.select( null );
		 Long count = page.getTotal();
	    
		PageModel<Exam> pm = PageModel.newPageModel(pageSize, pageNum, count.intValue() );
	        //pm.setDataList( pagelist);
		//pageMap.put(PageModel.mStartRow, pm.getStartRows());
		//pageMap.put(PageModel.mPageSize, pageSize);

		//pageMap.put(PageModel.mCondition,conditionMap) ;

		pm.setDataList(pageList);

		return pm;
	  
	}



}

