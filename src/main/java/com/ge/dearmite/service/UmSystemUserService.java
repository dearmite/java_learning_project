package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmSystemUser ;
import com.common.exception.*;
import utils.PageModel;

/**
* 系统用户表
*  Created by autoGen Tools 
*/

public interface UmSystemUserService  {

	 /**
	     * delete 系统用户表
	     * @param sysUserId PKID
	     * @throws MyException
	     */
	 public void deleteUmSystemUserByPK(java.lang.String sysUserId) throws MyException ;
	 
	  /**
	     * add 系统用户表
	     * @param  umSystemUser 系统用户表
	     * @throws MyException
	     */
	  void addUmSystemUser (UmSystemUser  umSystemUser)  throws MyException;
	  
	   /**
	     * edit 系统用户表
	     * @param umSystemUser 系统用户表
	     * @throws MyException
	     */
	  void updateUmSystemUser (UmSystemUser   umSystemUser)  throws MyException;
	    /**
	     * get 系统用户表
	     * @param sysUserId  系统用户表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmSystemUser getUmSystemUserByPK(java.lang.String sysUserId) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmSystemUser> queryUmSystemUserBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}