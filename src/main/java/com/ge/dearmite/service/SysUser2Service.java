package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysUser2 ;
import com.common.exception.*;
import utils.PageModel;

/**
* 用户
*  Created by autoGen Tools 
*/

public interface SysUser2Service  {

	 /**
	     * delete 用户
	     * @param userid PKID
	     * @throws MyException
	     */
	 public void deleteSysUser2ByPK(java.lang.Integer userid) throws MyException ;
	 
	  /**
	     * add 用户
	     * @param  sysUser2 用户
	     * @throws MyException
	     */
	  void addSysUser2 (SysUser2  sysUser2)  throws MyException;
	  
	   /**
	     * edit 用户
	     * @param sysUser2 用户
	     * @throws MyException
	     */
	  void updateSysUser2 (SysUser2   sysUser2)  throws MyException;
	    /**
	     * get 用户
	     * @param userid  用户 PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysUser2 getSysUser2ByPK(java.lang.Integer userid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysUser2> querySysUser2BySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}