package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . Exam ;
import com.common.exception.*;
import utils.PageModel;

/**
* exam
*  Created by autoGen Tools 
*/

public interface ExamService  {

	 /**
	     * delete exam
	     * @param examid PKID
	     * @throws MyException
	     */
	 public void deleteExamByPK(java.lang.Integer examid) throws MyException ;
	 
	  /**
	     * add exam
	     * @param  exam exam
	     * @throws MyException
	     */
	  void addExam (Exam  exam)  throws MyException;
	  
	   /**
	     * edit exam
	     * @param exam exam
	     * @throws MyException
	     */
	  void updateExam (Exam   exam)  throws MyException;
	    /**
	     * get exam
	     * @param examid  exam PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public Exam getExamByPK(java.lang.Integer examid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<Exam> queryExamBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}