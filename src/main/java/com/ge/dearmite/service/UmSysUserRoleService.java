package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmSysUserRole ;
import com.common.exception.*;
import utils.PageModel;

/**
* 系统用户角色表
*  Created by autoGen Tools 
*/

public interface UmSysUserRoleService  {

	 /**
	     * delete 系统用户角色表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	 public void deleteUmSysUserRoleByPK(java.lang.String pkUuid) throws MyException ;
	 
	  /**
	     * add 系统用户角色表
	     * @param  umSysUserRole 系统用户角色表
	     * @throws MyException
	     */
	  void addUmSysUserRole (UmSysUserRole  umSysUserRole)  throws MyException;
	  
	   /**
	     * edit 系统用户角色表
	     * @param umSysUserRole 系统用户角色表
	     * @throws MyException
	     */
	  void updateUmSysUserRole (UmSysUserRole   umSysUserRole)  throws MyException;
	    /**
	     * get 系统用户角色表
	     * @param pkUuid  系统用户角色表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmSysUserRole getUmSysUserRoleByPK(java.lang.String pkUuid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmSysUserRole> queryUmSysUserRoleBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}