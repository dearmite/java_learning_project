package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmUserDept ;
import com.common.exception.*;
import utils.PageModel;

/**
* UM用户机构关系表
*  Created by autoGen Tools 
*/

public interface UmUserDeptService  {

	 /**
	     * delete UM用户机构关系表
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	 public void deleteUmUserDeptByPK(java.lang.String pkUuid) throws MyException ;
	 
	  /**
	     * add UM用户机构关系表
	     * @param  umUserDept UM用户机构关系表
	     * @throws MyException
	     */
	  void addUmUserDept (UmUserDept  umUserDept)  throws MyException;
	  
	   /**
	     * edit UM用户机构关系表
	     * @param umUserDept UM用户机构关系表
	     * @throws MyException
	     */
	  void updateUmUserDept (UmUserDept   umUserDept)  throws MyException;
	    /**
	     * get UM用户机构关系表
	     * @param pkUuid  UM用户机构关系表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmUserDept getUmUserDeptByPK(java.lang.String pkUuid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmUserDept> queryUmUserDeptBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}