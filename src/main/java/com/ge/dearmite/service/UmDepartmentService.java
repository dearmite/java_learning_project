package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmDepartment ;
import com.common.exception.*;
import utils.PageModel;

/**
* 机构表
*  Created by autoGen Tools 
*/

public interface UmDepartmentService  {

	 /**
	     * delete 机构表
	     * @param dptCde PKID
	     * @throws MyException
	     */
	 public void deleteUmDepartmentByPK(java.lang.String dptCde) throws MyException ;
	 
	  /**
	     * add 机构表
	     * @param  umDepartment 机构表
	     * @throws MyException
	     */
	  void addUmDepartment (UmDepartment  umDepartment)  throws MyException;
	  
	   /**
	     * edit 机构表
	     * @param umDepartment 机构表
	     * @throws MyException
	     */
	  void updateUmDepartment (UmDepartment   umDepartment)  throws MyException;
	    /**
	     * get 机构表
	     * @param dptCde  机构表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmDepartment getUmDepartmentByPK(java.lang.String dptCde) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmDepartment> queryUmDepartmentBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}