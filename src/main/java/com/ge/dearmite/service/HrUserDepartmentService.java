package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . HrUserDepartment ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface HrUserDepartmentService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param emId PKID
	     * @throws MyException
	     */
	 public void deleteHrUserDepartmentByPK(java.lang.String emId) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  hrUserDepartment InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addHrUserDepartment (HrUserDepartment  hrUserDepartment)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param hrUserDepartment InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateHrUserDepartment (HrUserDepartment   hrUserDepartment)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param emId  InnoDB free: 11264 kB PK (java.lang.String  )
	     * @throws MyException
	     */
	 public HrUserDepartment getHrUserDepartmentByPK(java.lang.String emId) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<HrUserDepartment> queryHrUserDepartmentBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}