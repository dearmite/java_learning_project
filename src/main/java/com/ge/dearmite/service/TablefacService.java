package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . Tablefac ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface TablefacService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param id PKID
	     * @throws MyException
	     */
	 public void deleteTablefacByPK(java.lang.Integer id) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  tablefac InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addTablefac (Tablefac  tablefac)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param tablefac InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateTablefac (Tablefac   tablefac)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param id  InnoDB free: 11264 kB PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public Tablefac getTablefacByPK(java.lang.Integer id) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<Tablefac> queryTablefacBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}