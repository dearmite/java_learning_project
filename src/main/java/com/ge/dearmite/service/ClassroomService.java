package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . Classroom ;
import com.common.exception.*;
import utils.PageModel;

/**
* classroom
*  Created by autoGen Tools 
*/

public interface ClassroomService  {

	 /**
	     * delete classroom
	     * @param roomid PKID
	     * @throws MyException
	     */
	 public void deleteClassroomByPK(java.lang.Integer roomid) throws MyException ;
	 
	  /**
	     * add classroom
	     * @param  classroom classroom
	     * @throws MyException
	     */
	  void addClassroom (Classroom  classroom)  throws MyException;
	  
	   /**
	     * edit classroom
	     * @param classroom classroom
	     * @throws MyException
	     */
	  void updateClassroom (Classroom   classroom)  throws MyException;
	    /**
	     * get classroom
	     * @param roomid  classroom PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public Classroom getClassroomByPK(java.lang.Integer roomid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<Classroom> queryClassroomBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}