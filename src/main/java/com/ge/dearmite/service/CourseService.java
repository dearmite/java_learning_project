package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . Course ;
import com.common.exception.*;
import utils.PageModel;

/**
* course
*  Created by autoGen Tools 
*/

public interface CourseService  {

	 /**
	     * delete course
	     * @param courseid PKID
	     * @throws MyException
	     */
	 public void deleteCourseByPK(java.lang.Integer courseid) throws MyException ;
	 
	  /**
	     * add course
	     * @param  course course
	     * @throws MyException
	     */
	  void addCourse (Course  course)  throws MyException;
	  
	   /**
	     * edit course
	     * @param course course
	     * @throws MyException
	     */
	  void updateCourse (Course   course)  throws MyException;
	    /**
	     * get course
	     * @param courseid  course PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public Course getCourseByPK(java.lang.Integer courseid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<Course> queryCourseBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}