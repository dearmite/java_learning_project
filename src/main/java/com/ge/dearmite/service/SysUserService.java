package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . SysUser ;
import com.common.exception.*;
import utils.PageModel;

/**
* 用户
*  Created by autoGen Tools 
*/

public interface SysUserService  {

	 /**
	     * delete 用户
	     * @param userid PKID
	     * @throws MyException
	     */
	 public void deleteSysUserByPK(java.lang.Integer userid) throws MyException ;
	 
	  /**
	     * add 用户
	     * @param  sysUser 用户
	     * @throws MyException
	     */
	  void addSysUser (SysUser  sysUser)  throws MyException;
	  
	   /**
	     * edit 用户
	     * @param sysUser 用户
	     * @throws MyException
	     */
	  void updateSysUser (SysUser   sysUser)  throws MyException;
	    /**
	     * get 用户
	     * @param userid  用户 PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public SysUser getSysUserByPK(java.lang.Integer userid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<SysUser> querySysUserBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}