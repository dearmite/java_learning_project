package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmResource ;
import com.common.exception.*;
import utils.PageModel;

/**
* 资源表
*  Created by autoGen Tools 
*/

public interface UmResourceService  {

	 /**
	     * delete 资源表
	     * @param resourceId PKID
	     * @throws MyException
	     */
	 public void deleteUmResourceByPK(java.lang.String resourceId) throws MyException ;
	 
	  /**
	     * add 资源表
	     * @param  umResource 资源表
	     * @throws MyException
	     */
	  void addUmResource (UmResource  umResource)  throws MyException;
	  
	   /**
	     * edit 资源表
	     * @param umResource 资源表
	     * @throws MyException
	     */
	  void updateUmResource (UmResource   umResource)  throws MyException;
	    /**
	     * get 资源表
	     * @param resourceId  资源表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmResource getUmResourceByPK(java.lang.String resourceId) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmResource> queryUmResourceBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}