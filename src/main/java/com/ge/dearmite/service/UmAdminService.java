package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmAdmin ;
import com.common.exception.*;
import utils.PageModel;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UmAdminService  {

	 /**
	     * delete InnoDB free: 11264 kB
	     * @param pkUuid PKID
	     * @throws MyException
	     */
	 public void deleteUmAdminByPK(java.lang.String pkUuid) throws MyException ;
	 
	  /**
	     * add InnoDB free: 11264 kB
	     * @param  umAdmin InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void addUmAdmin (UmAdmin  umAdmin)  throws MyException;
	  
	   /**
	     * edit InnoDB free: 11264 kB
	     * @param umAdmin InnoDB free: 11264 kB
	     * @throws MyException
	     */
	  void updateUmAdmin (UmAdmin   umAdmin)  throws MyException;
	    /**
	     * get InnoDB free: 11264 kB
	     * @param pkUuid  InnoDB free: 11264 kB PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmAdmin getUmAdminByPK(java.lang.String pkUuid) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmAdmin> queryUmAdminBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}