package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . UmSystem ;
import com.common.exception.*;
import utils.PageModel;

/**
* 系统表
*  Created by autoGen Tools 
*/

public interface UmSystemService  {

	 /**
	     * delete 系统表
	     * @param sysCode PKID
	     * @throws MyException
	     */
	 public void deleteUmSystemByPK(java.lang.String sysCode) throws MyException ;
	 
	  /**
	     * add 系统表
	     * @param  umSystem 系统表
	     * @throws MyException
	     */
	  void addUmSystem (UmSystem  umSystem)  throws MyException;
	  
	   /**
	     * edit 系统表
	     * @param umSystem 系统表
	     * @throws MyException
	     */
	  void updateUmSystem (UmSystem   umSystem)  throws MyException;
	    /**
	     * get 系统表
	     * @param sysCode  系统表 PK (java.lang.String  )
	     * @throws MyException
	     */
	 public UmSystem getUmSystemByPK(java.lang.String sysCode) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<UmSystem> queryUmSystemBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}