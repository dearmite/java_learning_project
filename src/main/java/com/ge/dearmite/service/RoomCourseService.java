package com.ge.dearmite.service;

import java.util.*;
import  com.ge.dearmite.po . RoomCourse ;
import com.common.exception.*;
import utils.PageModel;

/**
* room_course
*  Created by autoGen Tools 
*/

public interface RoomCourseService  {

	 /**
	     * delete room_course
	     * @param roomCourseId PKID
	     * @throws MyException
	     */
	 public void deleteRoomCourseByPK(java.lang.Integer roomCourseId) throws MyException ;
	 
	  /**
	     * add room_course
	     * @param  roomCourse room_course
	     * @throws MyException
	     */
	  void addRoomCourse (RoomCourse  roomCourse)  throws MyException;
	  
	   /**
	     * edit room_course
	     * @param roomCourse room_course
	     * @throws MyException
	     */
	  void updateRoomCourse (RoomCourse   roomCourse)  throws MyException;
	    /**
	     * get room_course
	     * @param roomCourseId  room_course PK (java.lang.Integer  )
	     * @throws MyException
	     */
	 public RoomCourse getRoomCourseByPK(java.lang.Integer roomCourseId) throws MyException;
	  
	  /**
	 * search paged list by query condition
	 * 
	 * @param conditionMap  query condition
	 * @param page  now page
	 * @param pageSize size perPage
	 */
	 public PageModel<RoomCourse> queryRoomCourseBySearch(Map<String, Object> conditionMap, Integer page, Integer pageSize) ;
		 
}