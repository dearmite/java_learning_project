package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmSystemUser ;
import com.ge.dearmite.service.UmSystemUserService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umSystemUser-controller", description = "系统用户表")
@Controller
@RequestMapping("/umSystemUser")
public class UmSystemUserController  {

private static Logger logger = Logger.getLogger( UmSystemUserController.class);

	//=============================================
	@Autowired
	private UmSystemUserService umSystemUserService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 系统用户表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmSystemUser" ,  method = RequestMethod.POST)
	public Response addUmSystemUser( @RequestBody Map umSystemUserMap) 	 
	 {
	 
	  UmSystemUser umSystemUser = new UmSystemUser ();
	    
		    
			
						umSystemUser . setSysUserId ( ((String) umSystemUserMap.get("sysUserId") ));
				
			
						umSystemUser . setUserCode ( ((String) umSystemUserMap.get("userCode") ));
				
			
						umSystemUser . setSysCode ( ((String) umSystemUserMap.get("sysCode") ));
				
			
						umSystemUser . setValidInd ( ((String) umSystemUserMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemUserMap.get("createdDate"));
	            umSystemUser . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemUser . setCreatedUser ( ((String) umSystemUserMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemUserMap.get("updatedDate"));
	            umSystemUser . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemUser . setUpdatedUser ( ((String) umSystemUserMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemUserService.addUmSystemUser(umSystemUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 系统用户表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmSystemUser" ,  method = RequestMethod.POST)
	public Response updateUmSystemUser(@RequestBody Map umSystemUserMap)
	{
	    UmSystemUser umSystemUser = new UmSystemUser ();
	    
		    
			
				  umSystemUser . setSysUserId ( ((String) umSystemUserMap.get("sysUserId") ));
				
			
				  umSystemUser . setUserCode ( ((String) umSystemUserMap.get("userCode") ));
				
			
				  umSystemUser . setSysCode ( ((String) umSystemUserMap.get("sysCode") ));
				
			
				  umSystemUser . setValidInd ( ((String) umSystemUserMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemUserMap.get("createdDate"));
	       umSystemUser . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemUser . setCreatedUser ( ((String) umSystemUserMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemUserMap.get("updatedDate"));
	       umSystemUser . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemUser . setUpdatedUser ( ((String) umSystemUserMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemUserService.updateUmSystemUser(umSystemUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmSystemUserByPK(java.lang.String sysUserId) throws MyException ;
	 
	@ApiOperation(value = "delete 系统用户表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUserId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmSystemUser")
	public Response deleteUmSystemUser(@RequestParam java.lang.String sysUserId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemUserService.deleteUmSystemUserByPK( sysUserId) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  系统用户表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysUserId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmSystemUser")
	public Response getUmSystemUser(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umSystemUserService.getUmSystemUserByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 系统用户表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmSystemUser")
	public TableRes queryUmSystemUser(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmSystemUser> queryData = umSystemUserService.queryUmSystemUserBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

