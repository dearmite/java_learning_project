package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.Exam ;
import com.ge.dearmite.service.ExamService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " exam-controller", description = "exam")
@Controller
@RequestMapping("/exam")
public class ExamController  {

private static Logger logger = Logger.getLogger( ExamController.class);

	//=============================================
	@Autowired
	private ExamService examService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add exam")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addExam" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/exam/addExam", description = "add exam")
	public Response addExam( @RequestBody   Exam exam ) 	 
	 {

			 exam . setExamid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  Exam exam = new Exam ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				exam . setExamid ( Integer.parseInt((String) examMap.get("examid") ));
//		//		
//	//		
//		//				exam . setExamname ( ((String) examMap.get("examname") ));
//		//		
//	//		
//		//				exam . setExamtype ( ((String) examMap.get("examtype") ));
//		//		
//	//		
//		//				exam . setExamstate ( ((String) examMap.get("examstate") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			examService.addExam(exam);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update exam")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateExam" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/exam/updateExam", description = "update exam")
	public Response updateExam(@RequestBody  Exam exam)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			examService.updateExam(exam);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteExamByPK(java.lang.Integer examid) throws MyException ;
	 
	@ApiOperation(value = "delete exam")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "examid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteExam")
	@AuthorizedAccess (roles ="admin", permissions="/exam/deleteExam", description = "delete exam")
	public Response deleteExam(@RequestParam java.lang.Integer examid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			examService.deleteExamByPK( examid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  exam")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "examid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getExam")
	@AuthorizedAccess (permissions="/exam/getExam", description = "get exam")
	public Response getExam(@RequestParam java.lang.Integer examid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(examService.getExamByPK( examid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query exam by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryExam")
	@AuthorizedAccess (permissions="/exam/queryExam", description = "query exam")
	public TableRes queryExam(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<Exam> queryData = examService.queryExamBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

