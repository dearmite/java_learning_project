package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmRoleResource ;
import com.ge.dearmite.service.UmRoleResourceService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umRoleResource-controller", description = "角色资源关系表")
@Controller
@RequestMapping("/umRoleResource")
public class UmRoleResourceController  {

private static Logger logger = Logger.getLogger( UmRoleResourceController.class);

	//=============================================
	@Autowired
	private UmRoleResourceService umRoleResourceService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 角色资源关系表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmRoleResource" ,  method = RequestMethod.POST)
	public Response addUmRoleResource( @RequestBody Map umRoleResourceMap) 	 
	 {
	 
	  UmRoleResource umRoleResource = new UmRoleResource ();
	    
		    
			
						umRoleResource . setPkUuid ( ((String) umRoleResourceMap.get("pkUuid") ));
				
			
						umRoleResource . setRoleCode ( ((String) umRoleResourceMap.get("roleCode") ));
				
			
						umRoleResource . setResourceId ( ((String) umRoleResourceMap.get("resourceId") ));
				
			
						umRoleResource . setValidInd ( ((String) umRoleResourceMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umRoleResourceMap.get("createdDate"));
	            umRoleResource . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umRoleResource . setCreatedUser ( ((String) umRoleResourceMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umRoleResourceMap.get("updatedDate"));
	            umRoleResource . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umRoleResource . setUpdatedUser ( ((String) umRoleResourceMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleResourceService.addUmRoleResource(umRoleResource);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 角色资源关系表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmRoleResource" ,  method = RequestMethod.POST)
	public Response updateUmRoleResource(@RequestBody Map umRoleResourceMap)
	{
	    UmRoleResource umRoleResource = new UmRoleResource ();
	    
		    
			
				  umRoleResource . setPkUuid ( ((String) umRoleResourceMap.get("pkUuid") ));
				
			
				  umRoleResource . setRoleCode ( ((String) umRoleResourceMap.get("roleCode") ));
				
			
				  umRoleResource . setResourceId ( ((String) umRoleResourceMap.get("resourceId") ));
				
			
				  umRoleResource . setValidInd ( ((String) umRoleResourceMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umRoleResourceMap.get("createdDate"));
	       umRoleResource . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umRoleResource . setCreatedUser ( ((String) umRoleResourceMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umRoleResourceMap.get("updatedDate"));
	       umRoleResource . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umRoleResource . setUpdatedUser ( ((String) umRoleResourceMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleResourceService.updateUmRoleResource(umRoleResource);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmRoleResourceByPK(java.lang.String pkUuid) throws MyException ;
	 
	@ApiOperation(value = "delete 角色资源关系表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmRoleResource")
	public Response deleteUmRoleResource(@RequestParam java.lang.String pkUuid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleResourceService.deleteUmRoleResourceByPK( pkUuid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  角色资源关系表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmRoleResource")
	public Response getUmRoleResource(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umRoleResourceService.getUmRoleResourceByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 角色资源关系表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmRoleResource")
	public TableRes queryUmRoleResource(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmRoleResource> queryData = umRoleResourceService.queryUmRoleResourceBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

