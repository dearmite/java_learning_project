package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.RoomCourse ;
import com.ge.dearmite.service.RoomCourseService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " roomCourse-controller", description = "room_course")
@Controller
@RequestMapping("/roomCourse")
public class RoomCourseController  {

private static Logger logger = Logger.getLogger( RoomCourseController.class);

	//=============================================
	@Autowired
	private RoomCourseService roomCourseService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add room_course")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addRoomCourse" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/roomCourse/addRoomCourse", description = "add room_course")
	public Response addRoomCourse( @RequestBody   RoomCourse roomCourse ) 	 
	 {

			 roomCourse . setRoomCourseId ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  RoomCourse roomCourse = new RoomCourse ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				roomCourse . setRoomCourseId ( Integer.parseInt((String) roomCourseMap.get("roomCourseId") ));
//		//		
//	//		
//		//				roomCourse . setCourseid ( Integer.parseInt((String) roomCourseMap.get("courseid") ));
//		//		
//	//		
//		//				roomCourse . setRoomid ( Integer.parseInt((String) roomCourseMap.get("roomid") ));
//		//		
//	//		
//		//				roomCourse . setTime ( ((String) roomCourseMap.get("time") ));
//		//		
//	//		
//		//				roomCourse . setDate ( ((String) roomCourseMap.get("date") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomCourseService.addRoomCourse(roomCourse);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update room_course")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateRoomCourse" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/roomCourse/updateRoomCourse", description = "update room_course")
	public Response updateRoomCourse(@RequestBody  RoomCourse roomCourse)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomCourseService.updateRoomCourse(roomCourse);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteRoomCourseByPK(java.lang.Integer roomCourseId) throws MyException ;
	 
	@ApiOperation(value = "delete room_course")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomCourseId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteRoomCourse")
	@AuthorizedAccess (roles ="admin", permissions="/roomCourse/deleteRoomCourse", description = "delete room_course")
	public Response deleteRoomCourse(@RequestParam java.lang.Integer roomCourseId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomCourseService.deleteRoomCourseByPK( roomCourseId) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  room_course")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomCourseId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getRoomCourse")
	@AuthorizedAccess (permissions="/roomCourse/getRoomCourse", description = "get room_course")
	public Response getRoomCourse(@RequestParam java.lang.Integer roomCourseId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(roomCourseService.getRoomCourseByPK( roomCourseId )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query room_course by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryRoomCourse")
	@AuthorizedAccess (permissions="/roomCourse/queryRoomCourse", description = "query room_course")
	public TableRes queryRoomCourse(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<RoomCourse> queryData = roomCourseService.queryRoomCourseBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

