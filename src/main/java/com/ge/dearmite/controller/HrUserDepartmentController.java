package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.HrUserDepartment ;
import com.ge.dearmite.service.HrUserDepartmentService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " hrUserDepartment-controller", description = "InnoDB free: 11264 kB")
@Controller
@RequestMapping("/hrUserDepartment")
public class HrUserDepartmentController  {

private static Logger logger = Logger.getLogger( HrUserDepartmentController.class);

	//=============================================
	@Autowired
	private HrUserDepartmentService hrUserDepartmentService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add InnoDB free: 11264 kB")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addHrUserDepartment" ,  method = RequestMethod.POST)
	public Response addHrUserDepartment( @RequestBody Map hrUserDepartmentMap) 	 
	 {
	 
	  HrUserDepartment hrUserDepartment = new HrUserDepartment ();
	    
		    
			
						hrUserDepartment . setEmId ( ((String) hrUserDepartmentMap.get("emId") ));
				
			
						hrUserDepartment . setEmployeename ( ((String) hrUserDepartmentMap.get("employeename") ));
				
			
						hrUserDepartment . setSex ( ((String) hrUserDepartmentMap.get("sex") ));
				
			
						hrUserDepartment . setCompanyid ( ((String) hrUserDepartmentMap.get("companyid") ));
				
			
						hrUserDepartment . setAccount ( ((String) hrUserDepartmentMap.get("account") ));
				
			
						hrUserDepartment . setEmail ( ((String) hrUserDepartmentMap.get("email") ));
				
			
						hrUserDepartment . setPhone ( ((String) hrUserDepartmentMap.get("phone") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			hrUserDepartmentService.addHrUserDepartment(hrUserDepartment);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update InnoDB free: 11264 kB")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateHrUserDepartment" ,  method = RequestMethod.POST)
	public Response updateHrUserDepartment(@RequestBody Map hrUserDepartmentMap)
	{
	    HrUserDepartment hrUserDepartment = new HrUserDepartment ();
	    
		    
			
				  hrUserDepartment . setEmId ( ((String) hrUserDepartmentMap.get("emId") ));
				
			
				  hrUserDepartment . setEmployeename ( ((String) hrUserDepartmentMap.get("employeename") ));
				
			
				  hrUserDepartment . setSex ( ((String) hrUserDepartmentMap.get("sex") ));
				
			
				  hrUserDepartment . setCompanyid ( ((String) hrUserDepartmentMap.get("companyid") ));
				
			
				  hrUserDepartment . setAccount ( ((String) hrUserDepartmentMap.get("account") ));
				
			
				  hrUserDepartment . setEmail ( ((String) hrUserDepartmentMap.get("email") ));
				
			
				  hrUserDepartment . setPhone ( ((String) hrUserDepartmentMap.get("phone") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			hrUserDepartmentService.updateHrUserDepartment(hrUserDepartment);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteHrUserDepartmentByPK(java.lang.String emId) throws MyException ;
	 
	@ApiOperation(value = "delete InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "emId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteHrUserDepartment")
	public Response deleteHrUserDepartment(@RequestParam java.lang.String emId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			hrUserDepartmentService.deleteHrUserDepartmentByPK( emId) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "emId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getHrUserDepartment")
	public Response getHrUserDepartment(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(hrUserDepartmentService.getHrUserDepartmentByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query InnoDB free: 11264 kB by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryHrUserDepartment")
	public TableRes queryHrUserDepartment(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<HrUserDepartment> queryData = hrUserDepartmentService.queryHrUserDepartmentBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

