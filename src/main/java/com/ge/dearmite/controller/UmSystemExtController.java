package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmSystemExt ;
import com.ge.dearmite.service.UmSystemExtService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umSystemExt-controller", description = "系统扩展属性表")
@Controller
@RequestMapping("/umSystemExt")
public class UmSystemExtController  {

private static Logger logger = Logger.getLogger( UmSystemExtController.class);

	//=============================================
	@Autowired
	private UmSystemExtService umSystemExtService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 系统扩展属性表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmSystemExt" ,  method = RequestMethod.POST)
	public Response addUmSystemExt( @RequestBody Map umSystemExtMap) 	 
	 {
	 
	  UmSystemExt umSystemExt = new UmSystemExt ();
	    
		    
			
						umSystemExt . setPkUuid ( ((String) umSystemExtMap.get("pkUuid") ));
				
			
						umSystemExt . setSysCode ( ((String) umSystemExtMap.get("sysCode") ));
				
			
						umSystemExt . setSysAttr ( ((String) umSystemExtMap.get("sysAttr") ));
				
			
						umSystemExt . setSysAttrVal ( ((String) umSystemExtMap.get("sysAttrVal") ));
				
			
						umSystemExt . setValidInd ( ((String) umSystemExtMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemExtMap.get("createdDate"));
	            umSystemExt . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemExt . setCreatedUser ( ((String) umSystemExtMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemExtMap.get("updatedDate"));
	            umSystemExt . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemExt . setUpdatedUser ( ((String) umSystemExtMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemExtService.addUmSystemExt(umSystemExt);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 系统扩展属性表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmSystemExt" ,  method = RequestMethod.POST)
	public Response updateUmSystemExt(@RequestBody Map umSystemExtMap)
	{
	    UmSystemExt umSystemExt = new UmSystemExt ();
	    
		    
			
				  umSystemExt . setPkUuid ( ((String) umSystemExtMap.get("pkUuid") ));
				
			
				  umSystemExt . setSysCode ( ((String) umSystemExtMap.get("sysCode") ));
				
			
				  umSystemExt . setSysAttr ( ((String) umSystemExtMap.get("sysAttr") ));
				
			
				  umSystemExt . setSysAttrVal ( ((String) umSystemExtMap.get("sysAttrVal") ));
				
			
				  umSystemExt . setValidInd ( ((String) umSystemExtMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemExtMap.get("createdDate"));
	       umSystemExt . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemExt . setCreatedUser ( ((String) umSystemExtMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemExtMap.get("updatedDate"));
	       umSystemExt . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemExt . setUpdatedUser ( ((String) umSystemExtMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemExtService.updateUmSystemExt(umSystemExt);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmSystemExtByPK(java.lang.String pkUuid) throws MyException ;
	 
	@ApiOperation(value = "delete 系统扩展属性表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmSystemExt")
	public Response deleteUmSystemExt(@RequestParam java.lang.String pkUuid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemExtService.deleteUmSystemExtByPK( pkUuid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  系统扩展属性表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmSystemExt")
	public Response getUmSystemExt(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umSystemExtService.getUmSystemExtByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 系统扩展属性表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmSystemExt")
	public TableRes queryUmSystemExt(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmSystemExt> queryData = umSystemExtService.queryUmSystemExtBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

