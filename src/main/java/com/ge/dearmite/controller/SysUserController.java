package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysUser ;
import com.ge.dearmite.service.SysUserService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysUser-controller", description = "用户")
@Controller
@RequestMapping("/sysUser")
public class SysUserController  {

private static Logger logger = Logger.getLogger( SysUserController.class);

	//=============================================
	@Autowired
	private SysUserService sysUserService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 用户")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysUser" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUser/addSysUser", description = "add 用户")
	public Response addSysUser( @RequestBody   SysUser sysUser ) 	 
	 {

			 sysUser . setUserid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysUser sysUser = new SysUser ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysUser . setUserid ( Integer.parseInt((String) sysUserMap.get("userid") ));
//		//		
//	//		
//		//				sysUser . setName ( ((String) sysUserMap.get("name") ));
//		//		
//	//		
//		//				sysUser . setSex ( ((String) sysUserMap.get("sex") ));
//		//		
//	//		
//		//				sysUser . setAdress ( ((String) sysUserMap.get("adress") ));
//		//		
//	//		
//		//				sysUser . setTel ( ((String) sysUserMap.get("tel") ));
//		//		
//	//		
//		//				sysUser . setUsername ( ((String) sysUserMap.get("username") ));
//		//		
//	//		
//		//				sysUser . setPassword ( ((String) sysUserMap.get("password") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserService.addSysUser(sysUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 用户")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysUser" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUser/updateSysUser", description = "update 用户")
	public Response updateSysUser(@RequestBody  SysUser sysUser)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserService.updateSysUser(sysUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysUserByPK(java.lang.Integer userid) throws MyException ;
	 
	@ApiOperation(value = "delete 用户")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysUser")
	@AuthorizedAccess (roles ="admin", permissions="/sysUser/deleteSysUser", description = "delete 用户")
	public Response deleteSysUser(@RequestParam java.lang.Integer userid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserService.deleteSysUserByPK( userid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  用户")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysUser")
	@AuthorizedAccess (permissions="/sysUser/getSysUser", description = "get 用户")
	public Response getSysUser(@RequestParam java.lang.Integer userid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysUserService.getSysUserByPK( userid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 用户 by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysUser")
	@AuthorizedAccess (permissions="/sysUser/querySysUser", description = "query 用户")
	public TableRes querySysUser(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysUser> queryData = sysUserService.querySysUserBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

