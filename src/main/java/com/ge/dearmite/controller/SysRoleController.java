package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysRole ;
import com.ge.dearmite.service.SysRoleService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysRole-controller", description = "sys_role")
@Controller
@RequestMapping("/sysRole")
public class SysRoleController  {

private static Logger logger = Logger.getLogger( SysRoleController.class);

	//=============================================
	@Autowired
	private SysRoleService sysRoleService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add sys_role")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysRole" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysRole/addSysRole", description = "add sys_role")
	public Response addSysRole( @RequestBody   SysRole sysRole ) 	 
	 {

			 sysRole . setRoleid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysRole sysRole = new SysRole ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysRole . setRoleid ( Integer.parseInt((String) sysRoleMap.get("roleid") ));
//		//		
//	//		
//		//				sysRole . setName ( ((String) sysRoleMap.get("name") ));
//		//		
//	//		
//		//				sysRole . setState ( ((String) sysRoleMap.get("state") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRoleService.addSysRole(sysRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update sys_role")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysRole" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysRole/updateSysRole", description = "update sys_role")
	public Response updateSysRole(@RequestBody  SysRole sysRole)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRoleService.updateSysRole(sysRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysRoleByPK(java.lang.Integer roleid) throws MyException ;
	 
	@ApiOperation(value = "delete sys_role")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roleid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysRole")
	@AuthorizedAccess (roles ="admin", permissions="/sysRole/deleteSysRole", description = "delete sys_role")
	public Response deleteSysRole(@RequestParam java.lang.Integer roleid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRoleService.deleteSysRoleByPK( roleid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  sys_role")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roleid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysRole")
	@AuthorizedAccess (permissions="/sysRole/getSysRole", description = "get sys_role")
	public Response getSysRole(@RequestParam java.lang.Integer roleid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysRoleService.getSysRoleByPK( roleid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query sys_role by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysRole")
	@AuthorizedAccess (permissions="/sysRole/querySysRole", description = "query sys_role")
	public TableRes querySysRole(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysRole> queryData = sysRoleService.querySysRoleBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

