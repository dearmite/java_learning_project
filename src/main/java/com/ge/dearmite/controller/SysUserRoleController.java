package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysUserRole ;
import com.ge.dearmite.service.SysUserRoleService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysUserRole-controller", description = "用户角色")
@Controller
@RequestMapping("/sysUserRole")
public class SysUserRoleController  {

private static Logger logger = Logger.getLogger( SysUserRoleController.class);

	//=============================================
	@Autowired
	private SysUserRoleService sysUserRoleService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 用户角色")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysUserRole" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUserRole/addSysUserRole", description = "add 用户角色")
	public Response addSysUserRole( @RequestBody   SysUserRole sysUserRole ) 	 
	 {

			 sysUserRole . setUserroleid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysUserRole sysUserRole = new SysUserRole ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysUserRole . setUserroleid ( Integer.parseInt((String) sysUserRoleMap.get("userroleid") ));
//		//		
//	//		
//		//				sysUserRole . setUserid ( Integer.parseInt((String) sysUserRoleMap.get("userid") ));
//		//		
//	//		
//		//				sysUserRole . setRoleid ( Integer.parseInt((String) sysUserRoleMap.get("roleid") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserRoleService.addSysUserRole(sysUserRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 用户角色")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysUserRole" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUserRole/updateSysUserRole", description = "update 用户角色")
	public Response updateSysUserRole(@RequestBody  SysUserRole sysUserRole)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserRoleService.updateSysUserRole(sysUserRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysUserRoleByPK(java.lang.Integer userroleid) throws MyException ;
	 
	@ApiOperation(value = "delete 用户角色")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userroleid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysUserRole")
	@AuthorizedAccess (roles ="admin", permissions="/sysUserRole/deleteSysUserRole", description = "delete 用户角色")
	public Response deleteSysUserRole(@RequestParam java.lang.Integer userroleid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUserRoleService.deleteSysUserRoleByPK( userroleid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  用户角色")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userroleid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysUserRole")
	@AuthorizedAccess (permissions="/sysUserRole/getSysUserRole", description = "get 用户角色")
	public Response getSysUserRole(@RequestParam java.lang.Integer userroleid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysUserRoleService.getSysUserRoleByPK( userroleid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 用户角色 by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysUserRole")
	@AuthorizedAccess (permissions="/sysUserRole/querySysUserRole", description = "query 用户角色")
	public TableRes querySysUserRole(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysUserRole> queryData = sysUserRoleService.querySysUserRoleBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

