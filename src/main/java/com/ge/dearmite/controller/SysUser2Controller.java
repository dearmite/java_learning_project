package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysUser2 ;
import com.ge.dearmite.service.SysUser2Service ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysUser2-controller", description = "用户")
@Controller
@RequestMapping("/sysUser2")
public class SysUser2Controller  {

private static Logger logger = Logger.getLogger( SysUser2Controller.class);

	//=============================================
	@Autowired
	private SysUser2Service sysUser2Service;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 用户")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysUser2" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUser2/addSysUser2", description = "add 用户")
	public Response addSysUser2( @RequestBody   SysUser2 sysUser2 ) 	 
	 {

			 sysUser2 . setUserid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysUser2 sysUser2 = new SysUser2 ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysUser2 . setUserid ( Integer.parseInt((String) sysUser2Map.get("userid") ));
//		//		
//	//		
//		//				sysUser2 . setName ( ((String) sysUser2Map.get("name") ));
//		//		
//	//		
//		//				sysUser2 . setSex ( ((String) sysUser2Map.get("sex") ));
//		//		
//	//		
//		//				sysUser2 . setAdress ( ((String) sysUser2Map.get("adress") ));
//		//		
//	//		
//		//				sysUser2 . setTel ( ((String) sysUser2Map.get("tel") ));
//		//		
//	//		
//		//				sysUser2 . setUsername ( ((String) sysUser2Map.get("username") ));
//		//		
//	//		
//		//				sysUser2 . setPassword ( ((String) sysUser2Map.get("password") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUser2Service.addSysUser2(sysUser2);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 用户")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysUser2" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysUser2/updateSysUser2", description = "update 用户")
	public Response updateSysUser2(@RequestBody  SysUser2 sysUser2)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUser2Service.updateSysUser2(sysUser2);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysUser2ByPK(java.lang.Integer userid) throws MyException ;
	 
	@ApiOperation(value = "delete 用户")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysUser2")
	@AuthorizedAccess (roles ="admin", permissions="/sysUser2/deleteSysUser2", description = "delete 用户")
	public Response deleteSysUser2(@RequestParam java.lang.Integer userid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysUser2Service.deleteSysUser2ByPK( userid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  用户")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysUser2")
	@AuthorizedAccess (permissions="/sysUser2/getSysUser2", description = "get 用户")
	public Response getSysUser2(@RequestParam java.lang.Integer userid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysUser2Service.getSysUser2ByPK( userid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 用户 by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysUser2")
	@AuthorizedAccess (permissions="/sysUser2/querySysUser2", description = "query 用户")
	public TableRes querySysUser2(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysUser2> queryData = sysUser2Service.querySysUser2BySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

