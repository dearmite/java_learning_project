package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UserInfo ;
import com.ge.dearmite.service.UserInfoService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " userInfo-controller", description = "InnoDB free: 11264 kB")
@Controller
@RequestMapping("/userInfo")
public class UserInfoController  {

private static Logger logger = Logger.getLogger( UserInfoController.class);

	//=============================================
	@Autowired
	private UserInfoService userInfoService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add InnoDB free: 11264 kB")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUserInfo" ,  method = RequestMethod.POST)
	public Response addUserInfo( @RequestBody Map userInfoMap) 	 
	 {
	 
	  UserInfo userInfo = new UserInfo ();
	    
		    
			
						userInfo . setId ( Integer.parseInt((String) userInfoMap.get("id") ));
				
			
						userInfo . setUsername ( ((String) userInfoMap.get("username") ));
				
			
						userInfo . setPassword ( ((String) userInfoMap.get("password") ));
				
			
						userInfo . setUsertype ( ((String) userInfoMap.get("usertype") ));
				
			
						userInfo . setEnabled ( Integer.parseInt((String) userInfoMap.get("enabled") ));
				
			
						userInfo . setRealname ( ((String) userInfoMap.get("realname") ));
				
			
						userInfo . setQq ( ((String) userInfoMap.get("qq") ));
				
			
						userInfo . setEmail ( ((String) userInfoMap.get("email") ));
				
			
						userInfo . setAddress ( ((String) userInfoMap.get("address") ));
				
			
						userInfo . setTel ( ((String) userInfoMap.get("tel") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			userInfoService.addUserInfo(userInfo);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update InnoDB free: 11264 kB")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUserInfo" ,  method = RequestMethod.POST)
	public Response updateUserInfo(@RequestBody Map userInfoMap)
	{
	    UserInfo userInfo = new UserInfo ();
	    
		    
			
						userInfo . setId ( Integer.parseInt((String) userInfoMap.get("id") ));
				
			
				  userInfo . setUsername ( ((String) userInfoMap.get("username") ));
				
			
				  userInfo . setPassword ( ((String) userInfoMap.get("password") ));
				
			
				  userInfo . setUsertype ( ((String) userInfoMap.get("usertype") ));
				
			
						userInfo . setEnabled ( Integer.parseInt((String) userInfoMap.get("enabled") ));
				
			
				  userInfo . setRealname ( ((String) userInfoMap.get("realname") ));
				
			
				  userInfo . setQq ( ((String) userInfoMap.get("qq") ));
				
			
				  userInfo . setEmail ( ((String) userInfoMap.get("email") ));
				
			
				  userInfo . setAddress ( ((String) userInfoMap.get("address") ));
				
			
				  userInfo . setTel ( ((String) userInfoMap.get("tel") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			userInfoService.updateUserInfo(userInfo);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUserInfoByPK(java.lang.Integer id) throws MyException ;
	 
	@ApiOperation(value = "delete InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUserInfo")
	public Response deleteUserInfo(@RequestParam java.lang.Integer id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			userInfoService.deleteUserInfoByPK( id) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUserInfo")
	public Response getUserInfo(@RequestParam java.lang.Integer id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(userInfoService.getUserInfoByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query InnoDB free: 11264 kB by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUserInfo")
	public TableRes queryUserInfo(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UserInfo> queryData = userInfoService.queryUserInfoBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

