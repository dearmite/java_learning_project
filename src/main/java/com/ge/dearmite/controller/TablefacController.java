package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.Tablefac ;
import com.ge.dearmite.service.TablefacService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " tablefac-controller", description = "InnoDB free: 11264 kB")
@Controller
@RequestMapping("/tablefac")
public class TablefacController  {

private static Logger logger = Logger.getLogger( TablefacController.class);

	//=============================================
	@Autowired
	private TablefacService tablefacService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add InnoDB free: 11264 kB")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addTablefac" ,  method = RequestMethod.POST)
	public Response addTablefac( @RequestBody Map tablefacMap) 	 
	 {
	 
	  Tablefac tablefac = new Tablefac ();
	    
		    
			
						tablefac . setId ( Integer.parseInt((String) tablefacMap.get("id") ));
				
			
						tablefac . setName ( ((String) tablefacMap.get("name") ));
				
			
					  DateFormat dateadDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date dateadValue = dateadDf.parse((String) tablefacMap.get("datead"));
	            tablefac . setDatead ( dateadValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			tablefacService.addTablefac(tablefac);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update InnoDB free: 11264 kB")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateTablefac" ,  method = RequestMethod.POST)
	public Response updateTablefac(@RequestBody Map tablefacMap)
	{
	    Tablefac tablefac = new Tablefac ();
	    
		    
			
						tablefac . setId ( Integer.parseInt((String) tablefacMap.get("id") ));
				
			
				  tablefac . setName ( ((String) tablefacMap.get("name") ));
				
			
					  DateFormat dateadDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date dateadValue = dateadDf.parse((String) tablefacMap.get("datead"));
	       tablefac . setDatead ( dateadValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			tablefacService.updateTablefac(tablefac);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteTablefacByPK(java.lang.Integer id) throws MyException ;
	 
	@ApiOperation(value = "delete InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteTablefac")
	public Response deleteTablefac(@RequestParam java.lang.Integer id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			tablefacService.deleteTablefacByPK( id) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getTablefac")
	public Response getTablefac(@RequestParam java.lang.Integer id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(tablefacService.getTablefacByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query InnoDB free: 11264 kB by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryTablefac")
	public TableRes queryTablefac(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<Tablefac> queryData = tablefacService.queryTablefacBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

