package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysRolePermission ;
import com.ge.dearmite.service.SysRolePermissionService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysRolePermission-controller", description = "角色权限")
@Controller
@RequestMapping("/sysRolePermission")
public class SysRolePermissionController  {

private static Logger logger = Logger.getLogger( SysRolePermissionController.class);

	//=============================================
	@Autowired
	private SysRolePermissionService sysRolePermissionService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 角色权限")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysRolePermission" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysRolePermission/addSysRolePermission", description = "add 角色权限")
	public Response addSysRolePermission( @RequestBody   SysRolePermission sysRolePermission ) 	 
	 {

			 sysRolePermission . setRolepermissionid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysRolePermission sysRolePermission = new SysRolePermission ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysRolePermission . setRolepermissionid ( Integer.parseInt((String) sysRolePermissionMap.get("rolepermissionid") ));
//		//		
//	//		
//		//				sysRolePermission . setRoleid ( Integer.parseInt((String) sysRolePermissionMap.get("roleid") ));
//		//		
//	//		
//		//				sysRolePermission . setPermissionid ( Integer.parseInt((String) sysRolePermissionMap.get("permissionid") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRolePermissionService.addSysRolePermission(sysRolePermission);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 角色权限")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysRolePermission" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysRolePermission/updateSysRolePermission", description = "update 角色权限")
	public Response updateSysRolePermission(@RequestBody  SysRolePermission sysRolePermission)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRolePermissionService.updateSysRolePermission(sysRolePermission);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysRolePermissionByPK(java.lang.Integer rolepermissionid) throws MyException ;
	 
	@ApiOperation(value = "delete 角色权限")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "rolepermissionid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysRolePermission")
	@AuthorizedAccess (roles ="admin", permissions="/sysRolePermission/deleteSysRolePermission", description = "delete 角色权限")
	public Response deleteSysRolePermission(@RequestParam java.lang.Integer rolepermissionid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysRolePermissionService.deleteSysRolePermissionByPK( rolepermissionid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  角色权限")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "rolepermissionid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysRolePermission")
	@AuthorizedAccess (permissions="/sysRolePermission/getSysRolePermission", description = "get 角色权限")
	public Response getSysRolePermission(@RequestParam java.lang.Integer rolepermissionid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysRolePermissionService.getSysRolePermissionByPK( rolepermissionid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 角色权限 by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysRolePermission")
	@AuthorizedAccess (permissions="/sysRolePermission/querySysRolePermission", description = "query 角色权限")
	public TableRes querySysRolePermission(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysRolePermission> queryData = sysRolePermissionService.querySysRolePermissionBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

