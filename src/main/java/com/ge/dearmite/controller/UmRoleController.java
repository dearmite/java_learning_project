package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmRole ;
import com.ge.dearmite.service.UmRoleService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umRole-controller", description = "角色表")
@Controller
@RequestMapping("/umRole")
public class UmRoleController  {

private static Logger logger = Logger.getLogger( UmRoleController.class);

	//=============================================
	@Autowired
	private UmRoleService umRoleService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 角色表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmRole" ,  method = RequestMethod.POST)
	public Response addUmRole( @RequestBody Map umRoleMap) 	 
	 {
	 
	  UmRole umRole = new UmRole ();
	    
		    
			
						umRole . setRoleCode ( ((String) umRoleMap.get("roleCode") ));
				
			
						umRole . setSysCode ( ((String) umRoleMap.get("sysCode") ));
				
			
						umRole . setRoleCname ( ((String) umRoleMap.get("roleCname") ));
				
			
						umRole . setRoleDesc ( ((String) umRoleMap.get("roleDesc") ));
				
			
						umRole . setValidInd ( ((String) umRoleMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umRoleMap.get("createdDate"));
	            umRole . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umRole . setCreatedUser ( ((String) umRoleMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umRoleMap.get("updatedDate"));
	            umRole . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umRole . setUpdatedUser ( ((String) umRoleMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleService.addUmRole(umRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 角色表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmRole" ,  method = RequestMethod.POST)
	public Response updateUmRole(@RequestBody Map umRoleMap)
	{
	    UmRole umRole = new UmRole ();
	    
		    
			
				  umRole . setRoleCode ( ((String) umRoleMap.get("roleCode") ));
				
			
				  umRole . setSysCode ( ((String) umRoleMap.get("sysCode") ));
				
			
				  umRole . setRoleCname ( ((String) umRoleMap.get("roleCname") ));
				
			
				  umRole . setRoleDesc ( ((String) umRoleMap.get("roleDesc") ));
				
			
				  umRole . setValidInd ( ((String) umRoleMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umRoleMap.get("createdDate"));
	       umRole . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umRole . setCreatedUser ( ((String) umRoleMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umRoleMap.get("updatedDate"));
	       umRole . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umRole . setUpdatedUser ( ((String) umRoleMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleService.updateUmRole(umRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmRoleByPK(java.lang.String roleCode) throws MyException ;
	 
	@ApiOperation(value = "delete 角色表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roleCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmRole")
	public Response deleteUmRole(@RequestParam java.lang.String roleCode){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umRoleService.deleteUmRoleByPK( roleCode) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  角色表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roleCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmRole")
	public Response getUmRole(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umRoleService.getUmRoleByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 角色表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmRole")
	public TableRes queryUmRole(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmRole> queryData = umRoleService.queryUmRoleBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

