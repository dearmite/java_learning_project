package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.Classroom ;
import com.ge.dearmite.service.ClassroomService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " classroom-controller", description = "classroom")
@Controller
@RequestMapping("/classroom")
public class ClassroomController  {

private static Logger logger = Logger.getLogger( ClassroomController.class);

	//=============================================
	@Autowired
	private ClassroomService classroomService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add classroom")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addClassroom" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/classroom/addClassroom", description = "add classroom")
	public Response addClassroom( @RequestBody   Classroom classroom ) 	 
	 {

			 classroom . setRoomid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  Classroom classroom = new Classroom ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				classroom . setRoomid ( Integer.parseInt((String) classroomMap.get("roomid") ));
//		//		
//	//		
//		//				classroom . setRoomname ( ((String) classroomMap.get("roomname") ));
//		//		
//	//		
//		//				classroom . setCapacity ( ((String) classroomMap.get("capacity") ));
//		//		
//	//		
//		//				classroom . setState ( ((String) classroomMap.get("state") ));
//		//		
//	//		
//		//				classroom . setDate ( ((String) classroomMap.get("date") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			classroomService.addClassroom(classroom);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update classroom")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateClassroom" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/classroom/updateClassroom", description = "update classroom")
	public Response updateClassroom(@RequestBody  Classroom classroom)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			classroomService.updateClassroom(classroom);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteClassroomByPK(java.lang.Integer roomid) throws MyException ;
	 
	@ApiOperation(value = "delete classroom")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteClassroom")
	@AuthorizedAccess (roles ="admin", permissions="/classroom/deleteClassroom", description = "delete classroom")
	public Response deleteClassroom(@RequestParam java.lang.Integer roomid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			classroomService.deleteClassroomByPK( roomid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  classroom")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getClassroom")
	@AuthorizedAccess (permissions="/classroom/getClassroom", description = "get classroom")
	public Response getClassroom(@RequestParam java.lang.Integer roomid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(classroomService.getClassroomByPK( roomid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query classroom by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryClassroom")
	@AuthorizedAccess (permissions="/classroom/queryClassroom", description = "query classroom")
	public TableRes queryClassroom(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<Classroom> queryData = classroomService.queryClassroomBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

