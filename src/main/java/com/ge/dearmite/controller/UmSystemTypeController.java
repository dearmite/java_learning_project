package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmSystemType ;
import com.ge.dearmite.service.UmSystemTypeService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umSystemType-controller", description = "InnoDB free: 11264 kB")
@Controller
@RequestMapping("/umSystemType")
public class UmSystemTypeController  {

private static Logger logger = Logger.getLogger( UmSystemTypeController.class);

	//=============================================
	@Autowired
	private UmSystemTypeService umSystemTypeService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add InnoDB free: 11264 kB")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmSystemType" ,  method = RequestMethod.POST)
	public Response addUmSystemType( @RequestBody Map umSystemTypeMap) 	 
	 {
	 
	  UmSystemType umSystemType = new UmSystemType ();
	    
		    
			
						umSystemType . setSysTypeCode ( ((String) umSystemTypeMap.get("sysTypeCode") ));
				
			
						umSystemType . setSysTypeCname ( ((String) umSystemTypeMap.get("sysTypeCname") ));
				
			
						umSystemType . setParentCode ( ((String) umSystemTypeMap.get("parentCode") ));
				
			
						umSystemType . setSysTypeDesc ( ((String) umSystemTypeMap.get("sysTypeDesc") ));
				
			
						umSystemType . setValidInd ( ((String) umSystemTypeMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemTypeMap.get("createdDate"));
	            umSystemType . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemType . setCreatedUser ( ((String) umSystemTypeMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemTypeMap.get("updatedDate"));
	            umSystemType . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystemType . setUpdatedUser ( ((String) umSystemTypeMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemTypeService.addUmSystemType(umSystemType);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update InnoDB free: 11264 kB")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmSystemType" ,  method = RequestMethod.POST)
	public Response updateUmSystemType(@RequestBody Map umSystemTypeMap)
	{
	    UmSystemType umSystemType = new UmSystemType ();
	    
		    
			
				  umSystemType . setSysTypeCode ( ((String) umSystemTypeMap.get("sysTypeCode") ));
				
			
				  umSystemType . setSysTypeCname ( ((String) umSystemTypeMap.get("sysTypeCname") ));
				
			
				  umSystemType . setParentCode ( ((String) umSystemTypeMap.get("parentCode") ));
				
			
				  umSystemType . setSysTypeDesc ( ((String) umSystemTypeMap.get("sysTypeDesc") ));
				
			
				  umSystemType . setValidInd ( ((String) umSystemTypeMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemTypeMap.get("createdDate"));
	       umSystemType . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemType . setCreatedUser ( ((String) umSystemTypeMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemTypeMap.get("updatedDate"));
	       umSystemType . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystemType . setUpdatedUser ( ((String) umSystemTypeMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemTypeService.updateUmSystemType(umSystemType);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmSystemTypeByPK(java.lang.String sysTypeCode) throws MyException ;
	 
	@ApiOperation(value = "delete InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysTypeCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmSystemType")
	public Response deleteUmSystemType(@RequestParam java.lang.String sysTypeCode){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemTypeService.deleteUmSystemTypeByPK( sysTypeCode) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysTypeCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmSystemType")
	public Response getUmSystemType(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umSystemTypeService.getUmSystemTypeByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query InnoDB free: 11264 kB by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmSystemType")
	public TableRes queryUmSystemType(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmSystemType> queryData = umSystemTypeService.queryUmSystemTypeBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

