package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmSysUserRole ;
import com.ge.dearmite.service.UmSysUserRoleService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umSysUserRole-controller", description = "系统用户角色表")
@Controller
@RequestMapping("/umSysUserRole")
public class UmSysUserRoleController  {

private static Logger logger = Logger.getLogger( UmSysUserRoleController.class);

	//=============================================
	@Autowired
	private UmSysUserRoleService umSysUserRoleService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 系统用户角色表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmSysUserRole" ,  method = RequestMethod.POST)
	public Response addUmSysUserRole( @RequestBody Map umSysUserRoleMap) 	 
	 {
	 
	  UmSysUserRole umSysUserRole = new UmSysUserRole ();
	    
		    
			
						umSysUserRole . setPkUuid ( ((String) umSysUserRoleMap.get("pkUuid") ));
				
			
						umSysUserRole . setSysUserId ( ((String) umSysUserRoleMap.get("sysUserId") ));
				
			
						umSysUserRole . setRoleCode ( ((String) umSysUserRoleMap.get("roleCode") ));
				
			
						umSysUserRole . setValidInd ( ((String) umSysUserRoleMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSysUserRoleMap.get("createdDate"));
	            umSysUserRole . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSysUserRole . setCreatedUser ( ((String) umSysUserRoleMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSysUserRoleMap.get("updatedDate"));
	            umSysUserRole . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSysUserRole . setUpdatedUser ( ((String) umSysUserRoleMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSysUserRoleService.addUmSysUserRole(umSysUserRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 系统用户角色表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmSysUserRole" ,  method = RequestMethod.POST)
	public Response updateUmSysUserRole(@RequestBody Map umSysUserRoleMap)
	{
	    UmSysUserRole umSysUserRole = new UmSysUserRole ();
	    
		    
			
				  umSysUserRole . setPkUuid ( ((String) umSysUserRoleMap.get("pkUuid") ));
				
			
				  umSysUserRole . setSysUserId ( ((String) umSysUserRoleMap.get("sysUserId") ));
				
			
				  umSysUserRole . setRoleCode ( ((String) umSysUserRoleMap.get("roleCode") ));
				
			
				  umSysUserRole . setValidInd ( ((String) umSysUserRoleMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSysUserRoleMap.get("createdDate"));
	       umSysUserRole . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSysUserRole . setCreatedUser ( ((String) umSysUserRoleMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSysUserRoleMap.get("updatedDate"));
	       umSysUserRole . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSysUserRole . setUpdatedUser ( ((String) umSysUserRoleMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSysUserRoleService.updateUmSysUserRole(umSysUserRole);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmSysUserRoleByPK(java.lang.String pkUuid) throws MyException ;
	 
	@ApiOperation(value = "delete 系统用户角色表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmSysUserRole")
	public Response deleteUmSysUserRole(@RequestParam java.lang.String pkUuid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSysUserRoleService.deleteUmSysUserRoleByPK( pkUuid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  系统用户角色表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmSysUserRole")
	public Response getUmSysUserRole(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umSysUserRoleService.getUmSysUserRoleByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 系统用户角色表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmSysUserRole")
	public TableRes queryUmSysUserRole(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmSysUserRole> queryData = umSysUserRoleService.queryUmSysUserRoleBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

