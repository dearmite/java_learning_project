package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmUser ;
import com.ge.dearmite.service.UmUserService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umUser-controller", description = "UM用户表")
@Controller
@RequestMapping("/umUser")
public class UmUserController  {

private static Logger logger = Logger.getLogger( UmUserController.class);

	//=============================================
	@Autowired
	private UmUserService umUserService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add UM用户表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmUser" ,  method = RequestMethod.POST)
	public Response addUmUser( @RequestBody Map umUserMap) 	 
	 {
	 
	  UmUser umUser = new UmUser ();
	    
		    
			
						umUser . setUserCode ( ((String) umUserMap.get("userCode") ));
				
			
						umUser . setUserCname ( ((String) umUserMap.get("userCname") ));
				
			
						umUser . setUserEname ( ((String) umUserMap.get("userEname") ));
				
			
						umUser . setSex ( ((String) umUserMap.get("sex") ));
				
			
						umUser . setUserTel ( ((String) umUserMap.get("userTel") ));
				
			
						umUser . setStatus ( ((String) umUserMap.get("status") ));
				
			
						umUser . setPwd ( ((String) umUserMap.get("pwd") ));
				
			
					  DateFormat pwdUpdateTimeDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date pwdUpdateTimeValue = pwdUpdateTimeDf.parse((String) umUserMap.get("pwdUpdateTime"));
	            umUser . setPwdUpdateTime ( pwdUpdateTimeValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umUserMap.get("createdDate"));
	            umUser . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umUser . setCreatedUser ( ((String) umUserMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umUserMap.get("updatedDate"));
	            umUser . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umUser . setUpdatedUser ( ((String) umUserMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserService.addUmUser(umUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update UM用户表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmUser" ,  method = RequestMethod.POST)
	public Response updateUmUser(@RequestBody Map umUserMap)
	{
	    UmUser umUser = new UmUser ();
	    
		    
			
				  umUser . setUserCode ( ((String) umUserMap.get("userCode") ));
				
			
				  umUser . setUserCname ( ((String) umUserMap.get("userCname") ));
				
			
				  umUser . setUserEname ( ((String) umUserMap.get("userEname") ));
				
			
				  umUser . setSex ( ((String) umUserMap.get("sex") ));
				
			
				  umUser . setUserTel ( ((String) umUserMap.get("userTel") ));
				
			
				  umUser . setStatus ( ((String) umUserMap.get("status") ));
				
			
				  umUser . setPwd ( ((String) umUserMap.get("pwd") ));
				
			
					  DateFormat pwdUpdateTimeDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date pwdUpdateTimeValue = pwdUpdateTimeDf.parse((String) umUserMap.get("pwdUpdateTime"));
	       umUser . setPwdUpdateTime ( pwdUpdateTimeValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umUserMap.get("createdDate"));
	       umUser . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umUser . setCreatedUser ( ((String) umUserMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umUserMap.get("updatedDate"));
	       umUser . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umUser . setUpdatedUser ( ((String) umUserMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserService.updateUmUser(umUser);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmUserByPK(java.lang.String userCode) throws MyException ;
	 
	@ApiOperation(value = "delete UM用户表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmUser")
	public Response deleteUmUser(@RequestParam java.lang.String userCode){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserService.deleteUmUserByPK( userCode) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  UM用户表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "userCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmUser")
	public Response getUmUser(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umUserService.getUmUserByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query UM用户表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmUser")
	public TableRes queryUmUser(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmUser> queryData = umUserService.queryUmUserBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

