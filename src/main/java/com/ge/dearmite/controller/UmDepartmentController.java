package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmDepartment ;
import com.ge.dearmite.service.UmDepartmentService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umDepartment-controller", description = "机构表")
@Controller
@RequestMapping("/umDepartment")
public class UmDepartmentController  {

private static Logger logger = Logger.getLogger( UmDepartmentController.class);

	//=============================================
	@Autowired
	private UmDepartmentService umDepartmentService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 机构表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmDepartment" ,  method = RequestMethod.POST)
	public Response addUmDepartment( @RequestBody Map umDepartmentMap) 	 
	 {
	 
	  UmDepartment umDepartment = new UmDepartment ();
	    
		    
			
						umDepartment . setDptCde ( ((String) umDepartmentMap.get("dptCde") ));
				
			
						umDepartment . setDptCnm ( ((String) umDepartmentMap.get("dptCnm") ));
				
			
						umDepartment . setCDptEnm ( ((String) umDepartmentMap.get("cDptEnm") ));
				
			
						umDepartment . setCSnrDpt ( ((String) umDepartmentMap.get("cSnrDpt") ));
				
			
						umDepartment . setNDptLevl ( ((String) umDepartmentMap.get("nDptLevl") ));
				
			
						umDepartment . setDptRelCde ( ((String) umDepartmentMap.get("dptRelCde") ));
				
			
						umDepartment . setInsprmtNo ( ((String) umDepartmentMap.get("insprmtNo") ));
				
			
						umDepartment . setRptAddr ( ((String) umDepartmentMap.get("rptAddr") ));
				
			
						umDepartment . setRptTel ( ((String) umDepartmentMap.get("rptTel") ));
				
			
						umDepartment . setAlarmMrk ( ((String) umDepartmentMap.get("alarmMrk") ));
				
			
						umDepartment . setDepartmentMrk ( ((String) umDepartmentMap.get("departmentMrk") ));
				
			
						umDepartment . setFax ( ((String) umDepartmentMap.get("fax") ));
				
			
						umDepartment . setTel ( ((String) umDepartmentMap.get("tel") ));
				
			
						umDepartment . setConsTel ( ((String) umDepartmentMap.get("consTel") ));
				
			
						umDepartment . setDptCaddr ( ((String) umDepartmentMap.get("dptCaddr") ));
				
			
						umDepartment . setDptEaddr ( ((String) umDepartmentMap.get("dptEaddr") ));
				
			
						umDepartment . setZipCde ( ((String) umDepartmentMap.get("zipCde") ));
				
			
						umDepartment . setDptAbr ( ((String) umDepartmentMap.get("dptAbr") ));
				
			
						umDepartment . setDptSerno ( ((String) umDepartmentMap.get("dptSerno") ));
				
			
						umDepartment . setCtctCde ( ((String) umDepartmentMap.get("ctctCde") ));
				
			
						umDepartment . setInterCde ( ((String) umDepartmentMap.get("interCde") ));
				
			
					  DateFormat abdTmDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date abdTmValue = abdTmDf.parse((String) umDepartmentMap.get("abdTm"));
	            umDepartment . setAbdTm ( abdTmValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
					  DateFormat fndTmDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date fndTmValue = fndTmDf.parse((String) umDepartmentMap.get("fndTm"));
	            umDepartment . setFndTm ( fndTmValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umDepartment . setTaxrgstNo ( ((String) umDepartmentMap.get("taxrgstNo") ));
				
			
						umDepartment . setLcnAbr ( ((String) umDepartmentMap.get("lcnAbr") ));
				
			
						umDepartment . setBnsrgstNo ( ((String) umDepartmentMap.get("bnsrgstNo") ));
				
			
						umDepartment . setValidInd ( ((String) umDepartmentMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umDepartmentMap.get("createdDate"));
	            umDepartment . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umDepartment . setCreatedUser ( ((String) umDepartmentMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umDepartmentMap.get("updatedDate"));
	            umDepartment . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umDepartment . setUpdatedUser ( ((String) umDepartmentMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umDepartmentService.addUmDepartment(umDepartment);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 机构表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmDepartment" ,  method = RequestMethod.POST)
	public Response updateUmDepartment(@RequestBody Map umDepartmentMap)
	{
	    UmDepartment umDepartment = new UmDepartment ();
	    
		    
			
				  umDepartment . setDptCde ( ((String) umDepartmentMap.get("dptCde") ));
				
			
				  umDepartment . setDptCnm ( ((String) umDepartmentMap.get("dptCnm") ));
				
			
				  umDepartment . setCDptEnm ( ((String) umDepartmentMap.get("cDptEnm") ));
				
			
				  umDepartment . setCSnrDpt ( ((String) umDepartmentMap.get("cSnrDpt") ));
				
			
				  umDepartment . setNDptLevl ( ((String) umDepartmentMap.get("nDptLevl") ));
				
			
				  umDepartment . setDptRelCde ( ((String) umDepartmentMap.get("dptRelCde") ));
				
			
				  umDepartment . setInsprmtNo ( ((String) umDepartmentMap.get("insprmtNo") ));
				
			
				  umDepartment . setRptAddr ( ((String) umDepartmentMap.get("rptAddr") ));
				
			
				  umDepartment . setRptTel ( ((String) umDepartmentMap.get("rptTel") ));
				
			
				  umDepartment . setAlarmMrk ( ((String) umDepartmentMap.get("alarmMrk") ));
				
			
				  umDepartment . setDepartmentMrk ( ((String) umDepartmentMap.get("departmentMrk") ));
				
			
				  umDepartment . setFax ( ((String) umDepartmentMap.get("fax") ));
				
			
				  umDepartment . setTel ( ((String) umDepartmentMap.get("tel") ));
				
			
				  umDepartment . setConsTel ( ((String) umDepartmentMap.get("consTel") ));
				
			
				  umDepartment . setDptCaddr ( ((String) umDepartmentMap.get("dptCaddr") ));
				
			
				  umDepartment . setDptEaddr ( ((String) umDepartmentMap.get("dptEaddr") ));
				
			
				  umDepartment . setZipCde ( ((String) umDepartmentMap.get("zipCde") ));
				
			
				  umDepartment . setDptAbr ( ((String) umDepartmentMap.get("dptAbr") ));
				
			
				  umDepartment . setDptSerno ( ((String) umDepartmentMap.get("dptSerno") ));
				
			
				  umDepartment . setCtctCde ( ((String) umDepartmentMap.get("ctctCde") ));
				
			
				  umDepartment . setInterCde ( ((String) umDepartmentMap.get("interCde") ));
				
			
					  DateFormat abdTmDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date abdTmValue = abdTmDf.parse((String) umDepartmentMap.get("abdTm"));
	       umDepartment . setAbdTm ( abdTmValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
					  DateFormat fndTmDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date fndTmValue = fndTmDf.parse((String) umDepartmentMap.get("fndTm"));
	       umDepartment . setFndTm ( fndTmValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umDepartment . setTaxrgstNo ( ((String) umDepartmentMap.get("taxrgstNo") ));
				
			
				  umDepartment . setLcnAbr ( ((String) umDepartmentMap.get("lcnAbr") ));
				
			
				  umDepartment . setBnsrgstNo ( ((String) umDepartmentMap.get("bnsrgstNo") ));
				
			
				  umDepartment . setValidInd ( ((String) umDepartmentMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umDepartmentMap.get("createdDate"));
	       umDepartment . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umDepartment . setCreatedUser ( ((String) umDepartmentMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umDepartmentMap.get("updatedDate"));
	       umDepartment . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umDepartment . setUpdatedUser ( ((String) umDepartmentMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umDepartmentService.updateUmDepartment(umDepartment);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmDepartmentByPK(java.lang.String dptCde) throws MyException ;
	 
	@ApiOperation(value = "delete 机构表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "dptCde", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmDepartment")
	public Response deleteUmDepartment(@RequestParam java.lang.String dptCde){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umDepartmentService.deleteUmDepartmentByPK( dptCde) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  机构表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "dptCde", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmDepartment")
	public Response getUmDepartment(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umDepartmentService.getUmDepartmentByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 机构表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmDepartment")
	public TableRes queryUmDepartment(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmDepartment> queryData = umDepartmentService.queryUmDepartmentBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

