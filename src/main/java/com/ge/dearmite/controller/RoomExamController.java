package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.RoomExam ;
import com.ge.dearmite.service.RoomExamService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " roomExam-controller", description = "room_exam")
@Controller
@RequestMapping("/roomExam")
public class RoomExamController  {

private static Logger logger = Logger.getLogger( RoomExamController.class);

	//=============================================
	@Autowired
	private RoomExamService roomExamService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add room_exam")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addRoomExam" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/roomExam/addRoomExam", description = "add room_exam")
	public Response addRoomExam( @RequestBody   RoomExam roomExam ) 	 
	 {

			 roomExam . setRoomExamId ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  RoomExam roomExam = new RoomExam ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				roomExam . setRoomExamId ( Integer.parseInt((String) roomExamMap.get("roomExamId") ));
//		//		
//	//		
//		//				roomExam . setExamid ( ((String) roomExamMap.get("examid") ));
//		//		
//	//		
//		//				roomExam . setRoomld ( ((String) roomExamMap.get("roomld") ));
//		//		
//	//		
//		//				roomExam . setDate ( ((String) roomExamMap.get("date") ));
//		//		
//	//		
//		//				roomExam . setTime ( ((String) roomExamMap.get("time") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomExamService.addRoomExam(roomExam);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update room_exam")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateRoomExam" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/roomExam/updateRoomExam", description = "update room_exam")
	public Response updateRoomExam(@RequestBody  RoomExam roomExam)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomExamService.updateRoomExam(roomExam);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteRoomExamByPK(java.lang.Integer roomExamId) throws MyException ;
	 
	@ApiOperation(value = "delete room_exam")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomExamId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteRoomExam")
	@AuthorizedAccess (roles ="admin", permissions="/roomExam/deleteRoomExam", description = "delete room_exam")
	public Response deleteRoomExam(@RequestParam java.lang.Integer roomExamId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			roomExamService.deleteRoomExamByPK( roomExamId) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  room_exam")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "roomExamId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getRoomExam")
	@AuthorizedAccess (permissions="/roomExam/getRoomExam", description = "get room_exam")
	public Response getRoomExam(@RequestParam java.lang.Integer roomExamId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(roomExamService.getRoomExamByPK( roomExamId )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query room_exam by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryRoomExam")
	@AuthorizedAccess (permissions="/roomExam/queryRoomExam", description = "query room_exam")
	public TableRes queryRoomExam(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<RoomExam> queryData = roomExamService.queryRoomExamBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

