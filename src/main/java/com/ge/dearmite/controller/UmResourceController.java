package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmResource ;
import com.ge.dearmite.service.UmResourceService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umResource-controller", description = "资源表")
@Controller
@RequestMapping("/umResource")
public class UmResourceController  {

private static Logger logger = Logger.getLogger( UmResourceController.class);

	//=============================================
	@Autowired
	private UmResourceService umResourceService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 资源表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmResource" ,  method = RequestMethod.POST)
	public Response addUmResource( @RequestBody Map umResourceMap) 	 
	 {
	 
	  UmResource umResource = new UmResource ();
	    
		    
			
						umResource . setResourceId ( ((String) umResourceMap.get("resourceId") ));
				
			
						umResource . setResourceName ( ((String) umResourceMap.get("resourceName") ));
				
			
						umResource . setResourceType ( ((String) umResourceMap.get("resourceType") ));
				
			
						umResource . setSysCode ( ((String) umResourceMap.get("sysCode") ));
				
			
						umResource . setParentId ( ((String) umResourceMap.get("parentId") ));
				
			
						umResource . setUrl ( ((String) umResourceMap.get("url") ));
				
			
						umResource . setMenuIndex ( ((String) umResourceMap.get("menuIndex") ));
				
			
						umResource . setValidInd ( ((String) umResourceMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umResourceMap.get("createdDate"));
	            umResource . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umResource . setCreatedUser ( ((String) umResourceMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umResourceMap.get("updatedDate"));
	            umResource . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umResource . setUpdatedUser ( ((String) umResourceMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umResourceService.addUmResource(umResource);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 资源表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmResource" ,  method = RequestMethod.POST)
	public Response updateUmResource(@RequestBody Map umResourceMap)
	{
	    UmResource umResource = new UmResource ();
	    
		    
			
				  umResource . setResourceId ( ((String) umResourceMap.get("resourceId") ));
				
			
				  umResource . setResourceName ( ((String) umResourceMap.get("resourceName") ));
				
			
				  umResource . setResourceType ( ((String) umResourceMap.get("resourceType") ));
				
			
				  umResource . setSysCode ( ((String) umResourceMap.get("sysCode") ));
				
			
				  umResource . setParentId ( ((String) umResourceMap.get("parentId") ));
				
			
				  umResource . setUrl ( ((String) umResourceMap.get("url") ));
				
			
				  umResource . setMenuIndex ( ((String) umResourceMap.get("menuIndex") ));
				
			
				  umResource . setValidInd ( ((String) umResourceMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umResourceMap.get("createdDate"));
	       umResource . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umResource . setCreatedUser ( ((String) umResourceMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umResourceMap.get("updatedDate"));
	       umResource . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umResource . setUpdatedUser ( ((String) umResourceMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umResourceService.updateUmResource(umResource);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmResourceByPK(java.lang.String resourceId) throws MyException ;
	 
	@ApiOperation(value = "delete 资源表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "resourceId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmResource")
	public Response deleteUmResource(@RequestParam java.lang.String resourceId){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umResourceService.deleteUmResourceByPK( resourceId) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  资源表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "resourceId", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmResource")
	public Response getUmResource(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umResourceService.getUmResourceByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 资源表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmResource")
	public TableRes queryUmResource(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmResource> queryData = umResourceService.queryUmResourceBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

