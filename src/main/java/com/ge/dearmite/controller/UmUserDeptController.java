package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmUserDept ;
import com.ge.dearmite.service.UmUserDeptService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umUserDept-controller", description = "UM用户机构关系表")
@Controller
@RequestMapping("/umUserDept")
public class UmUserDeptController  {

private static Logger logger = Logger.getLogger( UmUserDeptController.class);

	//=============================================
	@Autowired
	private UmUserDeptService umUserDeptService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add UM用户机构关系表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmUserDept" ,  method = RequestMethod.POST)
	public Response addUmUserDept( @RequestBody Map umUserDeptMap) 	 
	 {
	 
	  UmUserDept umUserDept = new UmUserDept ();
	    
		    
			
						umUserDept . setPkUuid ( ((String) umUserDeptMap.get("pkUuid") ));
				
			
						umUserDept . setDeptCode ( ((String) umUserDeptMap.get("deptCode") ));
				
			
						umUserDept . setUserCode ( ((String) umUserDeptMap.get("userCode") ));
				
			
						umUserDept . setDefaultFlag ( ((String) umUserDeptMap.get("defaultFlag") ));
				
			
						umUserDept . setValidInd ( ((String) umUserDeptMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umUserDeptMap.get("createdDate"));
	            umUserDept . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umUserDept . setCreatedUser ( ((String) umUserDeptMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umUserDeptMap.get("updatedDate"));
	            umUserDept . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umUserDept . setUpdatedUser ( ((String) umUserDeptMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserDeptService.addUmUserDept(umUserDept);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update UM用户机构关系表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmUserDept" ,  method = RequestMethod.POST)
	public Response updateUmUserDept(@RequestBody Map umUserDeptMap)
	{
	    UmUserDept umUserDept = new UmUserDept ();
	    
		    
			
				  umUserDept . setPkUuid ( ((String) umUserDeptMap.get("pkUuid") ));
				
			
				  umUserDept . setDeptCode ( ((String) umUserDeptMap.get("deptCode") ));
				
			
				  umUserDept . setUserCode ( ((String) umUserDeptMap.get("userCode") ));
				
			
				  umUserDept . setDefaultFlag ( ((String) umUserDeptMap.get("defaultFlag") ));
				
			
				  umUserDept . setValidInd ( ((String) umUserDeptMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umUserDeptMap.get("createdDate"));
	       umUserDept . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umUserDept . setCreatedUser ( ((String) umUserDeptMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umUserDeptMap.get("updatedDate"));
	       umUserDept . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umUserDept . setUpdatedUser ( ((String) umUserDeptMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserDeptService.updateUmUserDept(umUserDept);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmUserDeptByPK(java.lang.String pkUuid) throws MyException ;
	 
	@ApiOperation(value = "delete UM用户机构关系表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmUserDept")
	public Response deleteUmUserDept(@RequestParam java.lang.String pkUuid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umUserDeptService.deleteUmUserDeptByPK( pkUuid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  UM用户机构关系表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmUserDept")
	public Response getUmUserDept(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umUserDeptService.getUmUserDeptByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query UM用户机构关系表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmUserDept")
	public TableRes queryUmUserDept(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmUserDept> queryData = umUserDeptService.queryUmUserDeptBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

