package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmAdmin ;
import com.ge.dearmite.service.UmAdminService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umAdmin-controller", description = "InnoDB free: 11264 kB")
@Controller
@RequestMapping("/umAdmin")
public class UmAdminController  {

private static Logger logger = Logger.getLogger( UmAdminController.class);

	//=============================================
	@Autowired
	private UmAdminService umAdminService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add InnoDB free: 11264 kB")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmAdmin" ,  method = RequestMethod.POST)
	public Response addUmAdmin( @RequestBody Map umAdminMap) 	 
	 {
	 
	  UmAdmin umAdmin = new UmAdmin ();
	    
		    
			
						umAdmin . setPkUuid ( ((String) umAdminMap.get("pkUuid") ));
				
			
						umAdmin . setUserCode ( ((String) umAdminMap.get("userCode") ));
				
			
						umAdmin . setSysCode ( ((String) umAdminMap.get("sysCode") ));
				
			
						umAdmin . setAdminLevel ( ((String) umAdminMap.get("adminLevel") ));
				
			
						umAdmin . setValidInd ( ((String) umAdminMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umAdminMap.get("createdDate"));
	            umAdmin . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umAdmin . setCreatedUser ( ((String) umAdminMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umAdminMap.get("updatedDate"));
	            umAdmin . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umAdmin . setUpdatedUser ( ((String) umAdminMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umAdminService.addUmAdmin(umAdmin);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update InnoDB free: 11264 kB")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmAdmin" ,  method = RequestMethod.POST)
	public Response updateUmAdmin(@RequestBody Map umAdminMap)
	{
	    UmAdmin umAdmin = new UmAdmin ();
	    
		    
			
				  umAdmin . setPkUuid ( ((String) umAdminMap.get("pkUuid") ));
				
			
				  umAdmin . setUserCode ( ((String) umAdminMap.get("userCode") ));
				
			
				  umAdmin . setSysCode ( ((String) umAdminMap.get("sysCode") ));
				
			
				  umAdmin . setAdminLevel ( ((String) umAdminMap.get("adminLevel") ));
				
			
				  umAdmin . setValidInd ( ((String) umAdminMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umAdminMap.get("createdDate"));
	       umAdmin . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umAdmin . setCreatedUser ( ((String) umAdminMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umAdminMap.get("updatedDate"));
	       umAdmin . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umAdmin . setUpdatedUser ( ((String) umAdminMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umAdminService.updateUmAdmin(umAdmin);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmAdminByPK(java.lang.String pkUuid) throws MyException ;
	 
	@ApiOperation(value = "delete InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmAdmin")
	public Response deleteUmAdmin(@RequestParam java.lang.String pkUuid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umAdminService.deleteUmAdminByPK( pkUuid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  InnoDB free: 11264 kB")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "pkUuid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmAdmin")
	public Response getUmAdmin(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umAdminService.getUmAdminByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query InnoDB free: 11264 kB by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmAdmin")
	public TableRes queryUmAdmin(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmAdmin> queryData = umAdminService.queryUmAdminBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

