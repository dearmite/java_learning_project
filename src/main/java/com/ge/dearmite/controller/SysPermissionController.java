package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.SysPermission ;
import com.ge.dearmite.service.SysPermissionService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " sysPermission-controller", description = "权限")
@Controller
@RequestMapping("/sysPermission")
public class SysPermissionController  {

private static Logger logger = Logger.getLogger( SysPermissionController.class);

	//=============================================
	@Autowired
	private SysPermissionService sysPermissionService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 权限")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addSysPermission" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysPermission/addSysPermission", description = "add 权限")
	public Response addSysPermission( @RequestBody   SysPermission sysPermission ) 	 
	 {

			 sysPermission . setPermissionid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  SysPermission sysPermission = new SysPermission ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				sysPermission . setPermissionid ( Integer.parseInt((String) sysPermissionMap.get("permissionid") ));
//		//		
//	//		
//		//				sysPermission . setPid ( Integer.parseInt((String) sysPermissionMap.get("pid") ));
//		//		
//	//		
//		//				sysPermission . setName ( ((String) sysPermissionMap.get("name") ));
//		//		
//	//		
//		//				sysPermission . setType ( ((String) sysPermissionMap.get("type") ));
//		//		
//	//		
//		//				sysPermission . setUrl ( ((String) sysPermissionMap.get("url") ));
//		//		
//	//		
//		//				sysPermission . setState ( ((String) sysPermissionMap.get("state") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysPermissionService.addSysPermission(sysPermission);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 权限")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateSysPermission" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/sysPermission/updateSysPermission", description = "update 权限")
	public Response updateSysPermission(@RequestBody  SysPermission sysPermission)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysPermissionService.updateSysPermission(sysPermission);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteSysPermissionByPK(java.lang.Integer permissionid) throws MyException ;
	 
	@ApiOperation(value = "delete 权限")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "permissionid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteSysPermission")
	@AuthorizedAccess (roles ="admin", permissions="/sysPermission/deleteSysPermission", description = "delete 权限")
	public Response deleteSysPermission(@RequestParam java.lang.Integer permissionid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			sysPermissionService.deleteSysPermissionByPK( permissionid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  权限")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "permissionid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getSysPermission")
	@AuthorizedAccess (permissions="/sysPermission/getSysPermission", description = "get 权限")
	public Response getSysPermission(@RequestParam java.lang.Integer permissionid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(sysPermissionService.getSysPermissionByPK( permissionid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 权限 by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/querySysPermission")
	@AuthorizedAccess (permissions="/sysPermission/querySysPermission", description = "query 权限")
	public TableRes querySysPermission(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<SysPermission> queryData = sysPermissionService.querySysPermissionBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

