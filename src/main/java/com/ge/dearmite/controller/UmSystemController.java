package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.UmSystem ;
import com.ge.dearmite.service.UmSystemService ;
import utils.PageModel;
import utils.Response;
import utils.ResultHttpStatus;
import utils.TableRes;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " umSystem-controller", description = "系统表")
@Controller
@RequestMapping("/umSystem")
public class UmSystemController  {

private static Logger logger = Logger.getLogger( UmSystemController.class);

	//=============================================
	@Autowired
	private UmSystemService umSystemService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add 系统表")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addUmSystem" ,  method = RequestMethod.POST)
	public Response addUmSystem( @RequestBody Map umSystemMap) 	 
	 {
	 
	  UmSystem umSystem = new UmSystem ();
	    
		    
			
						umSystem . setSysCode ( ((String) umSystemMap.get("sysCode") ));
				
			
						umSystem . setSysTypeCode ( ((String) umSystemMap.get("sysTypeCode") ));
				
			
						umSystem . setSysCname ( ((String) umSystemMap.get("sysCname") ));
				
			
						umSystem . setSysEname ( ((String) umSystemMap.get("sysEname") ));
				
			
						umSystem . setSysUrl ( ((String) umSystemMap.get("sysUrl") ));
				
			
						umSystem . setSysDesc ( ((String) umSystemMap.get("sysDesc") ));
				
			
						umSystem . setValidInd ( ((String) umSystemMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemMap.get("createdDate"));
	            umSystem . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystem . setCreatedUser ( ((String) umSystemMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemMap.get("updatedDate"));
	            umSystem . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
						umSystem . setUpdatedUser ( ((String) umSystemMap.get("updatedUser") ));
				
		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemService.addUmSystem(umSystem);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update 系统表")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateUmSystem" ,  method = RequestMethod.POST)
	public Response updateUmSystem(@RequestBody Map umSystemMap)
	{
	    UmSystem umSystem = new UmSystem ();
	    
		    
			
				  umSystem . setSysCode ( ((String) umSystemMap.get("sysCode") ));
				
			
				  umSystem . setSysTypeCode ( ((String) umSystemMap.get("sysTypeCode") ));
				
			
				  umSystem . setSysCname ( ((String) umSystemMap.get("sysCname") ));
				
			
				  umSystem . setSysEname ( ((String) umSystemMap.get("sysEname") ));
				
			
				  umSystem . setSysUrl ( ((String) umSystemMap.get("sysUrl") ));
				
			
				  umSystem . setSysDesc ( ((String) umSystemMap.get("sysDesc") ));
				
			
				  umSystem . setValidInd ( ((String) umSystemMap.get("validInd") ));
				
			
					  DateFormat createdDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date createdDateValue = createdDateDf.parse((String) umSystemMap.get("createdDate"));
	       umSystem . setCreatedDate ( createdDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystem . setCreatedUser ( ((String) umSystemMap.get("createdUser") ));
				
			
					  DateFormat updatedDateDf = new SimpleDateFormat("yyyy-mm-dd");
	        try {
	            Date updatedDateValue = updatedDateDf.parse((String) umSystemMap.get("updatedDate"));
	       umSystem . setUpdatedDate ( updatedDateValue );
	        } catch (ParseException e) {
	            e.printStackTrace();
	        } 
		      
				
			
				  umSystem . setUpdatedUser ( ((String) umSystemMap.get("updatedUser") ));
				
		
	
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemService.updateUmSystem(umSystem);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteUmSystemByPK(java.lang.String sysCode) throws MyException ;
	 
	@ApiOperation(value = "delete 系统表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteUmSystem")
	public Response deleteUmSystem(@RequestParam java.lang.String sysCode){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			umSystemService.deleteUmSystemByPK( sysCode) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  系统表")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "sysCode", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.String")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getUmSystem")
	public Response getUmSystem(@RequestParam java.lang.String id){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(umSystemService.getUmSystemByPK( id )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query 系统表 by condition 查询条件")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "查询的关键字Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "页数", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "每页显示条数", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryUmSystem")
	public TableRes queryUmSystem(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<UmSystem> queryData = umSystemService.queryUmSystemBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

