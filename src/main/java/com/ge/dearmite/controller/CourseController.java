package com.ge.dearmite.controller;


import java.util.*;
import java.text.*;
import com.ge.dearmite.po.Course ;
import com.ge.dearmite.service.CourseService ;
import utils.*;
import utils.annotations.AuthorizedAccess;

import com.common.exception.*;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;

import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;


import com.ge.dearmite.dao.*;
import  com.ge.dearmite.po . * ;
import com.ge.dearmite.service .* ;


@Api(value = " course-controller", description = "course")
@Controller
@RequestMapping("/course")
public class CourseController  {

private static Logger logger = Logger.getLogger( CourseController.class);

	//=============================================
	@Autowired
	private CourseService courseService;
	
	
	
	//=============================================
	
	@ApiOperation(value = "add course")
	@SuppressWarnings({"rawtypes" })
	@ResponseBody
	@RequestMapping(value = "/addCourse" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/course/addCourse", description = "add course")
	public Response addCourse( @RequestBody   Course course ) 	 
	 {

			 course . setCourseid ( null );      // ��ϵͳ��Ҫ���ǲ���������
	 
//	  Course course = new Course ();
//	   Ҳ���Դ�MAP��ʽ���Լ����� 
//		    
//	//		
//		//				course . setCourseid ( Integer.parseInt((String) courseMap.get("courseid") ));
//		//		
//	//		
//		//				course . setCoursename ( ((String) courseMap.get("coursename") ));
//		//		
//	//		
//		//				course . setCourseteacher ( ((String) courseMap.get("courseteacher") ));
//		//		
//	//		
//		//				course . setScore ( ((String) courseMap.get("score") ));
//		//		
//	//		
//		//				course . setState ( ((String) courseMap.get("state") ));
//		//		
//		
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			courseService.addCourse(course);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
			 response.setMsg(e.getMessage());
			return response;
		}
		 
	 }
	 
	 
    @ApiOperation(value = "update course")
    @SuppressWarnings({"rawtypes" })
    @ResponseBody
	@RequestMapping(value = "/updateCourse" ,  method = RequestMethod.POST)
	@AuthorizedAccess (roles ="admin", permissions="/course/updateCourse", description = "update course")
	public Response updateCourse(@RequestBody  Course course)
	{
	   
	
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			courseService.updateCourse(course);
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	/// public void deleteCourseByPK(java.lang.Integer courseid) throws MyException ;
	 
	@ApiOperation(value = "delete course")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "courseid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes" })
     @ResponseBody
	@RequestMapping("/deleteCourse")
	@AuthorizedAccess (roles ="admin", permissions="/course/deleteCourse", description = "delete course")
	public Response deleteCourse(@RequestParam java.lang.Integer courseid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			courseService.deleteCourseByPK( courseid) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}
	
	
	@ApiOperation(value ="get  course")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "courseid", value = "PK ", required = true, paramType = "delete", dataType = "java.lang.Integer")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
     @ResponseBody
	@RequestMapping("/getCourse")
	@AuthorizedAccess (permissions="/course/getCourse", description = "get course")
	public Response getCourse(@RequestParam java.lang.Integer courseid){
		Response response = new Response(ResultHttpStatus.OK.getValue(), ResultHttpStatus.OK.getName());
		try {
			response.setData(courseService.getCourseByPK( courseid )) ;
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
            response.setStatus(ResultHttpStatus.INTERNAL_ERROR.getValue());
            response.setMsg(e.getMessage());
            return response;
		}
	}

	@ApiOperation(value = "query course by condition ��ѯ����")
	 @ApiImplicitParams({
            @ApiImplicitParam(name = "conditionMap", value = "��ѯ�Ĺؼ���Map", paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "page", value = "ҳ��", required = true, paramType = "query", dataType = "int"),
            @ApiImplicitParam(name = "pageSize", value = "ÿҳ��ʾ����", required = true, paramType = "query", dataType = "int")
    })
    @SuppressWarnings({"rawtypes",
        "unchecked"
    })
    @ResponseBody
	@RequestMapping("/queryCourse")
	@AuthorizedAccess (permissions="/course/queryCourse", description = "query course")
	public TableRes queryCourse(@RequestParam Map<String, Object> conditionMapPage)
	{
		 TableRes response = new TableRes(0, ResultHttpStatus.OK.getName());
		try {
			 Map<String, Object> conditionMap = (Map<String, Object>) conditionMapPage.get(
	                    "conditionMap");
            int page = Integer.parseInt((String) conditionMapPage.get("page"));
            int pageSize = Integer.parseInt((String) conditionMapPage.get(
                        "limit"));

            PageModel<Course> queryData = courseService.queryCourseBySearch(conditionMap,
                    page, pageSize);
            response.setData(queryData.getDataList());
            response.setCount(queryData.getTotalRecord());
			
			return response;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);

            return response;
		}
	}
	 
}

