package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmSysUserRole ;

/**
* 系统用户角色表
*  Created by autoGen Tools 
*/

public interface UmSysUserRoleMapper  extends Mapper<UmSysUserRole> {

	
	  
	 List<UmSysUserRole> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}