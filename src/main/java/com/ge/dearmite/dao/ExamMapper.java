package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . Exam ;

/**
* exam
*  Created by autoGen Tools 
*/
@Repository
public interface ExamMapper  extends Mapper<Exam> {

	
	  
	 List<Exam> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}