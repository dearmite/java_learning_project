package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . Country ;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface CountryMapper  extends Mapper<Country> {

	
	  
	 List<Country> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}