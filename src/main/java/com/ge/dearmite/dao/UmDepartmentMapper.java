package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmDepartment ;

/**
* 机构表
*  Created by autoGen Tools 
*/

public interface UmDepartmentMapper  extends Mapper<UmDepartment> {

	
	  
	 List<UmDepartment> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}