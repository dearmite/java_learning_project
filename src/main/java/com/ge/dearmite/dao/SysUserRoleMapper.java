package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysUserRole ;

/**
* 用户角色
*  Created by autoGen Tools 
*/
@Repository
public interface SysUserRoleMapper  extends Mapper<SysUserRole> {

	
	  
	 List<SysUserRole> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}