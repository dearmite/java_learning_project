package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UserInfo ;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UserInfoMapper  extends Mapper<UserInfo> {

	
	  
	 List<UserInfo> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}