package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . Classroom ;

/**
* classroom
*  Created by autoGen Tools 
*/
@Repository
public interface ClassroomMapper  extends Mapper<Classroom> {

	
	  
	 List<Classroom> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}