package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . Tablefac ;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface TablefacMapper  extends Mapper<Tablefac> {

	
	  
	 List<Tablefac> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}