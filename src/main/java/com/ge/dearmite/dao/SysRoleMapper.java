package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysRole ;

/**
* sys_role
*  Created by autoGen Tools 
*/
@Repository
public interface SysRoleMapper  extends Mapper<SysRole> {

	
	  
	 List<SysRole> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}