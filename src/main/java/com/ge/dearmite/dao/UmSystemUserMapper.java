package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmSystemUser ;

/**
* 系统用户表
*  Created by autoGen Tools 
*/

public interface UmSystemUserMapper  extends Mapper<UmSystemUser> {

	
	  
	 List<UmSystemUser> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}