package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmUserDept ;

/**
* UM用户机构关系表
*  Created by autoGen Tools 
*/

public interface UmUserDeptMapper  extends Mapper<UmUserDept> {

	
	  
	 List<UmUserDept> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}