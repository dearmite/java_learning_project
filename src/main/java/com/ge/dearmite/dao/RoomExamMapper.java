package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . RoomExam ;

/**
* room_exam
*  Created by autoGen Tools 
*/
@Repository
public interface RoomExamMapper  extends Mapper<RoomExam> {

	
	  
	 List<RoomExam> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}