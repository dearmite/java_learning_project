package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmSystemType ;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UmSystemTypeMapper  extends Mapper<UmSystemType> {

	
	  
	 List<UmSystemType> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}