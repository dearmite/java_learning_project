package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmSystemExt ;

/**
* 系统扩展属性表
*  Created by autoGen Tools 
*/

public interface UmSystemExtMapper  extends Mapper<UmSystemExt> {

	
	  
	 List<UmSystemExt> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}