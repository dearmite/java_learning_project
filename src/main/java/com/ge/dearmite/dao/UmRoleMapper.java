package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmRole ;

/**
* 角色表
*  Created by autoGen Tools 
*/

public interface UmRoleMapper  extends Mapper<UmRole> {

	
	  
	 List<UmRole> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}