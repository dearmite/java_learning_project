package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmUser ;

/**
* UM用户表
*  Created by autoGen Tools 
*/

public interface UmUserMapper  extends Mapper<UmUser> {

	
	  
	 List<UmUser> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}