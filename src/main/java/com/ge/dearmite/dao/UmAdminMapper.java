package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmAdmin ;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/

public interface UmAdminMapper  extends Mapper<UmAdmin> {

	
	  
	 List<UmAdmin> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}