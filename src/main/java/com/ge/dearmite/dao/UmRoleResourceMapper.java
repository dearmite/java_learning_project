package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmRoleResource ;

/**
* 角色资源关系表
*  Created by autoGen Tools 
*/

public interface UmRoleResourceMapper  extends Mapper<UmRoleResource> {

	
	  
	 List<UmRoleResource> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}