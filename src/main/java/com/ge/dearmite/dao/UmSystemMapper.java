package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmSystem ;

/**
* 系统表
*  Created by autoGen Tools 
*/

public interface UmSystemMapper  extends Mapper<UmSystem> {

	
	  
	 List<UmSystem> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}