package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysUser ;

/**
* 用户
*  Created by autoGen Tools 
*/
@Repository
public interface SysUserMapper  extends Mapper<SysUser> {

	
	  
	 List<SysUser> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}