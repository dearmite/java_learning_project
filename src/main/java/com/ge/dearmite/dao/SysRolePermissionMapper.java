package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysRolePermission ;

/**
* 角色权限
*  Created by autoGen Tools 
*/
@Repository
public interface SysRolePermissionMapper  extends Mapper<SysRolePermission> {

	
	  
	 List<SysRolePermission> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}