package com.ge.dearmite.dao;

import java.util.*;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . UmResource ;

/**
* 资源表
*  Created by autoGen Tools 
*/

public interface UmResourceMapper  extends Mapper<UmResource> {

	
	  
	 List<UmResource> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}