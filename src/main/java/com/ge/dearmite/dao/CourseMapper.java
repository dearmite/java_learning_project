package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . Course ;

/**
* course
*  Created by autoGen Tools 
*/
@Repository
public interface CourseMapper  extends Mapper<Course> {

	
	  
	 List<Course> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}