package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysUser2 ;

/**
* 用户
*  Created by autoGen Tools 
*/
@Repository
public interface SysUser2Mapper  extends Mapper<SysUser2> {

	
	  
	 List<SysUser2> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}