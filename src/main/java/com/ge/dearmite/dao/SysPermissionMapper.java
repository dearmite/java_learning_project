package com.ge.dearmite.dao;

import java.util.*;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import  com.ge.dearmite.po . SysPermission ;

/**
* 权限
*  Created by autoGen Tools 
*/
@Repository
public interface SysPermissionMapper  extends Mapper<SysPermission> {

	
	  
	 List<SysPermission> queryListPage(Map<String, Object> map);
	 

	 int queryCount (Map<String, Object> conditionMap);
	 
	

}