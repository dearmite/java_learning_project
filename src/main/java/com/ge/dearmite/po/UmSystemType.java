package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@ApiModel(value = "InnoDB free: 11264 kB")
public class UmSystemType implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* 分类编码
	*/
		@Id
		 @ApiModelProperty(value = "分类编码")
	private java.lang.String sysTypeCode;
	/**
	* 分类名称
	*/
		 @ApiModelProperty(value = "分类名称")
	private java.lang.String sysTypeCname;
	/**
	* 父级编码
	*/
		 @ApiModelProperty(value = "父级编码")
	private java.lang.String parentCode;
	/**
	* 分类描述
	*/
		 @ApiModelProperty(value = "分类描述")
	private java.lang.String sysTypeDesc;
	/**
	* 有效标志位:1-有效,0-失效
	*/
		 @ApiModelProperty(value = "有效标志位:1-有效,0-失效")
	private java.lang.String validInd;
	/**
	* 创建时间
	*/
		 @ApiModelProperty(value = "创建时间")
	private java.util.Date createdDate;
	/**
	* 创建人
	*/
		 @ApiModelProperty(value = "创建人")
	private java.lang.String createdUser;
	/**
	* 修改时间
	*/
		 @ApiModelProperty(value = "修改时间")
	private java.util.Date updatedDate;
	/**
	* 修改人
	*/
		 @ApiModelProperty(value = "修改人")
	private java.lang.String updatedUser;

	/**
	* set 分类编码
	*/
	public void setSysTypeCode ( java.lang.String sysTypeCode ) {
		this.sysTypeCode = sysTypeCode;
	}
	/**
	* get 分类编码
	*/
	public java.lang.String getSysTypeCode (){
		return sysTypeCode;
	}
	
	/**
	* set 分类名称
	*/
	public void setSysTypeCname ( java.lang.String sysTypeCname ) {
		this.sysTypeCname = sysTypeCname;
	}
	/**
	* get 分类名称
	*/
	public java.lang.String getSysTypeCname (){
		return sysTypeCname;
	}
	
	/**
	* set 父级编码
	*/
	public void setParentCode ( java.lang.String parentCode ) {
		this.parentCode = parentCode;
	}
	/**
	* get 父级编码
	*/
	public java.lang.String getParentCode (){
		return parentCode;
	}
	
	/**
	* set 分类描述
	*/
	public void setSysTypeDesc ( java.lang.String sysTypeDesc ) {
		this.sysTypeDesc = sysTypeDesc;
	}
	/**
	* get 分类描述
	*/
	public java.lang.String getSysTypeDesc (){
		return sysTypeDesc;
	}
	
	/**
	* set 有效标志位:1-有效,0-失效
	*/
	public void setValidInd ( java.lang.String validInd ) {
		this.validInd = validInd;
	}
	/**
	* get 有效标志位:1-有效,0-失效
	*/
	public java.lang.String getValidInd (){
		return validInd;
	}
	
	/**
	* set 创建时间
	*/
	public void setCreatedDate ( java.util.Date createdDate ) {
		this.createdDate = createdDate;
	}
	/**
	* get 创建时间
	*/
	public java.util.Date getCreatedDate (){
		return createdDate;
	}
	
	/**
	* set 创建人
	*/
	public void setCreatedUser ( java.lang.String createdUser ) {
		this.createdUser = createdUser;
	}
	/**
	* get 创建人
	*/
	public java.lang.String getCreatedUser (){
		return createdUser;
	}
	
	/**
	* set 修改时间
	*/
	public void setUpdatedDate ( java.util.Date updatedDate ) {
		this.updatedDate = updatedDate;
	}
	/**
	* get 修改时间
	*/
	public java.util.Date getUpdatedDate (){
		return updatedDate;
	}
	
	/**
	* set 修改人
	*/
	public void setUpdatedUser ( java.lang.String updatedUser ) {
		this.updatedUser = updatedUser;
	}
	/**
	* get 修改人
	*/
	public java.lang.String getUpdatedUser (){
		return updatedUser;
	}
	

	/**
	 * 自带校验的功能.如果不能解决.请在相应的controller 里，去掉 @Validated 这样，就不会走后台的校验.
	 * *@NotNull  注解元素必须是非空
		*@Null  注解元素必须是空
		*@Digits  验证数字构成是否合法
		*@Future  验证是否在当前系统时间之后
		*@Past  验证是否在当前系统时间之前
		*@Max  验证值是否小于等于最大指定整数值
		*@Min  验证值是否大于等于最小指定整数值
		* @Pattern 验证字符串是否匹配指定的正则表达式
		* @Size 验证元素大小是否在指定范围内
		* @DecimalMax 验证值是否小于等于最大指定小数值
		* @DecimalMin 验证值是否大于等于最小指定小数值
		* @AssertTrue 被注释的元素必须为true
		* @AssertFalse 被注释的元素必须为false
		HibernateValidator扩展注解类：
		*@Email  被注释的元素必须是电子邮箱地址
		*@Length  被注释的字符串的大小必须在指定的范围内
		*@NotEmpty  被注释的字符串的必须非空
		*@Range  被注释的元素必须在合适的范围内
		如--------------
	 * @NotBlank(message="{valid.name}") 非空.
	 * @Length(min=4, max=20, message="{valid.password}") 长度限制
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") 可以与上面的min 一起使用.限定范围
     * @Email(message="{valid.email}") 正确email
     * @Past(message="{valid.birthday}") 今天以前的日期.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") 最最强大的 正则的.你想要啥样
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) 还不成的话自己写一个类.想校验啥
    */

}