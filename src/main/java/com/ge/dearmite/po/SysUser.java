package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 用户
*  Created by autoGen Tools 
*/
@ApiModel(value = "用户")
public class SysUser implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* 用户ID
	*/
	@Id
	 @ApiModelProperty(value = "用户ID")
	private java.lang.Integer userid;
	/**
	* 姓名
	*/
	 @ApiModelProperty(value = "姓名")
	private java.lang.String name;
	/**
	* 性别
	*/
	 @ApiModelProperty(value = "性别")
	private java.lang.String sex;
	/**
	* 地址
	*/
	 @ApiModelProperty(value = "地址")
	private java.lang.String adress;
	/**
	* 电话
	*/
	 @ApiModelProperty(value = "电话")
	private java.lang.String tel;
	/**
	* 登录名
	*/
	 @ApiModelProperty(value = "登录名")
	private java.lang.String username;
	/**
	* 密码
	*/
	 @ApiModelProperty(value = "密码")
	private java.lang.String password;

	/**
	* set 用户ID
	*/
	public void setUserid ( java.lang.Integer userid ) {
		this.userid = userid;
	}
	/**
	* get 用户ID
	*/
	public java.lang.Integer getUserid (){
		return userid;
	}
	
	/**
	* set 姓名
	*/
	public void setName ( java.lang.String name ) {
		this.name = name;
	}
	/**
	* get 姓名
	*/
	public java.lang.String getName (){
		return name;
	}
	
	/**
	* set 性别
	*/
	public void setSex ( java.lang.String sex ) {
		this.sex = sex;
	}
	/**
	* get 性别
	*/
	public java.lang.String getSex (){
		return sex;
	}
	
	/**
	* set 地址
	*/
	public void setAdress ( java.lang.String adress ) {
		this.adress = adress;
	}
	/**
	* get 地址
	*/
	public java.lang.String getAdress (){
		return adress;
	}
	
	/**
	* set 电话
	*/
	public void setTel ( java.lang.String tel ) {
		this.tel = tel;
	}
	/**
	* get 电话
	*/
	public java.lang.String getTel (){
		return tel;
	}
	
	/**
	* set 登录名
	*/
	public void setUsername ( java.lang.String username ) {
		this.username = username;
	}
	/**
	* get 登录名
	*/
	public java.lang.String getUsername (){
		return username;
	}
	
	/**
	* set 密码
	*/
	public void setPassword ( java.lang.String password ) {
		this.password = password;
	}
	/**
	* get 密码
	*/
	public java.lang.String getPassword (){
		return password;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}