package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@ApiModel(value = "InnoDB free: 11264 kB")
public class UserInfo implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* id
	*/
		@Id
		 @ApiModelProperty(value = "id")
	private java.lang.Integer id;
	/**
	* username
	*/
		 @ApiModelProperty(value = "username")
	private java.lang.String username;
	/**
	* password
	*/
		 @ApiModelProperty(value = "password")
	private java.lang.String password;
	/**
	* usertype
	*/
		 @ApiModelProperty(value = "usertype")
	private java.lang.String usertype;
	/**
	* enabled
	*/
		 @ApiModelProperty(value = "enabled")
	private java.lang.Integer enabled;
	/**
	* realname
	*/
		 @ApiModelProperty(value = "realname")
	private java.lang.String realname;
	/**
	* qq
	*/
		 @ApiModelProperty(value = "qq")
	private java.lang.String qq;
	/**
	* email
	*/
		 @ApiModelProperty(value = "email")
	private java.lang.String email;
	/**
	* address
	*/
		 @ApiModelProperty(value = "address")
	private java.lang.String address;
	/**
	* tel
	*/
		 @ApiModelProperty(value = "tel")
	private java.lang.String tel;

	/**
	* set id
	*/
	public void setId ( java.lang.Integer id ) {
		this.id = id;
	}
	/**
	* get id
	*/
	public java.lang.Integer getId (){
		return id;
	}
	
	/**
	* set username
	*/
	public void setUsername ( java.lang.String username ) {
		this.username = username;
	}
	/**
	* get username
	*/
	public java.lang.String getUsername (){
		return username;
	}
	
	/**
	* set password
	*/
	public void setPassword ( java.lang.String password ) {
		this.password = password;
	}
	/**
	* get password
	*/
	public java.lang.String getPassword (){
		return password;
	}
	
	/**
	* set usertype
	*/
	public void setUsertype ( java.lang.String usertype ) {
		this.usertype = usertype;
	}
	/**
	* get usertype
	*/
	public java.lang.String getUsertype (){
		return usertype;
	}
	
	/**
	* set enabled
	*/
	public void setEnabled ( java.lang.Integer enabled ) {
		this.enabled = enabled;
	}
	/**
	* get enabled
	*/
	public java.lang.Integer getEnabled (){
		return enabled;
	}
	
	/**
	* set realname
	*/
	public void setRealname ( java.lang.String realname ) {
		this.realname = realname;
	}
	/**
	* get realname
	*/
	public java.lang.String getRealname (){
		return realname;
	}
	
	/**
	* set qq
	*/
	public void setQq ( java.lang.String qq ) {
		this.qq = qq;
	}
	/**
	* get qq
	*/
	public java.lang.String getQq (){
		return qq;
	}
	
	/**
	* set email
	*/
	public void setEmail ( java.lang.String email ) {
		this.email = email;
	}
	/**
	* get email
	*/
	public java.lang.String getEmail (){
		return email;
	}
	
	/**
	* set address
	*/
	public void setAddress ( java.lang.String address ) {
		this.address = address;
	}
	/**
	* get address
	*/
	public java.lang.String getAddress (){
		return address;
	}
	
	/**
	* set tel
	*/
	public void setTel ( java.lang.String tel ) {
		this.tel = tel;
	}
	/**
	* get tel
	*/
	public java.lang.String getTel (){
		return tel;
	}
	

	/**
	 * 自带校验的功能.如果不能解决.请在相应的controller 里，去掉 @Validated 这样，就不会走后台的校验.
	 * *@NotNull  注解元素必须是非空
		*@Null  注解元素必须是空
		*@Digits  验证数字构成是否合法
		*@Future  验证是否在当前系统时间之后
		*@Past  验证是否在当前系统时间之前
		*@Max  验证值是否小于等于最大指定整数值
		*@Min  验证值是否大于等于最小指定整数值
		* @Pattern 验证字符串是否匹配指定的正则表达式
		* @Size 验证元素大小是否在指定范围内
		* @DecimalMax 验证值是否小于等于最大指定小数值
		* @DecimalMin 验证值是否大于等于最小指定小数值
		* @AssertTrue 被注释的元素必须为true
		* @AssertFalse 被注释的元素必须为false
		HibernateValidator扩展注解类：
		*@Email  被注释的元素必须是电子邮箱地址
		*@Length  被注释的字符串的大小必须在指定的范围内
		*@NotEmpty  被注释的字符串的必须非空
		*@Range  被注释的元素必须在合适的范围内
		如--------------
	 * @NotBlank(message="{valid.name}") 非空.
	 * @Length(min=4, max=20, message="{valid.password}") 长度限制
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") 可以与上面的min 一起使用.限定范围
     * @Email(message="{valid.email}") 正确email
     * @Past(message="{valid.birthday}") 今天以前的日期.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") 最最强大的 正则的.你想要啥样
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) 还不成的话自己写一个类.想校验啥
    */

}