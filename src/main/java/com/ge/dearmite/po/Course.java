package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* course
*  Created by autoGen Tools 
*/
@ApiModel(value = "course")
public class Course implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* courseId
	*/
	@Id
	 @ApiModelProperty(value = "courseId")
	private java.lang.Integer courseid;
	/**
	* courseName
	*/
	 @ApiModelProperty(value = "courseName")
	private java.lang.String coursename;
	/**
	* courseTeacher
	*/
	 @ApiModelProperty(value = "courseTeacher")
	private java.lang.String courseteacher;
	/**
	* score
	*/
	 @ApiModelProperty(value = "score")
	private java.lang.String score;
	/**
	* state
	*/
	 @ApiModelProperty(value = "state")
	private java.lang.String state;

	/**
	* set courseId
	*/
	public void setCourseid ( java.lang.Integer courseid ) {
		this.courseid = courseid;
	}
	/**
	* get courseId
	*/
	public java.lang.Integer getCourseid (){
		return courseid;
	}
	
	/**
	* set courseName
	*/
	public void setCoursename ( java.lang.String coursename ) {
		this.coursename = coursename;
	}
	/**
	* get courseName
	*/
	public java.lang.String getCoursename (){
		return coursename;
	}
	
	/**
	* set courseTeacher
	*/
	public void setCourseteacher ( java.lang.String courseteacher ) {
		this.courseteacher = courseteacher;
	}
	/**
	* get courseTeacher
	*/
	public java.lang.String getCourseteacher (){
		return courseteacher;
	}
	
	/**
	* set score
	*/
	public void setScore ( java.lang.String score ) {
		this.score = score;
	}
	/**
	* get score
	*/
	public java.lang.String getScore (){
		return score;
	}
	
	/**
	* set state
	*/
	public void setState ( java.lang.String state ) {
		this.state = state;
	}
	/**
	* get state
	*/
	public java.lang.String getState (){
		return state;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}