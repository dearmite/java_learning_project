package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* room_exam
*  Created by autoGen Tools 
*/
@ApiModel(value = "room_exam")
public class RoomExam implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* room_exam_id
	*/
	@Id
	 @ApiModelProperty(value = "room_exam_id")
	private java.lang.Integer roomExamId;
	/**
	* examId
	*/
	 @ApiModelProperty(value = "examId")
	private java.lang.String examid;
	/**
	* roomld
	*/
	 @ApiModelProperty(value = "roomld")
	private java.lang.String roomld;
	/**
	* date
	*/
	 @ApiModelProperty(value = "date")
	private java.lang.String date;
	/**
	* time
	*/
	 @ApiModelProperty(value = "time")
	private java.lang.String time;

	/**
	* set room_exam_id
	*/
	public void setRoomExamId ( java.lang.Integer roomExamId ) {
		this.roomExamId = roomExamId;
	}
	/**
	* get room_exam_id
	*/
	public java.lang.Integer getRoomExamId (){
		return roomExamId;
	}
	
	/**
	* set examId
	*/
	public void setExamid ( java.lang.String examid ) {
		this.examid = examid;
	}
	/**
	* get examId
	*/
	public java.lang.String getExamid (){
		return examid;
	}
	
	/**
	* set roomld
	*/
	public void setRoomld ( java.lang.String roomld ) {
		this.roomld = roomld;
	}
	/**
	* get roomld
	*/
	public java.lang.String getRoomld (){
		return roomld;
	}
	
	/**
	* set date
	*/
	public void setDate ( java.lang.String date ) {
		this.date = date;
	}
	/**
	* get date
	*/
	public java.lang.String getDate (){
		return date;
	}
	
	/**
	* set time
	*/
	public void setTime ( java.lang.String time ) {
		this.time = time;
	}
	/**
	* get time
	*/
	public java.lang.String getTime (){
		return time;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}