package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 资源表
*  Created by autoGen Tools 
*/
@ApiModel(value = "资源表")
public class UmResource implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* 资源编码
	*/
		@Id
		 @ApiModelProperty(value = "资源编码")
	private java.lang.String resourceId;
	/**
	* 名称
	*/
		 @ApiModelProperty(value = "名称")
	private java.lang.String resourceName;
	/**
	* 1 表示菜单
            0 表示按钮
	*/
		 @ApiModelProperty(value = "1 表示菜单            0 表示按钮")
	private java.lang.String resourceType;
	/**
	* 系统编码
	*/
		 @ApiModelProperty(value = "系统编码")
	private java.lang.String sysCode;
	/**
	* 父资源编码
	*/
		 @ApiModelProperty(value = "父资源编码")
	private java.lang.String parentId;
	/**
	* URL
	*/
		 @ApiModelProperty(value = "URL")
	private java.lang.String url;
	/**
	* 菜单序号
	*/
		 @ApiModelProperty(value = "菜单序号")
	private java.lang.String menuIndex;
	/**
	* 有效标志位:1-有效,0-失效
	*/
		 @ApiModelProperty(value = "有效标志位:1-有效,0-失效")
	private java.lang.String validInd;
	/**
	* 创建时间
	*/
		 @ApiModelProperty(value = "创建时间")
	private java.util.Date createdDate;
	/**
	* 创建人
	*/
		 @ApiModelProperty(value = "创建人")
	private java.lang.String createdUser;
	/**
	* 修改时间
	*/
		 @ApiModelProperty(value = "修改时间")
	private java.util.Date updatedDate;
	/**
	* 修改人
	*/
		 @ApiModelProperty(value = "修改人")
	private java.lang.String updatedUser;

	/**
	* set 资源编码
	*/
	public void setResourceId ( java.lang.String resourceId ) {
		this.resourceId = resourceId;
	}
	/**
	* get 资源编码
	*/
	public java.lang.String getResourceId (){
		return resourceId;
	}
	
	/**
	* set 名称
	*/
	public void setResourceName ( java.lang.String resourceName ) {
		this.resourceName = resourceName;
	}
	/**
	* get 名称
	*/
	public java.lang.String getResourceName (){
		return resourceName;
	}
	
	/**
	* set 1 表示菜单
            0 表示按钮
	*/
	public void setResourceType ( java.lang.String resourceType ) {
		this.resourceType = resourceType;
	}
	/**
	* get 1 表示菜单
            0 表示按钮
	*/
	public java.lang.String getResourceType (){
		return resourceType;
	}
	
	/**
	* set 系统编码
	*/
	public void setSysCode ( java.lang.String sysCode ) {
		this.sysCode = sysCode;
	}
	/**
	* get 系统编码
	*/
	public java.lang.String getSysCode (){
		return sysCode;
	}
	
	/**
	* set 父资源编码
	*/
	public void setParentId ( java.lang.String parentId ) {
		this.parentId = parentId;
	}
	/**
	* get 父资源编码
	*/
	public java.lang.String getParentId (){
		return parentId;
	}
	
	/**
	* set URL
	*/
	public void setUrl ( java.lang.String url ) {
		this.url = url;
	}
	/**
	* get URL
	*/
	public java.lang.String getUrl (){
		return url;
	}
	
	/**
	* set 菜单序号
	*/
	public void setMenuIndex ( java.lang.String menuIndex ) {
		this.menuIndex = menuIndex;
	}
	/**
	* get 菜单序号
	*/
	public java.lang.String getMenuIndex (){
		return menuIndex;
	}
	
	/**
	* set 有效标志位:1-有效,0-失效
	*/
	public void setValidInd ( java.lang.String validInd ) {
		this.validInd = validInd;
	}
	/**
	* get 有效标志位:1-有效,0-失效
	*/
	public java.lang.String getValidInd (){
		return validInd;
	}
	
	/**
	* set 创建时间
	*/
	public void setCreatedDate ( java.util.Date createdDate ) {
		this.createdDate = createdDate;
	}
	/**
	* get 创建时间
	*/
	public java.util.Date getCreatedDate (){
		return createdDate;
	}
	
	/**
	* set 创建人
	*/
	public void setCreatedUser ( java.lang.String createdUser ) {
		this.createdUser = createdUser;
	}
	/**
	* get 创建人
	*/
	public java.lang.String getCreatedUser (){
		return createdUser;
	}
	
	/**
	* set 修改时间
	*/
	public void setUpdatedDate ( java.util.Date updatedDate ) {
		this.updatedDate = updatedDate;
	}
	/**
	* get 修改时间
	*/
	public java.util.Date getUpdatedDate (){
		return updatedDate;
	}
	
	/**
	* set 修改人
	*/
	public void setUpdatedUser ( java.lang.String updatedUser ) {
		this.updatedUser = updatedUser;
	}
	/**
	* get 修改人
	*/
	public java.lang.String getUpdatedUser (){
		return updatedUser;
	}
	

	/**
	 * 自带校验的功能.如果不能解决.请在相应的controller 里，去掉 @Validated 这样，就不会走后台的校验.
	 * *@NotNull  注解元素必须是非空
		*@Null  注解元素必须是空
		*@Digits  验证数字构成是否合法
		*@Future  验证是否在当前系统时间之后
		*@Past  验证是否在当前系统时间之前
		*@Max  验证值是否小于等于最大指定整数值
		*@Min  验证值是否大于等于最小指定整数值
		* @Pattern 验证字符串是否匹配指定的正则表达式
		* @Size 验证元素大小是否在指定范围内
		* @DecimalMax 验证值是否小于等于最大指定小数值
		* @DecimalMin 验证值是否大于等于最小指定小数值
		* @AssertTrue 被注释的元素必须为true
		* @AssertFalse 被注释的元素必须为false
		HibernateValidator扩展注解类：
		*@Email  被注释的元素必须是电子邮箱地址
		*@Length  被注释的字符串的大小必须在指定的范围内
		*@NotEmpty  被注释的字符串的必须非空
		*@Range  被注释的元素必须在合适的范围内
		如--------------
	 * @NotBlank(message="{valid.name}") 非空.
	 * @Length(min=4, max=20, message="{valid.password}") 长度限制
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") 可以与上面的min 一起使用.限定范围
     * @Email(message="{valid.email}") 正确email
     * @Past(message="{valid.birthday}") 今天以前的日期.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") 最最强大的 正则的.你想要啥样
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) 还不成的话自己写一个类.想校验啥
    */

}