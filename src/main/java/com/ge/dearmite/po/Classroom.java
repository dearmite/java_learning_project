package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* classroom
*  Created by autoGen Tools 
*/
@ApiModel(value = "classroom")
public class Classroom implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* roomId
	*/
	@Id
	 @ApiModelProperty(value = "roomId")
	private java.lang.Integer roomid;
	/**
	* roomName
	*/
	 @ApiModelProperty(value = "roomName")
	private java.lang.String roomname;
	/**
	* capacity
	*/
	 @ApiModelProperty(value = "capacity")
	private java.lang.String capacity;
	/**
	* state
	*/
	 @ApiModelProperty(value = "state")
	private java.lang.String state;
	/**
	* date
	*/
	 @ApiModelProperty(value = "date")
	private java.lang.String date;

	/**
	* set roomId
	*/
	public void setRoomid ( java.lang.Integer roomid ) {
		this.roomid = roomid;
	}
	/**
	* get roomId
	*/
	public java.lang.Integer getRoomid (){
		return roomid;
	}
	
	/**
	* set roomName
	*/
	public void setRoomname ( java.lang.String roomname ) {
		this.roomname = roomname;
	}
	/**
	* get roomName
	*/
	public java.lang.String getRoomname (){
		return roomname;
	}
	
	/**
	* set capacity
	*/
	public void setCapacity ( java.lang.String capacity ) {
		this.capacity = capacity;
	}
	/**
	* get capacity
	*/
	public java.lang.String getCapacity (){
		return capacity;
	}
	
	/**
	* set state
	*/
	public void setState ( java.lang.String state ) {
		this.state = state;
	}
	/**
	* get state
	*/
	public java.lang.String getState (){
		return state;
	}
	
	/**
	* set date
	*/
	public void setDate ( java.lang.String date ) {
		this.date = date;
	}
	/**
	* get date
	*/
	public java.lang.String getDate (){
		return date;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}