package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* InnoDB free: 11264 kB
*  Created by autoGen Tools 
*/
@ApiModel(value = "InnoDB free: 11264 kB")
public class HrUserDepartment implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* 员工ID
	*/
		@Id
		 @ApiModelProperty(value = "员工ID")
	private java.lang.String emId;
	/**
	* 员工姓名
	*/
		 @ApiModelProperty(value = "员工姓名")
	private java.lang.String employeename;
	/**
	* 员工性别
	*/
		 @ApiModelProperty(value = "员工性别")
	private java.lang.String sex;
	/**
	* 公司ID
	*/
		 @ApiModelProperty(value = "公司ID")
	private java.lang.String companyid;
	/**
	* 员工帐号
	*/
		 @ApiModelProperty(value = "员工帐号")
	private java.lang.String account;
	/**
	* 员工域帐号
	*/
		 @ApiModelProperty(value = "员工域帐号")
	private java.lang.String email;
	/**
	* 员工电话
	*/
		 @ApiModelProperty(value = "员工电话")
	private java.lang.String phone;

	/**
	* set 员工ID
	*/
	public void setEmId ( java.lang.String emId ) {
		this.emId = emId;
	}
	/**
	* get 员工ID
	*/
	public java.lang.String getEmId (){
		return emId;
	}
	
	/**
	* set 员工姓名
	*/
	public void setEmployeename ( java.lang.String employeename ) {
		this.employeename = employeename;
	}
	/**
	* get 员工姓名
	*/
	public java.lang.String getEmployeename (){
		return employeename;
	}
	
	/**
	* set 员工性别
	*/
	public void setSex ( java.lang.String sex ) {
		this.sex = sex;
	}
	/**
	* get 员工性别
	*/
	public java.lang.String getSex (){
		return sex;
	}
	
	/**
	* set 公司ID
	*/
	public void setCompanyid ( java.lang.String companyid ) {
		this.companyid = companyid;
	}
	/**
	* get 公司ID
	*/
	public java.lang.String getCompanyid (){
		return companyid;
	}
	
	/**
	* set 员工帐号
	*/
	public void setAccount ( java.lang.String account ) {
		this.account = account;
	}
	/**
	* get 员工帐号
	*/
	public java.lang.String getAccount (){
		return account;
	}
	
	/**
	* set 员工域帐号
	*/
	public void setEmail ( java.lang.String email ) {
		this.email = email;
	}
	/**
	* get 员工域帐号
	*/
	public java.lang.String getEmail (){
		return email;
	}
	
	/**
	* set 员工电话
	*/
	public void setPhone ( java.lang.String phone ) {
		this.phone = phone;
	}
	/**
	* get 员工电话
	*/
	public java.lang.String getPhone (){
		return phone;
	}
	

	/**
	 * 自带校验的功能.如果不能解决.请在相应的controller 里，去掉 @Validated 这样，就不会走后台的校验.
	 * *@NotNull  注解元素必须是非空
		*@Null  注解元素必须是空
		*@Digits  验证数字构成是否合法
		*@Future  验证是否在当前系统时间之后
		*@Past  验证是否在当前系统时间之前
		*@Max  验证值是否小于等于最大指定整数值
		*@Min  验证值是否大于等于最小指定整数值
		* @Pattern 验证字符串是否匹配指定的正则表达式
		* @Size 验证元素大小是否在指定范围内
		* @DecimalMax 验证值是否小于等于最大指定小数值
		* @DecimalMin 验证值是否大于等于最小指定小数值
		* @AssertTrue 被注释的元素必须为true
		* @AssertFalse 被注释的元素必须为false
		HibernateValidator扩展注解类：
		*@Email  被注释的元素必须是电子邮箱地址
		*@Length  被注释的字符串的大小必须在指定的范围内
		*@NotEmpty  被注释的字符串的必须非空
		*@Range  被注释的元素必须在合适的范围内
		如--------------
	 * @NotBlank(message="{valid.name}") 非空.
	 * @Length(min=4, max=20, message="{valid.password}") 长度限制
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") 可以与上面的min 一起使用.限定范围
     * @Email(message="{valid.email}") 正确email
     * @Past(message="{valid.birthday}") 今天以前的日期.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") 最最强大的 正则的.你想要啥样
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) 还不成的话自己写一个类.想校验啥
    */

}