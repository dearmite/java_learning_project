package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* exam
*  Created by autoGen Tools 
*/
@ApiModel(value = "exam")
public class Exam implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* examId
	*/
	@Id
	 @ApiModelProperty(value = "examId")
	private java.lang.Integer examid;
	/**
	* examName
	*/
	 @ApiModelProperty(value = "examName")
	private java.lang.String examname;
	/**
	* examType
	*/
	 @ApiModelProperty(value = "examType")
	private java.lang.String examtype;
	/**
	* examState
	*/
	 @ApiModelProperty(value = "examState")
	private java.lang.String examstate;

	/**
	* set examId
	*/
	public void setExamid ( java.lang.Integer examid ) {
		this.examid = examid;
	}
	/**
	* get examId
	*/
	public java.lang.Integer getExamid (){
		return examid;
	}
	
	/**
	* set examName
	*/
	public void setExamname ( java.lang.String examname ) {
		this.examname = examname;
	}
	/**
	* get examName
	*/
	public java.lang.String getExamname (){
		return examname;
	}
	
	/**
	* set examType
	*/
	public void setExamtype ( java.lang.String examtype ) {
		this.examtype = examtype;
	}
	/**
	* get examType
	*/
	public java.lang.String getExamtype (){
		return examtype;
	}
	
	/**
	* set examState
	*/
	public void setExamstate ( java.lang.String examstate ) {
		this.examstate = examstate;
	}
	/**
	* get examState
	*/
	public java.lang.String getExamstate (){
		return examstate;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}