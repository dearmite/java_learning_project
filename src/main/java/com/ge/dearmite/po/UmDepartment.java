package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 机构表
*  Created by autoGen Tools 
*/
@ApiModel(value = "机构表")
public class UmDepartment implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* 机构代码
	*/
		@Id
		 @ApiModelProperty(value = "机构代码")
	private java.lang.String dptCde;
	/**
	* 机构名称
	*/
		 @ApiModelProperty(value = "机构名称")
	private java.lang.String dptCnm;
	/**
	* 英文名称
	*/
		 @ApiModelProperty(value = "英文名称")
	private java.lang.String cDptEnm;
	/**
	* 父机构代码
	*/
		 @ApiModelProperty(value = "父机构代码")
	private java.lang.String cSnrDpt;
	/**
	* 机构级别
	*/
		 @ApiModelProperty(value = "机构级别")
	private java.lang.String nDptLevl;
	/**
	* 机构关系码
	*/
		 @ApiModelProperty(value = "机构关系码")
	private java.lang.String dptRelCde;
	/**
	* 保险许可证
	*/
		 @ApiModelProperty(value = "保险许可证")
	private java.lang.String insprmtNo;
	/**
	* 报案地址
	*/
		 @ApiModelProperty(value = "报案地址")
	private java.lang.String rptAddr;
	/**
	* 报案电话
	*/
		 @ApiModelProperty(value = "报案电话")
	private java.lang.String rptTel;
	/**
	* 报警标志
	*/
		 @ApiModelProperty(value = "报警标志")
	private java.lang.String alarmMrk;
	/**
	* 部门标志
	*/
		 @ApiModelProperty(value = "部门标志")
	private java.lang.String departmentMrk;
	/**
	* 传真
	*/
		 @ApiModelProperty(value = "传真")
	private java.lang.String fax;
	/**
	* 联系电话
	*/
		 @ApiModelProperty(value = "联系电话")
	private java.lang.String tel;
	/**
	* 咨询电话
	*/
		 @ApiModelProperty(value = "咨询电话")
	private java.lang.String consTel;
	/**
	* 地址
	*/
		 @ApiModelProperty(value = "地址")
	private java.lang.String dptCaddr;
	/**
	* 英文地址
	*/
		 @ApiModelProperty(value = "英文地址")
	private java.lang.String dptEaddr;
	/**
	* 邮编
	*/
		 @ApiModelProperty(value = "邮编")
	private java.lang.String zipCde;
	/**
	* 机构简称
	*/
		 @ApiModelProperty(value = "机构简称")
	private java.lang.String dptAbr;
	/**
	* 机构流水号
	*/
		 @ApiModelProperty(value = "机构流水号")
	private java.lang.String dptSerno;
	/**
	* 联系人
	*/
		 @ApiModelProperty(value = "联系人")
	private java.lang.String ctctCde;
	/**
	* 内码
	*/
		 @ApiModelProperty(value = "内码")
	private java.lang.String interCde;
	/**
	* 失效时间
	*/
		 @ApiModelProperty(value = "失效时间")
	private java.util.Date abdTm;
	/**
	* 成立时间
	*/
		 @ApiModelProperty(value = "成立时间")
	private java.util.Date fndTm;
	/**
	* 税务登记号
	*/
		 @ApiModelProperty(value = "税务登记号")
	private java.lang.String taxrgstNo;
	/**
	* 营业执照
	*/
		 @ApiModelProperty(value = "营业执照")
	private java.lang.String lcnAbr;
	/**
	* 展业资格证
	*/
		 @ApiModelProperty(value = "展业资格证")
	private java.lang.String bnsrgstNo;
	/**
	* 有效标志位:1-有效,0-失效
	*/
		 @ApiModelProperty(value = "有效标志位:1-有效,0-失效")
	private java.lang.String validInd;
	/**
	* 创建时间
	*/
		 @ApiModelProperty(value = "创建时间")
	private java.util.Date createdDate;
	/**
	* 创建人
	*/
		 @ApiModelProperty(value = "创建人")
	private java.lang.String createdUser;
	/**
	* 修改时间
	*/
		 @ApiModelProperty(value = "修改时间")
	private java.util.Date updatedDate;
	/**
	* 修改人
	*/
		 @ApiModelProperty(value = "修改人")
	private java.lang.String updatedUser;

	/**
	* set 机构代码
	*/
	public void setDptCde ( java.lang.String dptCde ) {
		this.dptCde = dptCde;
	}
	/**
	* get 机构代码
	*/
	public java.lang.String getDptCde (){
		return dptCde;
	}
	
	/**
	* set 机构名称
	*/
	public void setDptCnm ( java.lang.String dptCnm ) {
		this.dptCnm = dptCnm;
	}
	/**
	* get 机构名称
	*/
	public java.lang.String getDptCnm (){
		return dptCnm;
	}
	
	/**
	* set 英文名称
	*/
	public void setCDptEnm ( java.lang.String cDptEnm ) {
		this.cDptEnm = cDptEnm;
	}
	/**
	* get 英文名称
	*/
	public java.lang.String getCDptEnm (){
		return cDptEnm;
	}
	
	/**
	* set 父机构代码
	*/
	public void setCSnrDpt ( java.lang.String cSnrDpt ) {
		this.cSnrDpt = cSnrDpt;
	}
	/**
	* get 父机构代码
	*/
	public java.lang.String getCSnrDpt (){
		return cSnrDpt;
	}
	
	/**
	* set 机构级别
	*/
	public void setNDptLevl ( java.lang.String nDptLevl ) {
		this.nDptLevl = nDptLevl;
	}
	/**
	* get 机构级别
	*/
	public java.lang.String getNDptLevl (){
		return nDptLevl;
	}
	
	/**
	* set 机构关系码
	*/
	public void setDptRelCde ( java.lang.String dptRelCde ) {
		this.dptRelCde = dptRelCde;
	}
	/**
	* get 机构关系码
	*/
	public java.lang.String getDptRelCde (){
		return dptRelCde;
	}
	
	/**
	* set 保险许可证
	*/
	public void setInsprmtNo ( java.lang.String insprmtNo ) {
		this.insprmtNo = insprmtNo;
	}
	/**
	* get 保险许可证
	*/
	public java.lang.String getInsprmtNo (){
		return insprmtNo;
	}
	
	/**
	* set 报案地址
	*/
	public void setRptAddr ( java.lang.String rptAddr ) {
		this.rptAddr = rptAddr;
	}
	/**
	* get 报案地址
	*/
	public java.lang.String getRptAddr (){
		return rptAddr;
	}
	
	/**
	* set 报案电话
	*/
	public void setRptTel ( java.lang.String rptTel ) {
		this.rptTel = rptTel;
	}
	/**
	* get 报案电话
	*/
	public java.lang.String getRptTel (){
		return rptTel;
	}
	
	/**
	* set 报警标志
	*/
	public void setAlarmMrk ( java.lang.String alarmMrk ) {
		this.alarmMrk = alarmMrk;
	}
	/**
	* get 报警标志
	*/
	public java.lang.String getAlarmMrk (){
		return alarmMrk;
	}
	
	/**
	* set 部门标志
	*/
	public void setDepartmentMrk ( java.lang.String departmentMrk ) {
		this.departmentMrk = departmentMrk;
	}
	/**
	* get 部门标志
	*/
	public java.lang.String getDepartmentMrk (){
		return departmentMrk;
	}
	
	/**
	* set 传真
	*/
	public void setFax ( java.lang.String fax ) {
		this.fax = fax;
	}
	/**
	* get 传真
	*/
	public java.lang.String getFax (){
		return fax;
	}
	
	/**
	* set 联系电话
	*/
	public void setTel ( java.lang.String tel ) {
		this.tel = tel;
	}
	/**
	* get 联系电话
	*/
	public java.lang.String getTel (){
		return tel;
	}
	
	/**
	* set 咨询电话
	*/
	public void setConsTel ( java.lang.String consTel ) {
		this.consTel = consTel;
	}
	/**
	* get 咨询电话
	*/
	public java.lang.String getConsTel (){
		return consTel;
	}
	
	/**
	* set 地址
	*/
	public void setDptCaddr ( java.lang.String dptCaddr ) {
		this.dptCaddr = dptCaddr;
	}
	/**
	* get 地址
	*/
	public java.lang.String getDptCaddr (){
		return dptCaddr;
	}
	
	/**
	* set 英文地址
	*/
	public void setDptEaddr ( java.lang.String dptEaddr ) {
		this.dptEaddr = dptEaddr;
	}
	/**
	* get 英文地址
	*/
	public java.lang.String getDptEaddr (){
		return dptEaddr;
	}
	
	/**
	* set 邮编
	*/
	public void setZipCde ( java.lang.String zipCde ) {
		this.zipCde = zipCde;
	}
	/**
	* get 邮编
	*/
	public java.lang.String getZipCde (){
		return zipCde;
	}
	
	/**
	* set 机构简称
	*/
	public void setDptAbr ( java.lang.String dptAbr ) {
		this.dptAbr = dptAbr;
	}
	/**
	* get 机构简称
	*/
	public java.lang.String getDptAbr (){
		return dptAbr;
	}
	
	/**
	* set 机构流水号
	*/
	public void setDptSerno ( java.lang.String dptSerno ) {
		this.dptSerno = dptSerno;
	}
	/**
	* get 机构流水号
	*/
	public java.lang.String getDptSerno (){
		return dptSerno;
	}
	
	/**
	* set 联系人
	*/
	public void setCtctCde ( java.lang.String ctctCde ) {
		this.ctctCde = ctctCde;
	}
	/**
	* get 联系人
	*/
	public java.lang.String getCtctCde (){
		return ctctCde;
	}
	
	/**
	* set 内码
	*/
	public void setInterCde ( java.lang.String interCde ) {
		this.interCde = interCde;
	}
	/**
	* get 内码
	*/
	public java.lang.String getInterCde (){
		return interCde;
	}
	
	/**
	* set 失效时间
	*/
	public void setAbdTm ( java.util.Date abdTm ) {
		this.abdTm = abdTm;
	}
	/**
	* get 失效时间
	*/
	public java.util.Date getAbdTm (){
		return abdTm;
	}
	
	/**
	* set 成立时间
	*/
	public void setFndTm ( java.util.Date fndTm ) {
		this.fndTm = fndTm;
	}
	/**
	* get 成立时间
	*/
	public java.util.Date getFndTm (){
		return fndTm;
	}
	
	/**
	* set 税务登记号
	*/
	public void setTaxrgstNo ( java.lang.String taxrgstNo ) {
		this.taxrgstNo = taxrgstNo;
	}
	/**
	* get 税务登记号
	*/
	public java.lang.String getTaxrgstNo (){
		return taxrgstNo;
	}
	
	/**
	* set 营业执照
	*/
	public void setLcnAbr ( java.lang.String lcnAbr ) {
		this.lcnAbr = lcnAbr;
	}
	/**
	* get 营业执照
	*/
	public java.lang.String getLcnAbr (){
		return lcnAbr;
	}
	
	/**
	* set 展业资格证
	*/
	public void setBnsrgstNo ( java.lang.String bnsrgstNo ) {
		this.bnsrgstNo = bnsrgstNo;
	}
	/**
	* get 展业资格证
	*/
	public java.lang.String getBnsrgstNo (){
		return bnsrgstNo;
	}
	
	/**
	* set 有效标志位:1-有效,0-失效
	*/
	public void setValidInd ( java.lang.String validInd ) {
		this.validInd = validInd;
	}
	/**
	* get 有效标志位:1-有效,0-失效
	*/
	public java.lang.String getValidInd (){
		return validInd;
	}
	
	/**
	* set 创建时间
	*/
	public void setCreatedDate ( java.util.Date createdDate ) {
		this.createdDate = createdDate;
	}
	/**
	* get 创建时间
	*/
	public java.util.Date getCreatedDate (){
		return createdDate;
	}
	
	/**
	* set 创建人
	*/
	public void setCreatedUser ( java.lang.String createdUser ) {
		this.createdUser = createdUser;
	}
	/**
	* get 创建人
	*/
	public java.lang.String getCreatedUser (){
		return createdUser;
	}
	
	/**
	* set 修改时间
	*/
	public void setUpdatedDate ( java.util.Date updatedDate ) {
		this.updatedDate = updatedDate;
	}
	/**
	* get 修改时间
	*/
	public java.util.Date getUpdatedDate (){
		return updatedDate;
	}
	
	/**
	* set 修改人
	*/
	public void setUpdatedUser ( java.lang.String updatedUser ) {
		this.updatedUser = updatedUser;
	}
	/**
	* get 修改人
	*/
	public java.lang.String getUpdatedUser (){
		return updatedUser;
	}
	

	/**
	 * 自带校验的功能.如果不能解决.请在相应的controller 里，去掉 @Validated 这样，就不会走后台的校验.
	 * *@NotNull  注解元素必须是非空
		*@Null  注解元素必须是空
		*@Digits  验证数字构成是否合法
		*@Future  验证是否在当前系统时间之后
		*@Past  验证是否在当前系统时间之前
		*@Max  验证值是否小于等于最大指定整数值
		*@Min  验证值是否大于等于最小指定整数值
		* @Pattern 验证字符串是否匹配指定的正则表达式
		* @Size 验证元素大小是否在指定范围内
		* @DecimalMax 验证值是否小于等于最大指定小数值
		* @DecimalMin 验证值是否大于等于最小指定小数值
		* @AssertTrue 被注释的元素必须为true
		* @AssertFalse 被注释的元素必须为false
		HibernateValidator扩展注解类：
		*@Email  被注释的元素必须是电子邮箱地址
		*@Length  被注释的字符串的大小必须在指定的范围内
		*@NotEmpty  被注释的字符串的必须非空
		*@Range  被注释的元素必须在合适的范围内
		如--------------
	 * @NotBlank(message="{valid.name}") 非空.
	 * @Length(min=4, max=20, message="{valid.password}") 长度限制
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") 可以与上面的min 一起使用.限定范围
     * @Email(message="{valid.email}") 正确email
     * @Past(message="{valid.birthday}") 今天以前的日期.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") 最最强大的 正则的.你想要啥样
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) 还不成的话自己写一个类.想校验啥
    */

}