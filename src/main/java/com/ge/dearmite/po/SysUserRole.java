package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* 用户角色
*  Created by autoGen Tools 
*/
@ApiModel(value = "用户角色")
public class SysUserRole implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* userRoleId
	*/
	@Id
	 @ApiModelProperty(value = "userRoleId")
	private java.lang.Integer userroleid;
	/**
	* 用户ID
	*/
	 @ApiModelProperty(value = "用户ID")
	private java.lang.Integer userid;
	/**
	* 角色ID
	*/
	 @ApiModelProperty(value = "角色ID")
	private java.lang.Integer roleid;

	/**
	* set userRoleId
	*/
	public void setUserroleid ( java.lang.Integer userroleid ) {
		this.userroleid = userroleid;
	}
	/**
	* get userRoleId
	*/
	public java.lang.Integer getUserroleid (){
		return userroleid;
	}
	
	/**
	* set 用户ID
	*/
	public void setUserid ( java.lang.Integer userid ) {
		this.userid = userid;
	}
	/**
	* get 用户ID
	*/
	public java.lang.Integer getUserid (){
		return userid;
	}
	
	/**
	* set 角色ID
	*/
	public void setRoleid ( java.lang.Integer roleid ) {
		this.roleid = roleid;
	}
	/**
	* get 角色ID
	*/
	public java.lang.Integer getRoleid (){
		return roleid;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}