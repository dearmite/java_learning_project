package com.ge.dearmite.po;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import org.hibernate.validator.constraints.*;

import org.springframework.validation.annotation.Validated;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
* room_course
*  Created by autoGen Tools 
*/
@ApiModel(value = "room_course")
public class RoomCourse implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

	/**
	* room_course_Id
	*/
	@Id
	 @ApiModelProperty(value = "room_course_Id")
	private java.lang.Integer roomCourseId;
	/**
	* courseId
	*/
	 @ApiModelProperty(value = "courseId")
	private java.lang.Integer courseid;
	/**
	* roomId
	*/
	 @ApiModelProperty(value = "roomId")
	private java.lang.Integer roomid;
	/**
	* time
	*/
	 @ApiModelProperty(value = "time")
	private java.lang.String time;
	/**
	* date
	*/
	 @ApiModelProperty(value = "date")
	private java.lang.String date;

	/**
	* set room_course_Id
	*/
	public void setRoomCourseId ( java.lang.Integer roomCourseId ) {
		this.roomCourseId = roomCourseId;
	}
	/**
	* get room_course_Id
	*/
	public java.lang.Integer getRoomCourseId (){
		return roomCourseId;
	}
	
	/**
	* set courseId
	*/
	public void setCourseid ( java.lang.Integer courseid ) {
		this.courseid = courseid;
	}
	/**
	* get courseId
	*/
	public java.lang.Integer getCourseid (){
		return courseid;
	}
	
	/**
	* set roomId
	*/
	public void setRoomid ( java.lang.Integer roomid ) {
		this.roomid = roomid;
	}
	/**
	* get roomId
	*/
	public java.lang.Integer getRoomid (){
		return roomid;
	}
	
	/**
	* set time
	*/
	public void setTime ( java.lang.String time ) {
		this.time = time;
	}
	/**
	* get time
	*/
	public java.lang.String getTime (){
		return time;
	}
	
	/**
	* set date
	*/
	public void setDate ( java.lang.String date ) {
		this.date = date;
	}
	/**
	* get date
	*/
	public java.lang.String getDate (){
		return date;
	}
	

	/**
	 * �Դ�У��Ĺ���.������ܽ��.������Ӧ��controller �ȥ�� @Validated �������Ͳ����ߺ�̨��У��.
	 * *@NotNull  ע��Ԫ�ر����Ƿǿ�
		*@Null  ע��Ԫ�ر����ǿ�
		*@Digits  ��֤���ֹ����Ƿ�Ϸ�
		*@Future  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮��
		*@Past  ��֤�Ƿ��ڵ�ǰϵͳʱ��֮ǰ
		*@Max  ��ֵ֤�Ƿ�С�ڵ������ָ������ֵ
		*@Min  ��ֵ֤�Ƿ���ڵ�����Сָ������ֵ
		* @Pattern ��֤�ַ����Ƿ�ƥ��ָ����������ʽ
		* @Size ��֤Ԫ�ش�С�Ƿ���ָ����Χ��
		* @DecimalMax ��ֵ֤�Ƿ�С�ڵ������ָ��С��ֵ
		* @DecimalMin ��ֵ֤�Ƿ���ڵ�����Сָ��С��ֵ
		* @AssertTrue ��ע�͵�Ԫ�ر���Ϊtrue
		* @AssertFalse ��ע�͵�Ԫ�ر���Ϊfalse
		HibernateValidator��չע���ࣺ
		*@Email  ��ע�͵�Ԫ�ر����ǵ��������ַ
		*@Length  ��ע�͵��ַ����Ĵ�С������ָ���ķ�Χ��
		*@NotEmpty  ��ע�͵��ַ����ı���ǿ�
		*@Range  ��ע�͵�Ԫ�ر����ں��ʵķ�Χ��
		��--------------
	 * @NotBlank(message="{valid.name}") �ǿ�.
	 * @Length(min=4, max=20, message="{valid.password}") ��������
	 * @Min(value=18, message="{valid.ageMin}")
     * @Max(value=100, message="{valid.ageMax}") �����������min һ��ʹ��.�޶���Χ
     * @Email(message="{valid.email}") ��ȷemail
     * @Past(message="{valid.birthday}") ������ǰ������.
     * @Pattern(regexp="^[a-zA-Z]{2,}$", message="{valid.address}") ����ǿ��� �����.����Ҫɶ��
     * @com.my.controller.validator.MyValidator(message="{valid.tel}", min=3) �����ɵĻ��Լ�дһ����.��У��ɶ
    */

}