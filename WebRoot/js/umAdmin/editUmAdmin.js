
//添加
var add = {

    umAdmin : {
        
                     
                               userCode : "" ,
                               sysCode : "" ,
                               adminLevel : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#userCode").val("");
           
                                      $("#sysCode").val("");
           
                                      $("#adminLevel").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umAdmin.id = $("#id").val();
                         
                                  add.umAdmin.userCode = $("#userCode").val();
                                      add.umAdmin.sysCode = $("#sysCode").val();
                                      add.umAdmin.adminLevel = $("#adminLevel").val();
                                      add.umAdmin.validInd = $("#validInd").val();
                                      add.umAdmin.createdDate = $("#createdDate").val();
                                      add.umAdmin.createdUser = $("#createdUser").val();
                                      add.umAdmin.updatedDate = $("#updatedDate").val();
                                      add.umAdmin.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umAdmin/addUmAdmin',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umAdmin),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umAdmin : {
        
                     
                               userCode : "" ,
                               sysCode : "" ,
                               adminLevel : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umAdmin/getUmAdmin?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.pkUuid);

                         
                                  $("#userCode").val(data.userCode);
           
                                      $("#sysCode").val(data.sysCode);
           
                                      $("#adminLevel").val(data.adminLevel);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umAdmin.id = $("#id").val();
                         
                              edit.umAdmin.userCode = $("#userCode").val();
                                  edit.umAdmin.sysCode = $("#sysCode").val();
                                  edit.umAdmin.adminLevel = $("#adminLevel").val();
                                  edit.umAdmin.validInd = $("#validInd").val();
                                  edit.umAdmin.createdDate = $("#createdDate").val();
                                  edit.umAdmin.createdUser = $("#createdUser").val();
                                  edit.umAdmin.updatedDate = $("#updatedDate").val();
                                  edit.umAdmin.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umAdmin/updateUmAdmin',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umAdmin),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();