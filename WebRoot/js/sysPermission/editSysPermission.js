//国际化
i18next.changeLanguage($.cookie('language'));
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢

i18next.init({
  lng: i18next.lang,
  debug: true,
  resources: {
    en: {
      translation: {
                    "permissionid" : "permissionid",
            
                    "pid" : "pid",
            
                    "name" : "name",
            
                    "type" : "type",
            
                    "url" : "url",
            
                    "state" : "state",
            
                "add" : "add",
        "edit" : "edit",
	"addConfirm" : "addConfirm",
        "addCancel" : "addCancel"

      }
    },
    ch: {
      translation: {
                    "permissionid" : "权限ID",
                    "pid" : "权限ID",
                    "name" : "名称",
                    "type" : "类型",
                    "url" : "url",
                    "state" : "状态",
                "add" : "增加",
        "edit" : "编辑",
	"addConfirm":"确认",
        "addCancel" : "取消"
      }
    }
  }
}, function(err, t) {
//	jqueryI18next.init(i18next, $);
  // init set content
  //updateContent();
});
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢  end 

var settings = {
                 '#permissionid':  i18next.t('permissionid') ,
                 '#pid':  i18next.t('pid') ,
                 '#name':  i18next.t('name') ,
                 '#type':  i18next.t('type') ,
                 '#url':  i18next.t('url') ,
                 '#state':  i18next.t('state') ,
        	
}
for(var key in settings) {
	$(key).html(settings[key]);
}
//页面js

//添加
var add = {

    sysPermission : {
        
                     
                               pid : "" ,
                               name : "" ,
                               type : "" ,
                               url : "" ,
                               state : "" ,
                   

        permissionid : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#pid").val("");
           
                                      $("#name").val("");
           
                                      $("#type").val("");
           
                                      $("#url").val("");
           
                                      $("#state").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.sysPermission.permissionid = $("#id").val();
                         
                                  add.sysPermission.pid = $("#pid").val();
                                      add.sysPermission.name = $("#name").val();
                                      add.sysPermission.type = $("#type").val();
                                      add.sysPermission.url = $("#url").val();
                                      add.sysPermission.state = $("#state").val();
                            
        $.ajax({
            url: addr + '/sysPermission/addSysPermission',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.sysPermission),
            success: function (data) {
                console.log('add = ' + data);
                 add.closeCurrentLayer();
                debugger;
                parent.location.reload();
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    sysPermission : {
        
                     
                               pid : "" ,
                               name : "" ,
                               type : "" ,
                               url : "" ,
                               state : "" ,
                   

        permissionid : null
    },
    //初始化
    init : function () {
        var permissionid  = getQueryString('permissionid');
        $.ajax({
            url: addr + '/sysPermission/getSysPermission?permissionid='+permissionid ,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#permissionid").val(data.permissionid);

                         
                                  $("#pid").val(data.pid);
           
                                      $("#name").val(data.name);
           
                                      $("#type").val(data.type);
           
                                      $("#url").val(data.url);
           
                                      $("#state").val(data.state);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.sysPermission.permissionid = $("#permissionid").val();
                         
                              edit.sysPermission.pid = $("#pid").val();
                                  edit.sysPermission.name = $("#name").val();
                                  edit.sysPermission.type = $("#type").val();
                                  edit.sysPermission.url = $("#url").val();
                                  edit.sysPermission.state = $("#state").val();
                            
        $.ajax({
            url: addr + '/sysPermission/updateSysPermission',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.sysPermission),
            success: function (data) {
                console.log('update = ' + data);
                 edit.closeCurrentLayer();
                parent.layui.table.reload('sysPermissionTable');
               
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

       
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

       
        
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


});


layui.use('form', function(){
    var form = layui.form;

    //自定义验证规则
    //form.verify({
    //    roleName: function(value){
    //        if(value.length < 4){
    //            return '请输入角色';
    //        }
    //    }
    //});

    //监听提交
    form.on('submit(editbtn)', function(data){
        debugger;

        edit.editConfirm();

        return false;

    });

    form.on('submit(addbtn)', function(data){
        debugger;

        add.addConfirm();

        return false;

    });
});

// edit.init();

