
//添加
var add = {

    umUser : {
        
                     
                               userCname : "" ,
                               userEname : "" ,
                               sex : "" ,
                               userTel : "" ,
                               status : "" ,
                               pwd : "" ,
                               pwdUpdateTime : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#userCname").val("");
           
                                      $("#userEname").val("");
           
                                      $("#sex").val("");
           
                                      $("#userTel").val("");
           
                                      $("#status").val("");
           
                                      $("#pwd").val("");
           
                                      $("#pwdUpdateTime").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umUser.id = $("#id").val();
                         
                                  add.umUser.userCname = $("#userCname").val();
                                      add.umUser.userEname = $("#userEname").val();
                                      add.umUser.sex = $("#sex").val();
                                      add.umUser.userTel = $("#userTel").val();
                                      add.umUser.status = $("#status").val();
                                      add.umUser.pwd = $("#pwd").val();
                                      add.umUser.pwdUpdateTime = $("#pwdUpdateTime").val();
                                      add.umUser.createdDate = $("#createdDate").val();
                                      add.umUser.createdUser = $("#createdUser").val();
                                      add.umUser.updatedDate = $("#updatedDate").val();
                                      add.umUser.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umUser/addUmUser',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umUser),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umUser : {
        
                     
                               userCname : "" ,
                               userEname : "" ,
                               sex : "" ,
                               userTel : "" ,
                               status : "" ,
                               pwd : "" ,
                               pwdUpdateTime : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umUser/getUmUser?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.userCode);

                         
                                  $("#userCname").val(data.userCname);
           
                                      $("#userEname").val(data.userEname);
           
                                      $("#sex").val(data.sex);
           
                                      $("#userTel").val(data.userTel);
           
                                      $("#status").val(data.status);
           
                                      $("#pwd").val(data.pwd);
           
                                      $("#pwdUpdateTime").val(data.pwdUpdateTime);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umUser.id = $("#id").val();
                         
                              edit.umUser.userCname = $("#userCname").val();
                                  edit.umUser.userEname = $("#userEname").val();
                                  edit.umUser.sex = $("#sex").val();
                                  edit.umUser.userTel = $("#userTel").val();
                                  edit.umUser.status = $("#status").val();
                                  edit.umUser.pwd = $("#pwd").val();
                                  edit.umUser.pwdUpdateTime = $("#pwdUpdateTime").val();
                                  edit.umUser.createdDate = $("#createdDate").val();
                                  edit.umUser.createdUser = $("#createdUser").val();
                                  edit.umUser.updatedDate = $("#updatedDate").val();
                                  edit.umUser.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umUser/updateUmUser',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umUser),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();