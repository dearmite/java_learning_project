//国际化
i18next.changeLanguage($.cookie('language'));
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢

i18next.init({
  lng: i18next.lang,
  debug: true,
  resources: {
    en: {
      translation: {
                    "roomExamId" : "roomExamId",
            
                    "examid" : "examid",
            
                    "roomld" : "roomld",
            
                    "date" : "date",
            
                    "time" : "time",
            
                "add" : "add",
        "edit" : "edit",
	"addConfirm" : "addConfirm",
        "addCancel" : "addCancel"

      }
    },
    ch: {
      translation: {
                    "roomExamId" : "room_exam_id",
                    "examid" : "examId",
                    "roomld" : "roomld",
                    "date" : "date",
                    "time" : "time",
                "add" : "增加",
        "edit" : "编辑",
	"addConfirm":"确认",
        "addCancel" : "取消"
      }
    }
  }
}, function(err, t) {
//	jqueryI18next.init(i18next, $);
  // init set content
  //updateContent();
});
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢  end 

var settings = {
                 '#roomExamId':  i18next.t('roomExamId') ,
                 '#examid':  i18next.t('examid') ,
                 '#roomld':  i18next.t('roomld') ,
                 '#date':  i18next.t('date') ,
                 '#time':  i18next.t('time') ,
        	
}
for(var key in settings) {
	$(key).html(settings[key]);
}
//页面js

//添加
var add = {

    roomExam : {
        
                     
                               examid : "" ,
                               roomld : "" ,
                               date : "" ,
                               time : "" ,
                   

        roomExamId : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#examid").val("");
           
                                      $("#roomld").val("");
           
                                      $("#date").val("");
           
                                      $("#time").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.roomExam.roomExamId = $("#id").val();
                         
                                  add.roomExam.examid = $("#examid").val();
                                      add.roomExam.roomld = $("#roomld").val();
                                      add.roomExam.date = $("#date").val();
                                      add.roomExam.time = $("#time").val();
                            
        $.ajax({
            url: addr + '/roomExam/addRoomExam',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.roomExam),
            success: function (data) {
                console.log('add = ' + data);
                 add.closeCurrentLayer();
                debugger;
                parent.location.reload();
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    roomExam : {
        
                     
                               examid : "" ,
                               roomld : "" ,
                               date : "" ,
                               time : "" ,
                   

        roomExamId : null
    },
    //初始化
    init : function () {
        var roomExamId  = getQueryString('roomExamId');
        $.ajax({
            url: addr + '/roomExam/getRoomExam?roomExamId='+roomExamId ,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#roomExamId").val(data.roomExamId);

                         
                                  $("#examid").val(data.examid);
           
                                      $("#roomld").val(data.roomld);
           
                                      $("#date").val(data.date);
           
                                      $("#time").val(data.time);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.roomExam.roomExamId = $("#roomExamId").val();
                         
                              edit.roomExam.examid = $("#examid").val();
                                  edit.roomExam.roomld = $("#roomld").val();
                                  edit.roomExam.date = $("#date").val();
                                  edit.roomExam.time = $("#time").val();
                            
        $.ajax({
            url: addr + '/roomExam/updateRoomExam',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.roomExam),
            success: function (data) {
                console.log('update = ' + data);
                 edit.closeCurrentLayer();
                parent.layui.table.reload('roomExamTable');
               
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

       
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

       
        
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


});


layui.use('form', function(){
    var form = layui.form;

    //自定义验证规则
    //form.verify({
    //    roleName: function(value){
    //        if(value.length < 4){
    //            return '请输入角色';
    //        }
    //    }
    //});

    //监听提交
    form.on('submit(editbtn)', function(data){
        debugger;

        edit.editConfirm();

        return false;

    });

    form.on('submit(addbtn)', function(data){
        debugger;

        add.addConfirm();

        return false;

    });
});

// edit.init();

