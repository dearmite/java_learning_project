
//添加
var add = {

    umResource : {
        
                     
                               resourceName : "" ,
                               resourceType : "" ,
                               sysCode : "" ,
                               parentId : "" ,
                               url : "" ,
                               menuIndex : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#resourceName").val("");
           
                                      $("#resourceType").val("");
           
                                      $("#sysCode").val("");
           
                                      $("#parentId").val("");
           
                                      $("#url").val("");
           
                                      $("#menuIndex").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umResource.id = $("#id").val();
                         
                                  add.umResource.resourceName = $("#resourceName").val();
                                      add.umResource.resourceType = $("#resourceType").val();
                                      add.umResource.sysCode = $("#sysCode").val();
                                      add.umResource.parentId = $("#parentId").val();
                                      add.umResource.url = $("#url").val();
                                      add.umResource.menuIndex = $("#menuIndex").val();
                                      add.umResource.validInd = $("#validInd").val();
                                      add.umResource.createdDate = $("#createdDate").val();
                                      add.umResource.createdUser = $("#createdUser").val();
                                      add.umResource.updatedDate = $("#updatedDate").val();
                                      add.umResource.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umResource/addUmResource',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umResource),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umResource : {
        
                     
                               resourceName : "" ,
                               resourceType : "" ,
                               sysCode : "" ,
                               parentId : "" ,
                               url : "" ,
                               menuIndex : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umResource/getUmResource?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.resourceId);

                         
                                  $("#resourceName").val(data.resourceName);
           
                                      $("#resourceType").val(data.resourceType);
           
                                      $("#sysCode").val(data.sysCode);
           
                                      $("#parentId").val(data.parentId);
           
                                      $("#url").val(data.url);
           
                                      $("#menuIndex").val(data.menuIndex);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umResource.id = $("#id").val();
                         
                              edit.umResource.resourceName = $("#resourceName").val();
                                  edit.umResource.resourceType = $("#resourceType").val();
                                  edit.umResource.sysCode = $("#sysCode").val();
                                  edit.umResource.parentId = $("#parentId").val();
                                  edit.umResource.url = $("#url").val();
                                  edit.umResource.menuIndex = $("#menuIndex").val();
                                  edit.umResource.validInd = $("#validInd").val();
                                  edit.umResource.createdDate = $("#createdDate").val();
                                  edit.umResource.createdUser = $("#createdUser").val();
                                  edit.umResource.updatedDate = $("#updatedDate").val();
                                  edit.umResource.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umResource/updateUmResource',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umResource),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();