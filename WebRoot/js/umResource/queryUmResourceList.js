function addUmResourceData()
{
    debugger;
    //自定页
    layer.open({
        id : "add",
        type: 2,
        title: 'add ',
        closeBtn : 1,
        area: ['300px', '500px'],
        content: addr + '/view/umResource/addUmResource.html'
    });

}

/** 简单表格 */
layui.use('table', function(){
    var table = layui.table;
    // 表格数据初始化
    table.render({
        elem: '#umResourceTable'
        ,url: addr + "/umResource/queryUmResource"
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [[
             {type:'checkbox'} ///如果不要选择,可以去掉此列

            ,  {field: 'resourceId', title: '资源编码'}
                ,  {field: 'resourceName', title: '名称'}
                ,  {field: 'resourceType', title: '1 表示菜单
            0 表示按钮'}
                ,  {field: 'sysCode', title: '系统编码'}
                ,  {field: 'parentId', title: '父资源编码'}
                ,  {field: 'url', title: 'URL'}
                ,  {field: 'menuIndex', title: '菜单序号'}
                ,  {field: 'validInd', title: '有效标志位:1-有效,0-失效'}
                ,  {field: 'createdDate', title: '创建时间'}
                ,  {field: 'createdUser', title: '创建人'}
                ,  {field: 'updatedDate', title: '修改时间'}
                ,  {field: 'updatedUser', title: '修改人'}
                   
            ,{field:'operating', templet: '#barDemo'}
        ]]
        ,page: true
    });

    // 表格监听
    table.on('tool(tableFilter)', function(obj){
        //获取数据
        var data = obj.data;
        if(obj.event === 'detail'){
            // open tab detail
        } else if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                //调用删除的方法
                var result = del(data.id);
                if (result){
                    obj.del();
                } else {
                    layer.msg('delete failure!')
                }
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            //自定页
            layer.open({
                id : "edit",
                type: 2,
                title: 'edit catalog',
                closeBtn : 1,
                area: ['300px', '500px'],
                content: addr + '/view/umResource/editUmResource.html?id=' + data.id
            });
        }

        //刷新表格:参数为表格ID
        // table.reload('table');
        // layer.close(index);
    });

    function del(id) {
        var result = false;
        $.ajax({
            url: addr + '/umResource/deleteUmResource?id='+id,
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.status == 200){
                    result = true;
                }
            }
        });
        return result;
    }
});

