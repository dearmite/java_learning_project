
//添加
var add = {

    umSystemType : {
        
                     
                               sysTypeCname : "" ,
                               parentCode : "" ,
                               sysTypeDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#sysTypeCname").val("");
           
                                      $("#parentCode").val("");
           
                                      $("#sysTypeDesc").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umSystemType.id = $("#id").val();
                         
                                  add.umSystemType.sysTypeCname = $("#sysTypeCname").val();
                                      add.umSystemType.parentCode = $("#parentCode").val();
                                      add.umSystemType.sysTypeDesc = $("#sysTypeDesc").val();
                                      add.umSystemType.validInd = $("#validInd").val();
                                      add.umSystemType.createdDate = $("#createdDate").val();
                                      add.umSystemType.createdUser = $("#createdUser").val();
                                      add.umSystemType.updatedDate = $("#updatedDate").val();
                                      add.umSystemType.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemType/addUmSystemType',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umSystemType),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umSystemType : {
        
                     
                               sysTypeCname : "" ,
                               parentCode : "" ,
                               sysTypeDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umSystemType/getUmSystemType?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.sysTypeCode);

                         
                                  $("#sysTypeCname").val(data.sysTypeCname);
           
                                      $("#parentCode").val(data.parentCode);
           
                                      $("#sysTypeDesc").val(data.sysTypeDesc);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umSystemType.id = $("#id").val();
                         
                              edit.umSystemType.sysTypeCname = $("#sysTypeCname").val();
                                  edit.umSystemType.parentCode = $("#parentCode").val();
                                  edit.umSystemType.sysTypeDesc = $("#sysTypeDesc").val();
                                  edit.umSystemType.validInd = $("#validInd").val();
                                  edit.umSystemType.createdDate = $("#createdDate").val();
                                  edit.umSystemType.createdUser = $("#createdUser").val();
                                  edit.umSystemType.updatedDate = $("#updatedDate").val();
                                  edit.umSystemType.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemType/updateUmSystemType',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umSystemType),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();