//国际化
i18next.changeLanguage($.cookie('language'));
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢

i18next.init({
  lng: i18next.lang,
  debug: true,
  resources: {
    en: {
      translation: {
                    "rolepermissionid" : "rolepermissionid",
            
                    "roleid" : "roleid",
            
                    "permissionid" : "permissionid",
            
                "add" : "add",
        "edit" : "edit",
	"addConfirm" : "addConfirm",
        "addCancel" : "addCancel"

      }
    },
    ch: {
      translation: {
                    "rolepermissionid" : "rolePermissionId",
                    "roleid" : "角色ID",
                    "permissionid" : "权限ID",
                "add" : "增加",
        "edit" : "编辑",
	"addConfirm":"确认",
        "addCancel" : "取消"
      }
    }
  }
}, function(err, t) {
//	jqueryI18next.init(i18next, $);
  // init set content
  //updateContent();
});
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢  end 

var settings = {
                 '#rolepermissionid':  i18next.t('rolepermissionid') ,
                 '#roleid':  i18next.t('roleid') ,
                 '#permissionid':  i18next.t('permissionid') ,
        	
}
for(var key in settings) {
	$(key).html(settings[key]);
}
//页面js

//添加
var add = {

    sysRolePermission : {
        
                     
                               roleid : "" ,
                               permissionid : "" ,
                   

        rolepermissionid : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#roleid").val("");
           
                                      $("#permissionid").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.sysRolePermission.rolepermissionid = $("#id").val();
                         
                                  add.sysRolePermission.roleid = $("#roleid").val();
                                      add.sysRolePermission.permissionid = $("#permissionid").val();
                            
        $.ajax({
            url: addr + '/sysRolePermission/addSysRolePermission',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.sysRolePermission),
            success: function (data) {
                console.log('add = ' + data);
                 add.closeCurrentLayer();
                debugger;
                parent.location.reload();
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    sysRolePermission : {
        
                     
                               roleid : "" ,
                               permissionid : "" ,
                   

        rolepermissionid : null
    },
    //初始化
    init : function () {
        var rolepermissionid  = getQueryString('rolepermissionid');
        $.ajax({
            url: addr + '/sysRolePermission/getSysRolePermission?rolepermissionid='+rolepermissionid ,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#rolepermissionid").val(data.rolepermissionid);

                         
                                  $("#roleid").val(data.roleid);
           
                                      $("#permissionid").val(data.permissionid);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.sysRolePermission.rolepermissionid = $("#rolepermissionid").val();
                         
                              edit.sysRolePermission.roleid = $("#roleid").val();
                                  edit.sysRolePermission.permissionid = $("#permissionid").val();
                            
        $.ajax({
            url: addr + '/sysRolePermission/updateSysRolePermission',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.sysRolePermission),
            success: function (data) {
                console.log('update = ' + data);
                 edit.closeCurrentLayer();
                parent.layui.table.reload('sysRolePermissionTable');
               
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

       
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

       
        
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


});


layui.use('form', function(){
    var form = layui.form;

    //自定义验证规则
    //form.verify({
    //    roleName: function(value){
    //        if(value.length < 4){
    //            return '请输入角色';
    //        }
    //    }
    //});

    //监听提交
    form.on('submit(editbtn)', function(data){
        debugger;

        edit.editConfirm();

        return false;

    });

    form.on('submit(addbtn)', function(data){
        debugger;

        add.addConfirm();

        return false;

    });
});

// edit.init();

