
//添加
var add = {

    userInfo : {
        
                     
                               username : "" ,
                               password : "" ,
                               usertype : "" ,
                               enabled : "" ,
                               realname : "" ,
                               qq : "" ,
                               email : "" ,
                               address : "" ,
                               tel : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#username").val("");
           
                                      $("#password").val("");
           
                                      $("#usertype").val("");
           
                                      $("#enabled").val("");
           
                                      $("#realname").val("");
           
                                      $("#qq").val("");
           
                                      $("#email").val("");
           
                                      $("#address").val("");
           
                                      $("#tel").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.userInfo.id = $("#id").val();
                         
                                  add.userInfo.username = $("#username").val();
                                      add.userInfo.password = $("#password").val();
                                      add.userInfo.usertype = $("#usertype").val();
                                      add.userInfo.enabled = $("#enabled").val();
                                      add.userInfo.realname = $("#realname").val();
                                      add.userInfo.qq = $("#qq").val();
                                      add.userInfo.email = $("#email").val();
                                      add.userInfo.address = $("#address").val();
                                      add.userInfo.tel = $("#tel").val();
                            
        $.ajax({
            url: addr + '/userInfo/addUserInfo',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.userInfo),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    userInfo : {
        
                     
                               username : "" ,
                               password : "" ,
                               usertype : "" ,
                               enabled : "" ,
                               realname : "" ,
                               qq : "" ,
                               email : "" ,
                               address : "" ,
                               tel : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/userInfo/getUserInfo?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.id);

                         
                                  $("#username").val(data.username);
           
                                      $("#password").val(data.password);
           
                                      $("#usertype").val(data.usertype);
           
                                      $("#enabled").val(data.enabled);
           
                                      $("#realname").val(data.realname);
           
                                      $("#qq").val(data.qq);
           
                                      $("#email").val(data.email);
           
                                      $("#address").val(data.address);
           
                                      $("#tel").val(data.tel);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.userInfo.id = $("#id").val();
                         
                              edit.userInfo.username = $("#username").val();
                                  edit.userInfo.password = $("#password").val();
                                  edit.userInfo.usertype = $("#usertype").val();
                                  edit.userInfo.enabled = $("#enabled").val();
                                  edit.userInfo.realname = $("#realname").val();
                                  edit.userInfo.qq = $("#qq").val();
                                  edit.userInfo.email = $("#email").val();
                                  edit.userInfo.address = $("#address").val();
                                  edit.userInfo.tel = $("#tel").val();
                            
        $.ajax({
            url: addr + '/userInfo/updateUserInfo',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.userInfo),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();