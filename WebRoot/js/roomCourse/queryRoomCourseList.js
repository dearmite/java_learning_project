//国际化
i18next.changeLanguage($.cookie('language'));
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢

i18next.init({
  lng: i18next.lang,
  debug: true,
  resources: {
    en: {
      translation: {
                    "roomCourseId" : "roomCourseId",
            
                    "courseid" : "courseid",
            
                    "roomid" : "roomid",
            
                    "time" : "time",
            
                    "date" : "date",
            
                "add" : "add",
        "edit" : "edit",
	"addConfirm" : "addConfirm",
        "addCancel" : "addCancel"

      }
    },
    ch: {
      translation: {
                    "roomCourseId" : "room_course_Id",
                    "courseid" : "courseId",
                    "roomid" : "roomId",
                    "time" : "time",
                    "date" : "date",
                "add" : "增加",
        "edit" : "编辑",
	"addConfirm":"确认",
        "addCancel" : "取消"
      }
    }
  }
}, function(err, t) {
//	jqueryI18next.init(i18next, $);
  // init set content
  //updateContent();
});
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢  end 


var settings = {
                 '#roomCourseId':  i18next.t('roomCourseId') ,
                 '#courseid':  i18next.t('courseid') ,
                 '#roomid':  i18next.t('roomid') ,
                 '#time':  i18next.t('time') ,
                 '#date':  i18next.t('date') ,
        	
}
for(var key in settings) {
	$(key).html(settings[key]);
}
//页面js
layui. addRoomCourseData = function()
{
    debugger;
    //自定页
    layer.open({
        id : "add",
        type: 2,
        title: i18next.t("add"),
        closeBtn : 1,
        area: ['330px', '180px'],
        content: addr + '/view/roomCourse/addRoomCourse.html'
    });

}

/** 简单表格 */
var limits = [10, 20, 30, 40, 50],
	limit = 10;
layui.use('table', function(){
    var table = layui.table;
    // 表格数据初始化
    table.render({
        elem: '#roomCourseTable'
        ,url: addr + "/roomCourse/queryRoomCourse"
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [[
             {type:'checkbox'} ///如果不要选择,可以去掉此列

            ,  {field: 'roomCourseId', title: i18next.t('roomCourseId')}
                ,  {field: 'courseid', title: i18next.t('courseid')}
                ,  {field: 'roomid', title: i18next.t('roomid')}
                ,  {field: 'time', title: i18next.t('time')}
                ,  {field: 'date', title: i18next.t('date')}
                   
            ,{field:'operating',
            title: i18next.t('voperating'),
            templet: '#barDemo'}
        ]],
        done: function(res, curr, count) {
			$('.layui-laypage-skip')[0].innerHTML = i18next.t('toPage') + '<input type="text" min="1" value="1" class="layui-input">' + i18next.t('pageTrans') + '<button type="button" class="layui-laypage-btn">' + i18next.t('okBtn') + '</button>';
			$('.layui-laypage-count')[0].innerHTML = i18next.t('total') + count + i18next.t('item');
			var e = ['<span class="layui-laypage-limits"><select lay-ignore>'];
			layui.each(limits, function(t, n) {
				e.push('<option value="' + n + '"' + (n === limit ? "selected" : "") + ">" + n + i18next.t('rowPage') + "</option>")
			}), e.join("") + "</select></span>";
			$('.layui-laypage-limits')[0].innerHTML = e;
		},
		page: {
			layout: ['prev', 'page', 'next', 'skip', 'count', 'limit'],
			groups: 1,
			next: '<span class="layui-icon">&#xe602;</span>',
			prev: '<span class="layui-icon">&#xe603;</span>',
			limits: limits,
			limit: limit
		}
    });

    // 表格监听
    table.on('tool(tableFilter)', function(obj){
        //获取数据
        var data = obj.data;
        if(obj.event === 'detail'){
            // open tab detail
        } else if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                //调用删除的方法
                var result = del(data.roomCourseId);
                if (result){
                    obj.del();
                } else {
                    layer.msg('delete failure!')
                }
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            //自定页
            layer.open({
                id : "edit",
                type: 2,
                title: i18next.t("edit") ,
                closeBtn : 1,
                area: ['300px', '500px'],
                content: addr + '/view/roomCourse/editRoomCourse.html?roomCourseId=' + data.roomCourseId
            });
        }

        //刷新表格:参数为表格ID
        // table.reload('table');
        // layer.close(index);
    });

    function del(roomCourseId) {
        var result = false;
        $.ajax(
        {
            url: addr + '/roomCourse/deleteRoomCourse?roomCourseId='+roomCourseId,
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.status == 200){
                    result = true;
                }
            }
        });
        return result;
    }
});

