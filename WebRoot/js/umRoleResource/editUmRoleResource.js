
//添加
var add = {

    umRoleResource : {
        
                     
                               roleCode : "" ,
                               resourceId : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#roleCode").val("");
           
                                      $("#resourceId").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umRoleResource.id = $("#id").val();
                         
                                  add.umRoleResource.roleCode = $("#roleCode").val();
                                      add.umRoleResource.resourceId = $("#resourceId").val();
                                      add.umRoleResource.validInd = $("#validInd").val();
                                      add.umRoleResource.createdDate = $("#createdDate").val();
                                      add.umRoleResource.createdUser = $("#createdUser").val();
                                      add.umRoleResource.updatedDate = $("#updatedDate").val();
                                      add.umRoleResource.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umRoleResource/addUmRoleResource',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umRoleResource),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umRoleResource : {
        
                     
                               roleCode : "" ,
                               resourceId : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umRoleResource/getUmRoleResource?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.pkUuid);

                         
                                  $("#roleCode").val(data.roleCode);
           
                                      $("#resourceId").val(data.resourceId);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umRoleResource.id = $("#id").val();
                         
                              edit.umRoleResource.roleCode = $("#roleCode").val();
                                  edit.umRoleResource.resourceId = $("#resourceId").val();
                                  edit.umRoleResource.validInd = $("#validInd").val();
                                  edit.umRoleResource.createdDate = $("#createdDate").val();
                                  edit.umRoleResource.createdUser = $("#createdUser").val();
                                  edit.umRoleResource.updatedDate = $("#updatedDate").val();
                                  edit.umRoleResource.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umRoleResource/updateUmRoleResource',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umRoleResource),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();