function addUmDepartmentData()
{
    debugger;
    //自定页
    layer.open({
        id : "add",
        type: 2,
        title: 'add ',
        closeBtn : 1,
        area: ['300px', '500px'],
        content: addr + '/view/umDepartment/addUmDepartment.html'
    });

}

/** 简单表格 */
layui.use('table', function(){
    var table = layui.table;
    // 表格数据初始化
    table.render({
        elem: '#umDepartmentTable'
        ,url: addr + "/umDepartment/queryUmDepartment"
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [[
             {type:'checkbox'} ///如果不要选择,可以去掉此列

            ,  {field: 'dptCde', title: '机构代码'}
                ,  {field: 'dptCnm', title: '机构名称'}
                ,  {field: 'cDptEnm', title: '英文名称'}
                ,  {field: 'cSnrDpt', title: '父机构代码'}
                ,  {field: 'nDptLevl', title: '机构级别'}
                ,  {field: 'dptRelCde', title: '机构关系码'}
                ,  {field: 'insprmtNo', title: '保险许可证'}
                ,  {field: 'rptAddr', title: '报案地址'}
                ,  {field: 'rptTel', title: '报案电话'}
                ,  {field: 'alarmMrk', title: '报警标志'}
                ,  {field: 'departmentMrk', title: '部门标志'}
                ,  {field: 'fax', title: '传真'}
                ,  {field: 'tel', title: '联系电话'}
                ,  {field: 'consTel', title: '咨询电话'}
                ,  {field: 'dptCaddr', title: '地址'}
                ,  {field: 'dptEaddr', title: '英文地址'}
                ,  {field: 'zipCde', title: '邮编'}
                ,  {field: 'dptAbr', title: '机构简称'}
                ,  {field: 'dptSerno', title: '机构流水号'}
                ,  {field: 'ctctCde', title: '联系人'}
                ,  {field: 'interCde', title: '内码'}
                ,  {field: 'abdTm', title: '失效时间'}
                ,  {field: 'fndTm', title: '成立时间'}
                ,  {field: 'taxrgstNo', title: '税务登记号'}
                ,  {field: 'lcnAbr', title: '营业执照'}
                ,  {field: 'bnsrgstNo', title: '展业资格证'}
                ,  {field: 'validInd', title: '有效标志位:1-有效,0-失效'}
                ,  {field: 'createdDate', title: '创建时间'}
                ,  {field: 'createdUser', title: '创建人'}
                ,  {field: 'updatedDate', title: '修改时间'}
                ,  {field: 'updatedUser', title: '修改人'}
                   
            ,{field:'operating', templet: '#barDemo'}
        ]]
        ,page: true
    });

    // 表格监听
    table.on('tool(tableFilter)', function(obj){
        //获取数据
        var data = obj.data;
        if(obj.event === 'detail'){
            // open tab detail
        } else if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                //调用删除的方法
                var result = del(data.id);
                if (result){
                    obj.del();
                } else {
                    layer.msg('delete failure!')
                }
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            //自定页
            layer.open({
                id : "edit",
                type: 2,
                title: 'edit catalog',
                closeBtn : 1,
                area: ['300px', '500px'],
                content: addr + '/view/umDepartment/editUmDepartment.html?id=' + data.id
            });
        }

        //刷新表格:参数为表格ID
        // table.reload('table');
        // layer.close(index);
    });

    function del(id) {
        var result = false;
        $.ajax({
            url: addr + '/umDepartment/deleteUmDepartment?id='+id,
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.status == 200){
                    result = true;
                }
            }
        });
        return result;
    }
});

