
//添加
var add = {

    umDepartment : {
        
                     
                               dptCnm : "" ,
                               cDptEnm : "" ,
                               cSnrDpt : "" ,
                               nDptLevl : "" ,
                               dptRelCde : "" ,
                               insprmtNo : "" ,
                               rptAddr : "" ,
                               rptTel : "" ,
                               alarmMrk : "" ,
                               departmentMrk : "" ,
                               fax : "" ,
                               tel : "" ,
                               consTel : "" ,
                               dptCaddr : "" ,
                               dptEaddr : "" ,
                               zipCde : "" ,
                               dptAbr : "" ,
                               dptSerno : "" ,
                               ctctCde : "" ,
                               interCde : "" ,
                               abdTm : "" ,
                               fndTm : "" ,
                               taxrgstNo : "" ,
                               lcnAbr : "" ,
                               bnsrgstNo : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#dptCnm").val("");
           
                                      $("#cDptEnm").val("");
           
                                      $("#cSnrDpt").val("");
           
                                      $("#nDptLevl").val("");
           
                                      $("#dptRelCde").val("");
           
                                      $("#insprmtNo").val("");
           
                                      $("#rptAddr").val("");
           
                                      $("#rptTel").val("");
           
                                      $("#alarmMrk").val("");
           
                                      $("#departmentMrk").val("");
           
                                      $("#fax").val("");
           
                                      $("#tel").val("");
           
                                      $("#consTel").val("");
           
                                      $("#dptCaddr").val("");
           
                                      $("#dptEaddr").val("");
           
                                      $("#zipCde").val("");
           
                                      $("#dptAbr").val("");
           
                                      $("#dptSerno").val("");
           
                                      $("#ctctCde").val("");
           
                                      $("#interCde").val("");
           
                                      $("#abdTm").val("");
           
                                      $("#fndTm").val("");
           
                                      $("#taxrgstNo").val("");
           
                                      $("#lcnAbr").val("");
           
                                      $("#bnsrgstNo").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umDepartment.id = $("#id").val();
                         
                                  add.umDepartment.dptCnm = $("#dptCnm").val();
                                      add.umDepartment.cDptEnm = $("#cDptEnm").val();
                                      add.umDepartment.cSnrDpt = $("#cSnrDpt").val();
                                      add.umDepartment.nDptLevl = $("#nDptLevl").val();
                                      add.umDepartment.dptRelCde = $("#dptRelCde").val();
                                      add.umDepartment.insprmtNo = $("#insprmtNo").val();
                                      add.umDepartment.rptAddr = $("#rptAddr").val();
                                      add.umDepartment.rptTel = $("#rptTel").val();
                                      add.umDepartment.alarmMrk = $("#alarmMrk").val();
                                      add.umDepartment.departmentMrk = $("#departmentMrk").val();
                                      add.umDepartment.fax = $("#fax").val();
                                      add.umDepartment.tel = $("#tel").val();
                                      add.umDepartment.consTel = $("#consTel").val();
                                      add.umDepartment.dptCaddr = $("#dptCaddr").val();
                                      add.umDepartment.dptEaddr = $("#dptEaddr").val();
                                      add.umDepartment.zipCde = $("#zipCde").val();
                                      add.umDepartment.dptAbr = $("#dptAbr").val();
                                      add.umDepartment.dptSerno = $("#dptSerno").val();
                                      add.umDepartment.ctctCde = $("#ctctCde").val();
                                      add.umDepartment.interCde = $("#interCde").val();
                                      add.umDepartment.abdTm = $("#abdTm").val();
                                      add.umDepartment.fndTm = $("#fndTm").val();
                                      add.umDepartment.taxrgstNo = $("#taxrgstNo").val();
                                      add.umDepartment.lcnAbr = $("#lcnAbr").val();
                                      add.umDepartment.bnsrgstNo = $("#bnsrgstNo").val();
                                      add.umDepartment.validInd = $("#validInd").val();
                                      add.umDepartment.createdDate = $("#createdDate").val();
                                      add.umDepartment.createdUser = $("#createdUser").val();
                                      add.umDepartment.updatedDate = $("#updatedDate").val();
                                      add.umDepartment.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umDepartment/addUmDepartment',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umDepartment),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umDepartment : {
        
                     
                               dptCnm : "" ,
                               cDptEnm : "" ,
                               cSnrDpt : "" ,
                               nDptLevl : "" ,
                               dptRelCde : "" ,
                               insprmtNo : "" ,
                               rptAddr : "" ,
                               rptTel : "" ,
                               alarmMrk : "" ,
                               departmentMrk : "" ,
                               fax : "" ,
                               tel : "" ,
                               consTel : "" ,
                               dptCaddr : "" ,
                               dptEaddr : "" ,
                               zipCde : "" ,
                               dptAbr : "" ,
                               dptSerno : "" ,
                               ctctCde : "" ,
                               interCde : "" ,
                               abdTm : "" ,
                               fndTm : "" ,
                               taxrgstNo : "" ,
                               lcnAbr : "" ,
                               bnsrgstNo : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umDepartment/getUmDepartment?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.dptCde);

                         
                                  $("#dptCnm").val(data.dptCnm);
           
                                      $("#cDptEnm").val(data.cDptEnm);
           
                                      $("#cSnrDpt").val(data.cSnrDpt);
           
                                      $("#nDptLevl").val(data.nDptLevl);
           
                                      $("#dptRelCde").val(data.dptRelCde);
           
                                      $("#insprmtNo").val(data.insprmtNo);
           
                                      $("#rptAddr").val(data.rptAddr);
           
                                      $("#rptTel").val(data.rptTel);
           
                                      $("#alarmMrk").val(data.alarmMrk);
           
                                      $("#departmentMrk").val(data.departmentMrk);
           
                                      $("#fax").val(data.fax);
           
                                      $("#tel").val(data.tel);
           
                                      $("#consTel").val(data.consTel);
           
                                      $("#dptCaddr").val(data.dptCaddr);
           
                                      $("#dptEaddr").val(data.dptEaddr);
           
                                      $("#zipCde").val(data.zipCde);
           
                                      $("#dptAbr").val(data.dptAbr);
           
                                      $("#dptSerno").val(data.dptSerno);
           
                                      $("#ctctCde").val(data.ctctCde);
           
                                      $("#interCde").val(data.interCde);
           
                                      $("#abdTm").val(data.abdTm);
           
                                      $("#fndTm").val(data.fndTm);
           
                                      $("#taxrgstNo").val(data.taxrgstNo);
           
                                      $("#lcnAbr").val(data.lcnAbr);
           
                                      $("#bnsrgstNo").val(data.bnsrgstNo);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umDepartment.id = $("#id").val();
                         
                              edit.umDepartment.dptCnm = $("#dptCnm").val();
                                  edit.umDepartment.cDptEnm = $("#cDptEnm").val();
                                  edit.umDepartment.cSnrDpt = $("#cSnrDpt").val();
                                  edit.umDepartment.nDptLevl = $("#nDptLevl").val();
                                  edit.umDepartment.dptRelCde = $("#dptRelCde").val();
                                  edit.umDepartment.insprmtNo = $("#insprmtNo").val();
                                  edit.umDepartment.rptAddr = $("#rptAddr").val();
                                  edit.umDepartment.rptTel = $("#rptTel").val();
                                  edit.umDepartment.alarmMrk = $("#alarmMrk").val();
                                  edit.umDepartment.departmentMrk = $("#departmentMrk").val();
                                  edit.umDepartment.fax = $("#fax").val();
                                  edit.umDepartment.tel = $("#tel").val();
                                  edit.umDepartment.consTel = $("#consTel").val();
                                  edit.umDepartment.dptCaddr = $("#dptCaddr").val();
                                  edit.umDepartment.dptEaddr = $("#dptEaddr").val();
                                  edit.umDepartment.zipCde = $("#zipCde").val();
                                  edit.umDepartment.dptAbr = $("#dptAbr").val();
                                  edit.umDepartment.dptSerno = $("#dptSerno").val();
                                  edit.umDepartment.ctctCde = $("#ctctCde").val();
                                  edit.umDepartment.interCde = $("#interCde").val();
                                  edit.umDepartment.abdTm = $("#abdTm").val();
                                  edit.umDepartment.fndTm = $("#fndTm").val();
                                  edit.umDepartment.taxrgstNo = $("#taxrgstNo").val();
                                  edit.umDepartment.lcnAbr = $("#lcnAbr").val();
                                  edit.umDepartment.bnsrgstNo = $("#bnsrgstNo").val();
                                  edit.umDepartment.validInd = $("#validInd").val();
                                  edit.umDepartment.createdDate = $("#createdDate").val();
                                  edit.umDepartment.createdUser = $("#createdUser").val();
                                  edit.umDepartment.updatedDate = $("#updatedDate").val();
                                  edit.umDepartment.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umDepartment/updateUmDepartment',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umDepartment),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();