
//添加
var add = {

    hrUserDepartment : {
        
                     
                               employeename : "" ,
                               sex : "" ,
                               companyid : "" ,
                               account : "" ,
                               email : "" ,
                               phone : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#employeename").val("");
           
                                      $("#sex").val("");
           
                                      $("#companyid").val("");
           
                                      $("#account").val("");
           
                                      $("#email").val("");
           
                                      $("#phone").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.hrUserDepartment.id = $("#id").val();
                         
                                  add.hrUserDepartment.employeename = $("#employeename").val();
                                      add.hrUserDepartment.sex = $("#sex").val();
                                      add.hrUserDepartment.companyid = $("#companyid").val();
                                      add.hrUserDepartment.account = $("#account").val();
                                      add.hrUserDepartment.email = $("#email").val();
                                      add.hrUserDepartment.phone = $("#phone").val();
                            
        $.ajax({
            url: addr + '/hrUserDepartment/addHrUserDepartment',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.hrUserDepartment),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    hrUserDepartment : {
        
                     
                               employeename : "" ,
                               sex : "" ,
                               companyid : "" ,
                               account : "" ,
                               email : "" ,
                               phone : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/hrUserDepartment/getHrUserDepartment?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.emId);

                         
                                  $("#employeename").val(data.employeename);
           
                                      $("#sex").val(data.sex);
           
                                      $("#companyid").val(data.companyid);
           
                                      $("#account").val(data.account);
           
                                      $("#email").val(data.email);
           
                                      $("#phone").val(data.phone);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.hrUserDepartment.id = $("#id").val();
                         
                              edit.hrUserDepartment.employeename = $("#employeename").val();
                                  edit.hrUserDepartment.sex = $("#sex").val();
                                  edit.hrUserDepartment.companyid = $("#companyid").val();
                                  edit.hrUserDepartment.account = $("#account").val();
                                  edit.hrUserDepartment.email = $("#email").val();
                                  edit.hrUserDepartment.phone = $("#phone").val();
                            
        $.ajax({
            url: addr + '/hrUserDepartment/updateHrUserDepartment',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.hrUserDepartment),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();