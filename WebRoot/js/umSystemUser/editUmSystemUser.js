
//添加
var add = {

    umSystemUser : {
        
                     
                               userCode : "" ,
                               sysCode : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#userCode").val("");
           
                                      $("#sysCode").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umSystemUser.id = $("#id").val();
                         
                                  add.umSystemUser.userCode = $("#userCode").val();
                                      add.umSystemUser.sysCode = $("#sysCode").val();
                                      add.umSystemUser.validInd = $("#validInd").val();
                                      add.umSystemUser.createdDate = $("#createdDate").val();
                                      add.umSystemUser.createdUser = $("#createdUser").val();
                                      add.umSystemUser.updatedDate = $("#updatedDate").val();
                                      add.umSystemUser.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemUser/addUmSystemUser',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umSystemUser),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umSystemUser : {
        
                     
                               userCode : "" ,
                               sysCode : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umSystemUser/getUmSystemUser?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.sysUserId);

                         
                                  $("#userCode").val(data.userCode);
           
                                      $("#sysCode").val(data.sysCode);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umSystemUser.id = $("#id").val();
                         
                              edit.umSystemUser.userCode = $("#userCode").val();
                                  edit.umSystemUser.sysCode = $("#sysCode").val();
                                  edit.umSystemUser.validInd = $("#validInd").val();
                                  edit.umSystemUser.createdDate = $("#createdDate").val();
                                  edit.umSystemUser.createdUser = $("#createdUser").val();
                                  edit.umSystemUser.updatedDate = $("#updatedDate").val();
                                  edit.umSystemUser.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemUser/updateUmSystemUser',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umSystemUser),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();