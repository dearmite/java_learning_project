
//添加
var add = {

    country : {
        
                     
                               countryname : "" ,
                               countrycode : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#countryname").val("");
           
                                      $("#countrycode").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.country.id = $("#id").val();
                         
                                  add.country.countryname = $("#countryname").val();
                                      add.country.countrycode = $("#countrycode").val();
                            
        $.ajax({
            url: addr + '/country/addCountry',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.country),
            success: function (data) {
                console.log('add = ' + data);
		parent.layui.table.reload('countryTable');
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    country : {
        
                     
                               countryname : "" ,
                               countrycode : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/country/getCountry?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.id);

                         
                                  $("#countryname").val(data.countryname);
           
                                      $("#countrycode").val(data.countrycode);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.country.id = $("#id").val();
                         
                              edit.country.countryname = $("#countryname").val();
                                  edit.country.countrycode = $("#countrycode").val();
                            
        $.ajax({
            url: addr + '/country/updateCountry',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.country),
            success: function (data) {
                console.log('update = ' + data)
                console.log(parent.layui.table.reload('countryTable'));
                parent.layui.table.reload('countryTable');
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();