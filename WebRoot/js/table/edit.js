
//添加
var add = {

};

//删除
var del = {

};

//编辑
var edit = {
    userInfo : {
        id : null,
        username : "",
        address : "",
        email : "",
        tel : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/country/getCountry?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.id);
        $("#username").val(data.countryname);
        $("#address").val(data.countrycode);

    },
    //编辑确认
    editConfirm : function () {
        debugger;
        edit.userInfo.id = $("#id").val();
        edit.userInfo.countryName = $("#username").val();
        edit.userInfo.countryCode = $("#address").val();
        edit.userInfo.tel = $("#tel").val();
        edit.userInfo.email = $("#email").val();
        $.ajax({
            url: addr + '/country/updateCountry',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.userInfo),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#confirm").click(function () {
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#cancle").click(function () {
        edit.closeCurrentLayer();
    });

})

// edit.init();