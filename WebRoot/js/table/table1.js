
/** 简单表格 */
layui.use('table', function(){
    var table = layui.table;
    // 表格数据初始化
    table.render({
        elem: '#table'
        ,url: addr + "/country/queryCountry"
        ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [[
            {field: 'id', title: 'ID', style: 'background-color: #009688; color: #fff;'}
            ,  {field: 'countryname', width: 150, title: 'catalog', templet: '#cnameTpl'}
            ,  {field: 'countrycode', title: 'sub-catalog'}
            ,{field:'operating', templet: '#barDemo'}
        ]]
        ,page: true
    });

    // 表格监听
    table.on('tool(tableFilter)', function(obj){
        //获取数据
        var data = obj.data;
        if(obj.event === 'detail'){
            // open tab detail
        } else if(obj.event === 'del'){
            layer.confirm('真的删除行么', function(index){
                //调用删除的方法
                var result = del(data.id);
                if (result){
                    obj.del();
                } else {
                    layer.msg('delete failure!')
                }
                layer.close(index);
            });
        } else if(obj.event === 'edit'){
            //自定页
            layer.open({
                id : "edit",
                type: 2,
                title: 'edit catalog',
                closeBtn : 1,
                area: ['300px', '500px'],
                content: addr + '/view/table/edit.html?id=' + data.id
            });
        }

        //刷新表格:参数为表格ID
        // table.reload('table');
        // layer.close(index);
    });

    function del(id) {
        var result = false;
        $.ajax({
            url: addr + '/country/deleteCountry?id='+id,
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.status == 200){
                    result = true;
                }
            }
        });
        return result;
    }
});

