
//添加
var add = {

    umRole : {
        
                     
                               sysCode : "" ,
                               roleCname : "" ,
                               roleDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#sysCode").val("");
           
                                      $("#roleCname").val("");
           
                                      $("#roleDesc").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umRole.id = $("#id").val();
                         
                                  add.umRole.sysCode = $("#sysCode").val();
                                      add.umRole.roleCname = $("#roleCname").val();
                                      add.umRole.roleDesc = $("#roleDesc").val();
                                      add.umRole.validInd = $("#validInd").val();
                                      add.umRole.createdDate = $("#createdDate").val();
                                      add.umRole.createdUser = $("#createdUser").val();
                                      add.umRole.updatedDate = $("#updatedDate").val();
                                      add.umRole.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umRole/addUmRole',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umRole),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umRole : {
        
                     
                               sysCode : "" ,
                               roleCname : "" ,
                               roleDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umRole/getUmRole?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.roleCode);

                         
                                  $("#sysCode").val(data.sysCode);
           
                                      $("#roleCname").val(data.roleCname);
           
                                      $("#roleDesc").val(data.roleDesc);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umRole.id = $("#id").val();
                         
                              edit.umRole.sysCode = $("#sysCode").val();
                                  edit.umRole.roleCname = $("#roleCname").val();
                                  edit.umRole.roleDesc = $("#roleDesc").val();
                                  edit.umRole.validInd = $("#validInd").val();
                                  edit.umRole.createdDate = $("#createdDate").val();
                                  edit.umRole.createdUser = $("#createdUser").val();
                                  edit.umRole.updatedDate = $("#updatedDate").val();
                                  edit.umRole.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umRole/updateUmRole',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umRole),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();