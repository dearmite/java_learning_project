
//添加
var add = {

    umSystemExt : {
        
                     
                               sysCode : "" ,
                               sysAttr : "" ,
                               sysAttrVal : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#sysCode").val("");
           
                                      $("#sysAttr").val("");
           
                                      $("#sysAttrVal").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umSystemExt.id = $("#id").val();
                         
                                  add.umSystemExt.sysCode = $("#sysCode").val();
                                      add.umSystemExt.sysAttr = $("#sysAttr").val();
                                      add.umSystemExt.sysAttrVal = $("#sysAttrVal").val();
                                      add.umSystemExt.validInd = $("#validInd").val();
                                      add.umSystemExt.createdDate = $("#createdDate").val();
                                      add.umSystemExt.createdUser = $("#createdUser").val();
                                      add.umSystemExt.updatedDate = $("#updatedDate").val();
                                      add.umSystemExt.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemExt/addUmSystemExt',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umSystemExt),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umSystemExt : {
        
                     
                               sysCode : "" ,
                               sysAttr : "" ,
                               sysAttrVal : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umSystemExt/getUmSystemExt?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.pkUuid);

                         
                                  $("#sysCode").val(data.sysCode);
           
                                      $("#sysAttr").val(data.sysAttr);
           
                                      $("#sysAttrVal").val(data.sysAttrVal);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umSystemExt.id = $("#id").val();
                         
                              edit.umSystemExt.sysCode = $("#sysCode").val();
                                  edit.umSystemExt.sysAttr = $("#sysAttr").val();
                                  edit.umSystemExt.sysAttrVal = $("#sysAttrVal").val();
                                  edit.umSystemExt.validInd = $("#validInd").val();
                                  edit.umSystemExt.createdDate = $("#createdDate").val();
                                  edit.umSystemExt.createdUser = $("#createdUser").val();
                                  edit.umSystemExt.updatedDate = $("#updatedDate").val();
                                  edit.umSystemExt.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystemExt/updateUmSystemExt',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umSystemExt),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();