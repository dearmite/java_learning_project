
//添加
var add = {

    umSystem : {
        
                     
                               sysTypeCode : "" ,
                               sysCname : "" ,
                               sysEname : "" ,
                               sysUrl : "" ,
                               sysDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#sysTypeCode").val("");
           
                                      $("#sysCname").val("");
           
                                      $("#sysEname").val("");
           
                                      $("#sysUrl").val("");
           
                                      $("#sysDesc").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umSystem.id = $("#id").val();
                         
                                  add.umSystem.sysTypeCode = $("#sysTypeCode").val();
                                      add.umSystem.sysCname = $("#sysCname").val();
                                      add.umSystem.sysEname = $("#sysEname").val();
                                      add.umSystem.sysUrl = $("#sysUrl").val();
                                      add.umSystem.sysDesc = $("#sysDesc").val();
                                      add.umSystem.validInd = $("#validInd").val();
                                      add.umSystem.createdDate = $("#createdDate").val();
                                      add.umSystem.createdUser = $("#createdUser").val();
                                      add.umSystem.updatedDate = $("#updatedDate").val();
                                      add.umSystem.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystem/addUmSystem',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umSystem),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umSystem : {
        
                     
                               sysTypeCode : "" ,
                               sysCname : "" ,
                               sysEname : "" ,
                               sysUrl : "" ,
                               sysDesc : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umSystem/getUmSystem?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.sysCode);

                         
                                  $("#sysTypeCode").val(data.sysTypeCode);
           
                                      $("#sysCname").val(data.sysCname);
           
                                      $("#sysEname").val(data.sysEname);
           
                                      $("#sysUrl").val(data.sysUrl);
           
                                      $("#sysDesc").val(data.sysDesc);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umSystem.id = $("#id").val();
                         
                              edit.umSystem.sysTypeCode = $("#sysTypeCode").val();
                                  edit.umSystem.sysCname = $("#sysCname").val();
                                  edit.umSystem.sysEname = $("#sysEname").val();
                                  edit.umSystem.sysUrl = $("#sysUrl").val();
                                  edit.umSystem.sysDesc = $("#sysDesc").val();
                                  edit.umSystem.validInd = $("#validInd").val();
                                  edit.umSystem.createdDate = $("#createdDate").val();
                                  edit.umSystem.createdUser = $("#createdUser").val();
                                  edit.umSystem.updatedDate = $("#updatedDate").val();
                                  edit.umSystem.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSystem/updateUmSystem',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umSystem),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();