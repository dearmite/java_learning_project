
//添加
var add = {

    umUserDept : {
        
                     
                               deptCode : "" ,
                               userCode : "" ,
                               defaultFlag : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#deptCode").val("");
           
                                      $("#userCode").val("");
           
                                      $("#defaultFlag").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umUserDept.id = $("#id").val();
                         
                                  add.umUserDept.deptCode = $("#deptCode").val();
                                      add.umUserDept.userCode = $("#userCode").val();
                                      add.umUserDept.defaultFlag = $("#defaultFlag").val();
                                      add.umUserDept.validInd = $("#validInd").val();
                                      add.umUserDept.createdDate = $("#createdDate").val();
                                      add.umUserDept.createdUser = $("#createdUser").val();
                                      add.umUserDept.updatedDate = $("#updatedDate").val();
                                      add.umUserDept.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umUserDept/addUmUserDept',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umUserDept),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umUserDept : {
        
                     
                               deptCode : "" ,
                               userCode : "" ,
                               defaultFlag : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umUserDept/getUmUserDept?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.pkUuid);

                         
                                  $("#deptCode").val(data.deptCode);
           
                                      $("#userCode").val(data.userCode);
           
                                      $("#defaultFlag").val(data.defaultFlag);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umUserDept.id = $("#id").val();
                         
                              edit.umUserDept.deptCode = $("#deptCode").val();
                                  edit.umUserDept.userCode = $("#userCode").val();
                                  edit.umUserDept.defaultFlag = $("#defaultFlag").val();
                                  edit.umUserDept.validInd = $("#validInd").val();
                                  edit.umUserDept.createdDate = $("#createdDate").val();
                                  edit.umUserDept.createdUser = $("#createdUser").val();
                                  edit.umUserDept.updatedDate = $("#updatedDate").val();
                                  edit.umUserDept.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umUserDept/updateUmUserDept',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umUserDept),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();