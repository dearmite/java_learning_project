
//添加
var add = {

    umSysUserRole : {
        
                     
                               sysUserId : "" ,
                               roleCode : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#sysUserId").val("");
           
                                      $("#roleCode").val("");
           
                                      $("#validInd").val("");
           
                                      $("#createdDate").val("");
           
                                      $("#createdUser").val("");
           
                                      $("#updatedDate").val("");
           
                                      $("#updatedUser").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.umSysUserRole.id = $("#id").val();
                         
                                  add.umSysUserRole.sysUserId = $("#sysUserId").val();
                                      add.umSysUserRole.roleCode = $("#roleCode").val();
                                      add.umSysUserRole.validInd = $("#validInd").val();
                                      add.umSysUserRole.createdDate = $("#createdDate").val();
                                      add.umSysUserRole.createdUser = $("#createdUser").val();
                                      add.umSysUserRole.updatedDate = $("#updatedDate").val();
                                      add.umSysUserRole.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSysUserRole/addUmSysUserRole',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.umSysUserRole),
            success: function (data) {
                console.log('add = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    umSysUserRole : {
        
                     
                               sysUserId : "" ,
                               roleCode : "" ,
                               validInd : "" ,
                               createdDate : "" ,
                               createdUser : "" ,
                               updatedDate : "" ,
                               updatedUser : "" ,
                   

        id : null
    },
    //初始化
    init : function () {
        var id = getQueryString('id');
        $.ajax({
            url: addr + '/umSysUserRole/getUmSysUserRole?id='+id,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val(data.pkUuid);

                         
                                  $("#sysUserId").val(data.sysUserId);
           
                                      $("#roleCode").val(data.roleCode);
           
                                      $("#validInd").val(data.validInd);
           
                                      $("#createdDate").val(data.createdDate);
           
                                      $("#createdUser").val(data.createdUser);
           
                                      $("#updatedDate").val(data.updatedDate);
           
                                      $("#updatedUser").val(data.updatedUser);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.umSysUserRole.id = $("#id").val();
                         
                              edit.umSysUserRole.sysUserId = $("#sysUserId").val();
                                  edit.umSysUserRole.roleCode = $("#roleCode").val();
                                  edit.umSysUserRole.validInd = $("#validInd").val();
                                  edit.umSysUserRole.createdDate = $("#createdDate").val();
                                  edit.umSysUserRole.createdUser = $("#createdUser").val();
                                  edit.umSysUserRole.updatedDate = $("#updatedDate").val();
                                  edit.umSysUserRole.updatedUser = $("#updatedUser").val();
                            
        $.ajax({
            url: addr + '/umSysUserRole/updateUmSysUserRole',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.umSysUserRole),
            success: function (data) {
                console.log('update = ' + data)
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

        add.closeCurrentLayer();
        parent.location.reload();
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

        edit.closeCurrentLayer();
        parent.location.reload();
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


})

// edit.init();