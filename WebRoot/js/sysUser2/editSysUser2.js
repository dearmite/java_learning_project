//国际化
i18next.changeLanguage($.cookie('language'));
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢

i18next.init({
  lng: i18next.lang,
  debug: true,
  resources: {
    en: {
      translation: {
                    "userid" : "userid",
            
                    "name" : "name",
            
                    "sex" : "sex",
            
                    "adress" : "adress",
            
                    "tel" : "tel",
            
                    "username" : "username",
            
                    "password" : "password",
            
                "add" : "add",
        "edit" : "edit",
	"addConfirm" : "addConfirm",
        "addCancel" : "addCancel"

      }
    },
    ch: {
      translation: {
                    "userid" : "用户ID",
                    "name" : "姓名",
                    "sex" : "性别",
                    "adress" : "地址",
                    "tel" : "电话",
                    "username" : "登录名",
                    "password" : "密码",
                "add" : "增加",
        "edit" : "编辑",
	"addConfirm":"确认",
        "addCancel" : "取消"
      }
    }
  }
}, function(err, t) {
//	jqueryI18next.init(i18next, $);
  // init set content
  //updateContent();
});
// 这一段也可以放在 统一的国际化信息里面
// 这一段也可以放在 统一的国际化信息里面
//此文件由马光dearmite模板生成,
//根据实际项目需要进行酌情删改.目前无版权要求.随意COPY
//有问题的话,请联系dearmite$126.com 8195819$qq.com 请不要发送垃圾邮件 前面的$改为@
//谢谢  end 

var settings = {
                 '#userid':  i18next.t('userid') ,
                 '#name':  i18next.t('name') ,
                 '#sex':  i18next.t('sex') ,
                 '#adress':  i18next.t('adress') ,
                 '#tel':  i18next.t('tel') ,
                 '#username':  i18next.t('username') ,
                 '#password':  i18next.t('password') ,
        	
}
for(var key in settings) {
	$(key).html(settings[key]);
}
//页面js

//添加
var add = {

    sysUser2 : {
        
                     
                               name : "" ,
                               sex : "" ,
                               adress : "" ,
                               tel : "" ,
                               username : "" ,
                               password : "" ,
                   

        userid : null
    },
    //初始化
    init : function () {
        
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#id").val("");

                         
                                  $("#name").val("");
           
                                      $("#sex").val("");
           
                                      $("#adress").val("");
           
                                      $("#tel").val("");
           
                                      $("#username").val("");
           
                                      $("#password").val("");
           
                    

    },
    //确认
    addConfirm : function () {
        debugger;

        add.sysUser2.userid = $("#id").val();
                         
                                  add.sysUser2.name = $("#name").val();
                                      add.sysUser2.sex = $("#sex").val();
                                      add.sysUser2.adress = $("#adress").val();
                                      add.sysUser2.tel = $("#tel").val();
                                      add.sysUser2.username = $("#username").val();
                                      add.sysUser2.password = $("#password").val();
                            
        $.ajax({
            url: addr + '/sysUser2/addSysUser2',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(add.sysUser2),
            success: function (data) {
                console.log('add = ' + data);
                 add.closeCurrentLayer();
                debugger;
                parent.location.reload();
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};


//删除
var del = {

};

//编辑
var edit = {
    sysUser2 : {
        
                     
                               name : "" ,
                               sex : "" ,
                               adress : "" ,
                               tel : "" ,
                               username : "" ,
                               password : "" ,
                   

        userid : null
    },
    //初始化
    init : function () {
        var userid  = getQueryString('userid');
        $.ajax({
            url: addr + '/sysUser2/getSysUser2?userid='+userid ,
            type: "get",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                edit.setInitData(data.data);
            }
        });
    },
    //页面赋值
    setInitData : function (data) {
        debugger;
        $("#userid").val(data.userid);

                         
                                  $("#name").val(data.name);
           
                                      $("#sex").val(data.sex);
           
                                      $("#adress").val(data.adress);
           
                                      $("#tel").val(data.tel);
           
                                      $("#username").val(data.username);
           
                                      $("#password").val(data.password);
           
                    

    },
    //编辑确认
    editConfirm : function () {
        debugger;

        edit.sysUser2.userid = $("#userid").val();
                         
                              edit.sysUser2.name = $("#name").val();
                                  edit.sysUser2.sex = $("#sex").val();
                                  edit.sysUser2.adress = $("#adress").val();
                                  edit.sysUser2.tel = $("#tel").val();
                                  edit.sysUser2.username = $("#username").val();
                                  edit.sysUser2.password = $("#password").val();
                            
        $.ajax({
            url: addr + '/sysUser2/updateSysUser2',
            type: "post",
            async: false,
            xhrFields: {withCredentials: true},
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data : JSON.stringify(edit.sysUser2),
            success: function (data) {
                console.log('update = ' + data);
                 edit.closeCurrentLayer();
                parent.layui.table.reload('sysUser2Table');
               
            }
        });
    },
    //关闭当前弹框
    closeCurrentLayer : function () {
        //当你在iframe页面关闭自身时
        var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
        parent.layer.close(index); //再执行关闭
    }
};

$(function () {
    $("#addConfirm").click(function () {
        debugger;
        add.addConfirm();

       
    });

    $("#addCancle").click(function () {
        add.closeCurrentLayer();
    });
    ////

    $("#editConfirm").click(function () {
        debugger;
        edit.editConfirm();

       
        
    });

    $("#editCancle").click(function () {
        edit.closeCurrentLayer();
    });


});


layui.use('form', function(){
    var form = layui.form;

    //自定义验证规则
    //form.verify({
    //    roleName: function(value){
    //        if(value.length < 4){
    //            return '请输入角色';
    //        }
    //    }
    //});

    //监听提交
    form.on('submit(editbtn)', function(data){
        debugger;

        edit.editConfirm();

        return false;

    });

    form.on('submit(addbtn)', function(data){
        debugger;

        add.addConfirm();

        return false;

    });
});

// edit.init();

