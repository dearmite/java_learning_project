/************************************************************************************************
 * by jutleo@gmail.com  2014-02-11
 * 自由编辑组件amd文件 
 * 实现思路：
 * 1、利用mvvm思路，对vm进行操作，本文件提供对应组件的接口，需要配合iss.js使用。
 * 2、此组件采用oo思路编写
 ************************************************************************************************/
define(function (){
	/**
	 * 定义到vm中的属性及方法，最终全部使用vm.x=x方式赋值到相应组件中
	 * 此函数作为类使用，防止对象化数据交叉影响
	 */
	 function CompsCore() {
	 	//控制全选
		this.checkedAll=false;
		//当前编辑的行索引
		this.editIndex=undefined;
		this.selectTriggleType='editer';
		this.editName="";
		this.dwName="";
    	this.dwType="";
    	//数据变量
    	this.datagrid = [];
		this.pageSize = 10;
		this.rowCount = 0;
		this.pageCount= 0;
		//是否显示行号列表
		this.isShowRowNum=true;
		this.pageNo = 0;
		this.isShowItems=false;
		this.isShowGird = false;
		this._selectData=[];
		this.selectDatas={};
		this.addRowData={};
		//单击事件
		this.inputClick = function(e, vm, name) {
			var val = this.value;
			vm[name] = val;
			vm[name+"_v"] = val;
			vm._status!="UNCHANGED"?(function(m){
				m._status = "INSERTED";
			})(vm):(function(m){
				m._status = "UPDATE";
			})(vm);
		};
		//下拉框下拉选项单击事件
		this.optionClick=function(e,id,obj,name,codelistname,type){
			if(!e)return;
			var vm=avalon.vmodels[id];
			var text=obj.childNodes[1].innerHTML;
        	var value=obj.childNodes[0].innerHTML;
			vm.datagrid[0][name]=text;
        	vm.datagrid[0][name+"_v"]=value;
        	vm.isShowItems=false;
        	if (window.event) { 
			 	e.cancelBubble=true; 
			} else { 
				 e.stopPropagation(); 
			}  
		}
		//显示下拉框事件
		this.showSelect=function(e,id,name,type,codelistName,triggerType){
			var X= this.getBoundingClientRect().left+document.documentElement.scrollLeft;
                     var Y =this.getBoundingClientRect().bottom+document.documentElement.scrollTop + document.body.scrollTop;
			var selectDiv=document.getElementById(id+"_selectItems");
                      selectDiv.style.top=Y+"px";
			selectDiv.style.left=X+"px";
			selectDiv.style.width=this.parentNode.clientWidth+"px";
			var vm=avalon.vmodels[id];
			triggerType=='filter'?vm.selectTriggleType='filter':vm.selectTriggleType='editer'
			vm.editName=name;
			vm._selectData=vm.selectDatas[name];//[{'value':'zg1','text':'中国1'},{'value':'mg1','text':'美国1'},{'value':'hg1','text':'韩国1'}];
			vm.isShowItems=true;
			if (window.event) { 
			 	e.cancelBubble=true; 
			} else { 
				 e.stopPropagation(); 
			}  
		};
		//控制组件的显示和隐藏
		this.openOrClose = function() {
			this.$vmodel.isShowGird = !this.$vmodel.isShowGird;
		};
		//控制全选，仅仅对于多选组件生效
		this.allCheckbox=function(id){
		   var vm=avalon.vmodels[id];
		   vm.checkedAll=!vm.checkedAll;
		   for(var i=0;i<vm.datagrid.length;i++){
			   vm.datagrid[i]._selected=vm.checkedAll;
		   }
		}
		//行双击
		this.dblclickRow=function(id){
		   var vm=avalon.vmodels[id];
		   vm.editIndex=this.$vmodel.$index;
		},
		//行单击
		this.clickRow=function(id){
		   var vm=avalon.vmodels[id];
		   if(this.$vmodel.$index!=vm.editIndex){
			   vm.editIndex=undefined;
			    this.$vmodel.el._selected=!this.$vmodel.el._selected;
		   }
		}
		//是否显示高级查询
		this.isShowAdvanceQuery = false;
		//显示高级查询
		this.showAdvanceQuery = function() {
			this.$vmodel.isShowAdvanceQuery = !this.$vmodel.isShowAdvanceQuery;
		};
		//下一页按钮
		this.pageNext=function(){
			var pageNo=this.$vmodel.pageNo;
			var pageCount=this.$vmodel.pageCount;
			if(pageNo<pageCount){
				this.$vmodel.pageNo=pageNo+1;
				eval("query_"+this.$vmodel.$id+"()");
				this.$vmodel.checkedAll=false;
			}
		};
		//上一页按钮
		this.pagePrev=function(){
			var pageNo=this.$vmodel.pageNo;
			if(pageNo>1){
				this.$vmodel.pageNo=pageNo-1;
				eval("query_"+this.$vmodel.$id+"()");
				this.$vmodel.checkedAll=false;
			}
		};
		//第一页按钮
		this.pageFirst=function(){
			var pageNo=this.$vmodel.pageNo;
			if(pageNo>1){
				this.$vmodel.pageNo=1;
				eval("query_"+this.$vmodel.$id+"()");
				this.$vmodel.checkedAll=false;
			}
		};
		//最后一页按钮
		this.pageLast=function(){
			var pageNo=this.$vmodel.pageNo;
			var pageCount=this.$vmodel.pageCount;
			if(pageNo<pageCount){
				this.$vmodel.pageNo=pageCount;
				eval("query_"+this.$vmodel.$id+"()");
				this.$vmodel.checkedAll=false;
			}
		}
    }
    /**
     * 组件对外提供的接口
     */
    var Interface={
    	//初始化方法，由主入口js调用，用来初始化vm
	  init : function (id,opts){
		  opts = avalon.mix(new CompsCore(),opts);
			opts.id=id;
			var div=document.getElementById(id);
			var vmodel=avalon.define(id, function(vm){
				for(var name in opts) {
				   vm[name] = opts[name];
				}
			});
			avalon.bindEvent("click",function(){
				vmodel.isShowItems=false;
			});
			avalon.scan(div,vmodel);
			return vmodel;
	  },
	  //添加行事件
        addRow:function(vm){
	        var length=vm.datagrid.length;
	        var copyData={};
	        avalon.mix(copyData,vm.$model.addRowData);
	        copyData['_index']=length;
			vm.datagrid.push(copyData);
		},
		//删除行事件
		delRow:function(vm,indexs){
			if(!indexs&&indexs!=0){
				return;
			}
			avalon.type(indexs)!='array'?indexs=[indexs]:'';
			for(var i=0;i<indexs.length;i++){
				var vgrid=vm.datagrid[indexs[i]];
				if(vgrid){
					vgrid['_selected']=false;
				    vgrid['_status']='DELETED';
				}
			}	
		},
		//删除选中行事件
		delSelectedRow:function(vm){
			var datagrid=vm.$model.datagrid;
	    	for(var i=0,l=datagrid.length;i<l;i++){
	    		var obj=datagrid[i];
	    		if(obj['_selected']){
	    			vm.datagrid[i]['_selected']=false;
	    			vm.datagrid[i]['_status']='DELETED';
	    	    }
	    	 }
		},
		//给组件重新赋予数据，并直接渲染组件dom结构
        setGridData:function(vm,data){
		  iss.useCheck=false;
		  vm.datagrid=data;
		  iss.useCheck=true;
       },
       /**
        * 获取组件的属性值
        * id: 组件的名称 一般为dwName_dwType
        * attrName:属性名称
        */
       getAttrValue:function(id,attrName){
    	   var datagrid=avalon.vmodels[id].datagrid;
    	   if(datagrid.length !=0) {
    	   	return datagrid[0][attrName];
    	   }
       },
       /**
        * 设置组件的属性值
        * id: 组件的名称 一般为dwName_dwType
        * attrName:属性名称
        * value： 需要设置的值
        */
       setAttrValue:function(id,attrName,value){
    	   var datagrid=avalon.vmodels[id].datagrid;
    	   if(datagrid.length !=0) {
    	   	datagrid[0][attrName] = value;
    	   	datagrid[0][attrName+"_v"] = value;
    	   }
       },
       /**
        * 获取选择行的索引
        * id: 组件的名称 一般为dwName_dwType
        * 返回一个数组，每个元素为索引号
        */
       getSelectedRowIndex:function(id){
    	   var vm=avalon.vmodels[id];
    	   var datagrid=vm.$model.datagrid;
	       var arr=[];
		   for(var i=0,l=datagrid.length;i<l;i++){
			   var obj=datagrid[i];
			   if(obj['_selected']){
				   arr.push(i);
			   }
		   }
	       return arr;
       }
    }
    return Interface;
});