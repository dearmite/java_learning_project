/************************************************************************************************
 * by jutleo@gmail.com  2014-02-11
 * 行编辑组件amd文件 
 * 实现思路：
 * 1、利用mvvm思路，对vm进行操作，本文件提供对应组件的接口，需要配合iss.js使用。
 * 2、此组件采用oo思路编写
 ************************************************************************************************/
define(function (){
	/**
	 * 定义到vm中的属性及方法，最终全部使用vm.x=x方式赋值到相应组件中
	 * 此函数作为类使用，防止对象化数据交叉影响
	 */
	 function CompsCore() {
		this.queryMethod='';         //filter 查询方法
		this.checkedAll=false;       //全选
		this.editIndex=undefined;    //当前编辑行,用于定位数据行 和填值
		this.selectTriggleType='editer'; //标记是filter 还是 行编辑 弹出的select下拉框
		this.editName="";
		this.dwName="";
    	this.dwType="";
    	this.datagrid = [];
		this.pageSize = 10;
		this.rowCount = 0;
		this.filter=[];
		this.pageCount= 0;
		this.pageNo = 0;
		this.isShowItems=false;  //select 下拉框是否显示
		this.isShowGird = false; //显示或隐藏组件
		this._selectData=[];     //当前下拉框下拉数据
		this.selectDatas={};     //所有下拉数据
		this.addRowData={};      //添加一行需要的vm模型
		this.inputClick = function(e, vm, name) {
			var val = this.value;
			vm[name] = val;
			vm[name+"_v"] = val;
			if(vm._status!="INSERTED") {
				vm._status = "UPDATED";
			}
		};
		this.optionClick=function(e,id,obj,name,codelistname,type){
			if(!e)return;
			var vm=avalon.vmodels[id];
			var text=obj.childNodes[1].innerHTML;
        	var value=obj.childNodes[0].innerHTML;
        	var index=vm.editIndex;
			if(vm.selectTriggleType=='filter'){
				vm.filter[name]=text;
        	    vm.filter[name+"_v"]=value;
			}else{
				vm.datagrid[index][name]=text;
				//grid控件header不考虑数组
				vm.datagrid[index][name+"_v"]=value;
				if(vm.datagrid[index]._status!="INSERTED") {
					vm.datagrid[index]._status = "UPDATED";
				}
			}
        	vm.isShowItems=false;
        	if (window.event) { 
			 	e.cancelBubble=true; 
			} else { 
				 e.stopPropagation(); 
			}  
		}
        this.showSelect=function(e,id,name,type,codelistName,triggleType){
			var X= this.getBoundingClientRect().left+document.documentElement.scrollLeft;
                     var Y =this.getBoundingClientRect().bottom+document.documentElement.scrollTop + document.body.scrollTop;
			var selectDiv=document.getElementById(id+"_selectItems");
                      selectDiv.style.top=Y+"px";
			selectDiv.style.left=X+"px";
			selectDiv.style.width=this.parentNode.clientWidth+"px";
			var vm=avalon.vmodels[id];
			triggleType=='filter'?vm.selectTriggleType='filter':vm.selectTriggleType='editer'
			vm.editName=name;
			vm._selectData=[];
			vm._selectData=vm.$model.selectDatas[name];//[{'value':'zg1','text':'中国1'},{'value':'mg1','text':'美国1'},{'value':'hg1','text':'韩国1'}];
			vm.isShowItems=true;
			if (window.event) { 
			 	e.cancelBubble=true; 
			} else { 
				 e.stopPropagation(); 
			}  
		};
		this.openOrClose = function() {
			this.$vmodel.isShowGird = !this.$vmodel.isShowGird;
		};
		this.allCheckbox=function(id){
		   var vm=avalon.vmodels[id];
		   vm.checkedAll=!vm.checkedAll;
		   for(var i=0;i<vm.datagrid.length;i++){
			   vm.datagrid[i]._selected=vm.checkedAll;
		   }
		}
		this.dblclickRow=function(id){
		   	iss.useCheck=false;
		   	var vm=avalon.vmodels[id];
		   //行双击之前
			try{vm.$events["beforeDBClicked"]();}catch(e){}
		   	vm.editIndex=this.$vmodel.$index;
		   	iss.useCheck=true;
		   	//行双击之后
		   	try{vm.$events["afterDBClicked"]();}catch(e){}
		},
		this.clickRow=function(id){
		   var vm=avalon.vmodels[id];
		   //行选择之前
		   	try{vm.$events["beforeSelected"]();}catch(e){}
		   if(!vm.editIndex){
			  this.$vmodel.el._selected=!this.$vmodel.el._selected;
		   }
		   if(this.$vmodel.$index!=vm.editIndex){
			   vm.editIndex=undefined;
		   } 
           	//行选择之后
		   	try{vm.$events["afterSelected"]();}catch(e){}
		}
		this.TextChanged=function(v){
			var id = this.getAttribute("text-id");
			//alert(this.name);
			//alert(this.value);
			//alert(this.type)
			//文本域
			var vm=avalon.vmodels[id];
			if("textarea"==this.type){
				alert("textarea")
				try{vm.$events["textarea"]();}catch(e){}
			}
			//文本框
			if("text"==this.type){
				alert("text")
				try{vm.$events["text"]();}catch(e){}
			}
		}
		this.isShowAdvanceQuery = false;
		this.showAdvanceQuery = function() {
			this.$vmodel.isShowAdvanceQuery = !this.$vmodel.isShowAdvanceQuery;
		};
		this.pageNext=function(){
			var pageNo=this.$vmodel.pageNo-0;
			var pageCount=this.$vmodel.pageCount-0;
			if(pageNo<pageCount){
				this.$vmodel.pageNo=pageNo-0+1;
				var method=this.$vmodel.queryMethod;
//				!method?method="query_"+this.$vmodel.$id+"()":'';
				eval(method);
				this.$vmodel.checkedAll=false;
			}
		};
		this.pagePrev=function(){
			var pageNo=this.$vmodel.pageNo-0;
			if(pageNo>1){
				this.$vmodel.pageNo=pageNo-1;
				var method=this.$vmodel.queryMethod;
//				!method?method="query_"+this.$vmodel.$id+"()":'';
				eval(method);
				this.$vmodel.checkedAll=false;
			}
		};
		this.pageFirst=function(){
			var pageNo=this.$vmodel.pageNo-0;
			if(pageNo>1){
				this.$vmodel.pageNo=1;
				var method=this.$vmodel.queryMethod;
//				!method?method="query_"+this.$vmodel.$id+"()":'';
				eval(method);
				this.$vmodel.checkedAll=false;
			}
		};
		this.pageLast=function(){
			var pageNo=this.$vmodel.pageNo-0;
			var pageCount=this.$vmodel.pageCount-0;
			if(pageNo<pageCount){
				this.$vmodel.pageNo=pageCount;
				var method=this.$vmodel.queryMethod;
//				!method?method="query_"+this.$vmodel.$id+"()":'';
				eval(method);
				this.$vmodel.checkedAll=false;
			}
		};
		this.goPageData = function(){
			var vmid = this.getAttribute("vmid"),v = this.value;
			var vm = avalon.vmodels[vmid];
			var pageNo = parseInt(vm.pageNo);
			if(isNaN(v) || v ===0){
				this.value = vm.pageNo;
				return;	
			} else {
				//合法数字
				vm.pageNo = parseInt(v);
				var method=vm.queryMethod;
				eval(method);
				vm.checkedAll=false;
			}
		};
    }
	/**
     * 组件对外提供的接口
     */
	var Interface={
		//初始化方法，由主入口js调用，用来初始化vm
	  	init : function (id,opts){
		  opts = avalon.mix(new CompsCore(),opts);
			opts.id=id;
			var div=document.getElementById(id);
			var vmodel=avalon.define(id, function(vm){
				for(var name in opts) {
				   vm[name] = opts[name];
				}
			});
			for(var i in opts.filter){
				(function(i){
					vmodel.filter.$watch(i, function(newValue, oldValue) {
			        var rst=iss.validate(newValue,vmodel.filter_checkType[i]);
			        if(rst!==true){
			        	if(oldValue&&iss.validate(oldValue,vmodel.filter_checkType[i])===true){
			        		vmodel.filter[i]=oldValue;
			        	}else{
			        		vmodel.filter[i]='';
			        	}
			        	iss.msgTip({
				            title : '数据格式不正确',
				            content : '请输入'+rst,
				            timeout:2000
     					});
			        }
    			  })
				})(i);
				
			}
			avalon.bindEvent("click",function(){
				vmodel.isShowItems=false;
			});
			avalon.scan(div,vmodel);
			return vmodel;
	  },
	  //添加行事件
        addRow:function(vm){
			iss.useCheck=false;
	        var length=vm.datagrid.length;
	        var copyData={};
	        avalon.mix(copyData,vm.$model.addRowData);
	        copyData['_index']=length;
	        copyData['_status']='INSERTED';
			vm.datagrid.push(copyData);
			vm.editIndex=length;
			iss.useCheck=true;
		},
		//删除行事件
		delRow:function(vm,indexs){
			if(!indexs&&indexs!=0){
				return;
			}
			avalon.type(indexs)!='array'?indexs=[indexs]:'';
			for(var i=0;i<indexs.length;i++){
				var vgrid=vm.datagrid[indexs[i]];
				if(vgrid){
					vgrid['_selected']=false;
				    vgrid['_status']='DELETED';
				}
			}	
		},
		//删除选中行事件
		delSelectedRow:function(vm){
			var datagrid=vm.$model.datagrid;
	    	for(var i=0,l=datagrid.length;i<l;i++){
	    		var obj=datagrid[i];
	    		if(obj['_selected']){
	    			vm.datagrid[i]['_selected']=false;
	    			vm.datagrid[i]['_status']='DELETED';
	    	    }
	    	 }
		},
		//给组件重新赋予数据，并直接渲染组件dom结构
        setGridData:function(vm,data){
		  iss.useCheck=false;
          vm.checkedAll=false;
    	  for(var i=0;i<data.length;i++){
			  data[i]['_selected']=false;
		  }
		  vm.datagrid=data;
		  data=null;
		  iss.useCheck=true;
       },
       /**
        * 获取组件的属性值
        * id: 组件的名称 一般为dwName_dwType
        * attrName:属性名称
        */
       getAttrValue:function(id,attrName,index){
    	   var vm=avalon.vmodels[id];
    	   if(avalon.type(vm.$model.datagrid)==="array"&&vm.$model.datagrid.length>0){
    		   if(index||index===0){
    		       return vm.$model.datagrid[index][attrName];
	    	   }else{
	    		   var datagrid=vm.$model.datagrid;
	    		   var arr=[];
	    		   for(var i=0,l=datagrid.length;i<l;i++){
	    			   var obj=datagrid[i];
	    			   if(obj['_selected']){
	    				   arr.push(obj[attrName]);
	    			   }
	    		   }
	    		   return arr;
	    	   }
    	   }
       },
       /**
        * 设置组件的属性值
        * id: 组件的名称 一般为dwName_dwType
        * attrName:属性名称
        * value： 需要设置的值
        */
       setAttrValue:function(id,attrName,value,index){
    	   var vm=avalon.vmodels[id];
    	   if(avalon.type(vm.datagrid)==="array"&&vm.datagrid.length>0){
    		   if(index||index===0){
    		        vm.datagrid[index][attrName]=value+'';
	    	   }else{
	    		   var datagrid=vm.datagrid;
	    		   var arr=[];
	    		   for(var i=0,l=datagrid.length;i<l;i++){
	    			   var obj=datagrid[i];
	    			   if(obj['_selected']){
	    				   obj[attrName]=value+'';
	    			   }
	    		   }
	    	   }
    	   }
       },
       /**
        * 获取选择行的索引
        * id: 组件的名称 一般为dwName_dwType
        * 返回一个数组，每个元素为索引号
        */
       getSelectedRowIndex:function(id){
    	   var vm=avalon.vmodels[id];
    	   var datagrid=vm.$model.datagrid;
	       var arr=[];
		   for(var i=0,l=datagrid.length;i<l;i++){
			   var obj=datagrid[i];
			   if(obj['_selected']){
				   arr.push(i);
			   }
		   }
	       return arr;
       }
    }
    return Interface;
});