define(function(){
	//获取dom对象的工具方法
	var $leo = function(id) {
		return document.getElementById(id);
	};
	
	//tab组件的tab单击事件
	function tabAction() {
		this.tabClick = function(id) {
			var tabs = avalon.vmodels[id].tabs;
			avalon.each(tabs, function(inx, val){
				val.actived = false;
			});
			this.$vmodel.tt.actived = true;
		};
	}
	//组件的dom字符串模版
	var outHTML = function(id, opts) {
		var tabHTML = 
		'<div class="leo-tab-head">' +
			'<ul ms-each-tt="tabs">' +
				'<li class="leo-li-default leo-tab-head-ul-li" ms-class-actived="tt.actived"><a ms-click="tabClick(\''+id+'\')" href="javascript:void(0)">{{tt.title}}</a></li>' +
			'</ul>' +
		'</div>' +
		'<div class="leo-tab-body" ms-each-tc="tabs">' +
	        '<div class="leo-tab-body-panel" ms-visible="tc.actived">{{tc.content | html}}</div>' +
		'</div>';
		
		return tabHTML;
	};
	
	//组件对象
	function Tab() {
	}
	//组件初始化方法
	Tab.prototype.init = function(id, opts) {
		var renderObj = $leo(id);
		renderObj.className = "leo-tab";
		renderObj.setAttribute("ms-controller", id);
		renderObj.innerHTML = outHTML(id, opts);
		
		opts = avalon.mix(new tabAction(),opts);
		
		//定义vm
		var vms = avalon.define(id, function(vm){
			for(var name in opts) {
			   vm[name] = opts[name];
			}
		});
		avalon.scan(renderObj, vms);
		return vms;
	};
	
	return new Tab();
});