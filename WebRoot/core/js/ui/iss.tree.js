define(function(){
	var $leo = function(id) {
		return document.getElementById(id);
	};
	
	//防止事件冒泡
	function stopBubble(e) {
	    if(e && e.stopPropagation) {
	        e.stopPropagation();
	    } else {
        	e.cancelBubble = true;
        }
    }
	
    //组件dom字符串模版
	function outHTML(id, opts) {
		var treeHTML = 
			'<script type="avalon" id="tmpl">' + 
	            '<ul ms-each-el="subtree">' + 
	            	'<li ms-click="nodeClick($event, el)"><span ms-class-folder="el.isFolder" ms-class-file="!el.isFolder">{{el.text}}</span><div ms-include="\'tmpl\'" ms-visible="el.showFlag" ></div></li>' + 
	            '</ul>' + 
	        '</script>' + 
			'<ul class="leo-tree" ms-each-el="tree">' +
	            '<li ms-click="nodeClick($event, el)"><span ms-class-folder="el.isFolder" ms-class-file="!el.isFolder">{{el.text}}</span><div ms-include="\'tmpl\'" ms-visible="el.showFlag"></div></li>' +
	        '</ul>';
		return treeHTML;
	}
	
	//树的节点单击事件
	function treeAction() {
		this.nodeClick = function(e, nodeVM) {
			if(this.$vmodel.el.subtree.length > 0) {
                this.$vmodel.el.showFlag = !this.$vmodel.el.showFlag;
            } else {
            	this.$vmodel.el.isFolder = false;
            }
            stopBubble(e); 
		}
		this.isFolder = true;
	}
	
	//树对象
	function Tree() {
	}
	//树初始化方法
	Tree.prototype.init = function(id, opts) {
		var renderObj = $leo(id);
		renderObj.setAttribute("ms-controller", id);
		renderObj.innerHTML = outHTML(id, opts);
		
		opts = avalon.mix(new treeAction(),opts);
		
		//定义vm
		var vms = avalon.define(id, function(vm){
			for(var name in opts) {
			   vm[name] = opts[name];
			}
		});
		avalon.scan(renderObj, vms);
		return vms;
	};
	
	return new Tree();
});