(function(exports){
	exports.issUI = exports.issUI?exports.issUI:{};
	exports.issUI.Tab = function(id, opts) {
		require("../ui/iss.tab,css!../../skin/default/iss.tab.css", function(issTab) {
			var tab = issTab.init(id, opts);
		});
	}
	exports.issUI.Tree = function(id, opts) {
		require("../ui/iss.tree,css!../../skin/default/iss.tree.css", function(issTree) {
			var tab = issTree.init(id, opts);
		});
	};
	exports.issUI.TreeList = function(id, opts) {
		require("../ui/iss.tree,css!../../skin/default/iss.treelist.css", function(issTree) {
			var tab = issTree.init(id, opts);
		});
	};
})(this);