//定义输出字段过滤器
avalon.filters.checkType = function(str,id,name,checkType,dwType){
	if(!iss.useCheck){
		return str;
	}
	var vm=avalon.vmodels[id];
	var index=dwType=='GRID_EDIT'?vm.editIndex:0;
	var obj=vm.datagrid[index];
	var reg,msg;
	var rst=iss.validate(str,checkType);
	if(rst!==true){
		    obj[name]="";
			obj[name+"_v"]="";
			str="";
	    	iss.msgTip({
	            title : '数据格式不正确',
	            content : '请输入'+rst,
	            timeout:2000
			});
	}
	return str;
}
avalon.mix({
	bindEvent:function(type,fn){
	    if(window.attachEvent) {
   			 document.attachEvent(type,fn);
		} else {
			document.addEventListener(type,fn,true);
		} 
	},
	//日期回填模型事件
	datepick:function(dp,id,name,type){
		var v=dp.cal.getNewDateStr();
		var vm=avalon.vmodels[id];
		if(type==='filter'){
			vm.filter[name+'_v']=v;
		}else if(type==='editer'){
			var index=vm.editIndex||0;
			vm.datagrid[index][name+'_v']=v;
			vm.datagrid[index][name]=v;
			vm.datagrid[index]._status="UPDATED";
		}
		return true;
	}
});


(function(win){
	// 前后端交互数据模型
	var JsonModel=function(){
		return { 
			modelType : "json",
			beanName : "",
			beanMethod : "",
			dwData : "<data></data>",
			custData: ""
	   }
	}
	//自定义数据模型
	var customerModel=function(){
		return{
			successFn : function(){},
			errorFn : function(){},
			isSendData:true, //是否发送页面模型数据到后台
			isSendFilter:true, //是否发送页面查询器数据到后台
			CUST_DATA:{} //自定义数据  格式{name:'123',age:'23'}
		}
	}
	//获取page相关工具类
	var avalonTool={
		getPageNo:function(id){
		   return avalon.vmodels[id].pageNo;
		},
		getPageSize:function(id){
			return avalon.vmodels[id].pageSize;
		},
		getPageCount:function(id){
			return avalon.vmodels[id].pageCount;
		},
		getRowCount:function(id){
			return avalon.vmodels[id].rowCount;
		}
	}
  //对外暴露的方法
   var Iss={
	  //初始化组件
	  init:function(id,opts,fn){
		  //debugger
		  if(REQUIRE_OBJ[opts.dwType]){
			  REQUIRE_OBJ[opts.dwType].init(id,opts);
		  }else{
			   require("../ui/iss."+opts.dwType+",css!../../skin/default/grid.css", function(gridObj) {
				  REQUIRE_OBJ[opts.dwType]=gridObj;
			      gridObj.init(id,opts);
	              if(typeof fn==='function'){
	                	fn();
	              }
               });
		  }
	  },
	  useCheck:true,
	  //提供校验规则
	  validate:function(str,checkType){
		  var reg,msg;
		  switch(checkType){        
				case 'Number':reg = /^([+-]?)\d*\.?\d+$/;msg="数字";break;
				case 'Integer':reg = /^-?[1-9]\d*$/; msg="整数";break;
				case 'SignlessInt':reg = /^[1-9]\d*$/; msg="正整数";break;
				case 'NegativeInt':reg = /^-[1-9]\d*$/;msg="负整数";break;
				case 'Double':reg = /^-?([1-9]\d*|0(?!\.0+$))\.\d+?$/;msg="小数";break;
				case 'SignlessDbl':reg = /^[1-9]\d*.\d*|0.\d*[1-9]\d*$/; msg="正小数";break;
				case 'NegativeDbl':reg = /^-([1-9]\d*.\d*|0.\d*[1-9]\d*)$/;msg="负小数";break;
				case 'NumberText':reg = /^'([+-]?)\d*\.?\d+'$|^"([+-]?)\d*\.?\d+"$/;msg="数字字符";break;
				case 'Letter':reg = /^[A-Za-z]+$/;msg="字母";break;
				case 'UpCase':reg = /^[A-Z]+$/; msg="大写字母";break;
				case 'LowerCase':reg = /^[a-z]+$/; msg="小写字母";break;
				case 'English':reg =/^[\x00-\xFF]+$/;msg="ACSII字符";break;
				case 'Chinese':reg =/^[\u4e00-\u9fa5|\s]+$/; msg="中文字符";break;
				case 'Email':reg=/\w+((-w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+/;msg="Email";break;
			    case 'Mobile':reg=/0?(13|14|15|18)[0-9]{9}/;msg="手机号码";break;
			    case 'Url':reg=/[a-zA-z]+:\/\/[^\s]+/;msg="网址";break;
			    case 'IDCard':reg =/(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;msg="身份证号码";break;
		  }
		    if(checkType=='Text'||(!str&&str!==0)){
		    	return true;
		    }
			if(reg&&!reg.test(str)){
				return msg;
			}
			return true;
	  },
	  /**
	   * @param {Object} id  from的id属性
	   * @param {Object} config 校验配置
	   * @return {TypeName} 
	   */
	  validateForm:function(id,config){
		  return $("#"+id).validate(config);
	  },
	  //GRID_EIDT 添加一行空数据
	  addRow:function(id){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  gridObj.addRow(avalon.vmodels[id]);
	  },
	  //显示或者隐藏组件
	  showOrHide:function(id) {
	  	avalon.vmodels[id].isShowGird = !avalon.vmodels[id].isShowGird;
		var x = 10;
	  },
	   
	   //GRID_EIDT 删除指定行数据
	   //@param index 类型[数组或整数] , index下标从0开始 如[0,3] 或 3
	  delRow:function(id,indexs){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  gridObj.delRow(avalon.vmodels[id],indexs);
	  },
	  
	  //删除选中的行
	  delSelectedRow:function(id){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  gridObj.delSelectedRow(avalon.vmodels[id]);
	  },
	  //更新vm数据模型
	  setDataGrid:function(id,data){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  gridObj.setGridData(avalon.vmodels[id],data);
	  },
	  /**
	   * 
	   * @param {Object} id  组件id
	   * @param {Object} beanName 后台actoin名称
	   * @param {Object} beanMethod 后台方法名称
	   * @param {Object} extraData 
	   * {  isSendData:true,     //是否发送页面模型数据到后台   如果只是查询数据可以设置为false
	   *	isSendFilter:true,   //是否发送页面查询器数据到后台 为false 不发送查询器数据到后台
	   *	custData:{}        //自定义数据标准json格式
	   * }
	   */
	  sendService:function(id,beanName,beanMethod,extraData){
		  window.D1=new Date-0;
		  var returnFlag=false;
		  var custModel=customerModel();
		  avalon.mix(custModel,extraData);
		  var json_data=JsonModel();
		  json_data.beanName=beanName;
		  json_data.beanMethod=beanMethod;
		  var vm = avalon.vmodels[id].$model;
		  var filter = {};
		  if(custModel.isSendFilter){
			for ( var key in vm.filter) {
				/^\w+_v$/.test(key) ? (function(k, v) {
					k = k.replace(/^(\w+)_v$/, "$1");
					vt=vm.filter[k];
				    v==''?v=vt:'';
				    if(avalon.type(v)==='array'){
				    	v=v.join(',');
				    }
				    filter[k] = {value: v};
				})(key, vm.filter[key]) : "";
		    }
		  }
		  var dataObjVoList = [], datas = vm.datagrid;
		  if(custModel.isSendData){
			for ( var i = 0, len = datas.length; i < len; i++) {
				var data = datas[i];
				var dataObjVo = {
					index : i + 1,
					selected : data._selected,
					status : data._status
				};
				var attrs = [];
				for ( var key in data) {
					/^\w+_v$/.test(key) ? (function(k, v) {
						k = k.replace(/^(\w+)_v$/, "$1");
						var vt=data[k];
						v==''?v=vt:'';
						if(avalon.type(v)==='array'){
				    	   v=v.join(',');
				        }
						attrs.push( {
							name : k,
							newValue : v,
							value : v
						});
					})(key, data[key]) : "";
				}
				dataObjVo.field = attrs;
				dataObjVoList.push(dataObjVo);
			}
		  }
		  //默认显示第0页，第一页加载数据时页码应该为1
		  vm.pageNo = parseInt(vm.pageNo)===0?vm.pageNo=1:vm.pageNo = vm.pageNo;
		  var dataModel = {
			isFilter : false,
			dwType : vm.dwType,
			dwName : vm.dwName,
			rowCount : vm.rowCount,
			pageSize : vm.pageSize,
			pageNo : vm.pageNo,
			pageCount : vm.pageCount,
			filter : filter,
			dataObj : dataObjVoList
		  };
		  json_data.dwData = [ dataModel ];// JSON.stringify( [ dataModel ]);
		  json_data.custData = custModel.CUST_DATA; //JSON.stringify(custModel.custData);
		  Util.request({
			async: true,
			model: json_data,
			onError: function(jqXHR, textStatus, data, sendError,excDtl) {
				//sendError && alert( data.exception );
			  	if(sendError){
				  //调出邮件窗口
			  		var uri="../exception/exp_mail.jsp?exceptionDetail="+excDtl;
			  		uri=encodeURI(encodeURI(uri));
			  		window.open(uri);
			  	}
				try{document.getElementById(id+"_data_tbody").style.display="none";}catch(e){};
				return false;
			},
			onSuccess: function(jqXHR, textStatus, data) {
				data&&data.custData?custData=data.custData:'';
				if(data.status === "success") {
					var d=new Date-0;
					avalon.each(data.dwData, function(inx1, v1) {
						var vm = avalon.vmodels[id];
						vm.pageCount = v1.pageCount;
						vm.pageNo = v1.pageNo;
						vm.pageSize = v1.pageSize;
						vm.rowCount = v1.rowCount;
						var datagrid = [];
						avalon.each(v1.dataObj, function(inx2, v2) {
							var tmpjson = {};
							tmpjson['_selected'] = v2.selected;
							tmpjson["_index"] = v2.index;
							tmpjson["_status"]=v2.status;
							avalon.each(v2.field, function(inx3, v3) {
								tmpjson[inx3] = v3.text;
								tmpjson[inx3 + "_v"] = v3.newValue;
							});
							datagrid.push(tmpjson);
						});
						var gridObj=getRequireObj(vm.dwType);
			            gridObj.setGridData(vm,datagrid);
			            avalon.log("总时间："+(new Date-0-window.D1));
					});
				    avalon.log("回填数据时间："+(new Date-0-d)+"ms");
				    //自定义回调
				    custModel.successFn(data);
				    try{
				    	document.getElementById(id+"_data_tbody").style.display="";
				    }catch(e){
				    }
				    returnFlag=true;
				    if(custData.DEBUG_FILE_NAME){
				    	iss.download(custData.DEBUG_FILE_NAME,custData.DEBUG_FILE_NAME,true);
				    }
				} else if(data.status === "ERROR") {
					//自定义回调
				    custModel.errorFn(data);
				    try{document.getElementById(id+"_data_tbody").style.display="none";}catch(e){};
				}
				
			}
		   });
		   setTimeout(function(){
			   	var ar = [],vmx = avalon.vmodels[id],pageSize = parseInt(vmx.pageSize)||10;
			   	if(vmx.datagrid.length === 0) {
			   		try{
			   			document.getElementById(id+"_data_tbody").style.display="none";
			   		}catch(e){}
				   	for(var i = 0; i < pageSize; i++) {
				   		var json_ = {};
				   		for(var j in vmx.addRowData.$model) {
				   			json_[j] = NaN;
				   		}
				   		ar.push(json_);
				   	}
				   	vmx.datagrid = ar;
			   	}
//			   	document.getElementById(id+"_data_tbody").style.display="";
		   }, 0);
		  return returnFlag;
	  },
	  //设置自定义事件
	  setEvents:function(id,eName, eFn) {
	  		var vm=avalon.vmodels[id];
	  		vm.$events[eName] = eFn;
//	  		beforeSelected : function(){},
//		  	afterSelected : function(){},
//		  	beforeDBClicked : function(){},
//		  	afterDBClicked : function(){};
	  },
	  //获取总共的行数
	  getRowCount:function(id){
    	   var vm=avalon.vmodels[id];
    	   if(avalon.type(vm.datagrid)==="array"){
    		  return vm.datagrid.length;
    	   }else{
    		   return 0;
    	   }
	  },
	  /**获取选中行的 行号 下标从0开始
	   * ONE_SELECT 返回数据
	   * MULTI_SELECT,GRID_EDIT 返回数组
	   * @param {Object} id
	   * @return {TypeName} 
	   */
	  getSelectedRowIndex:function(id){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  return  gridObj.getSelectedRowIndex(id);
	  },
	  //获取指定行指定字段的值
	  getAttrValue:function(id,attrName,index){
		    var gridObj=getRequireObj(avalon.vmodels[id].dwType);
			return  gridObj.getAttrValue(id,attrName,index);
	  },
	  //设置指定行指定字段的值
	  setAttrValue:function(id,attrName,value,index){
		  var gridObj=getRequireObj(avalon.vmodels[id].dwType);
		  gridObj.setAttrValue(id,attrName,value,index);
	  },
	  getCustData:function(){
		  return custData;
	  },
	  getJsonObj:function(){
		  return JSON_OBJ;
	  },
	  //日期控件
	  calendar:function(id,jsonDate){
		  jsonDate=jsonDate||{};
    	  $('#'+id).omCalendar(jsonDate);
	  },
	  //messageBox控件
	  msgBox:{
		  alert:function(config){
		     $.omMessageBox.alert(config);
		  },
		  confirm:function(config){
		   $.omMessageBox.confirm(config);
          },prompt:function(config){
    	   $.omMessageBox.prompt(config);
          }, 
          waiting:function(config,timeout){
    	   timeout=timeout||3000;
    	   $.omMessageBox.waiting(config);
           setTimeout("$.omMessageBox.waiting('close');",timeout);
         }
	  },
	  //模态窗口
	  dialog:function(id,config){
		  $("#"+id).omDialog(config);
	  },
	  //右下角消息提示
	  msgTip:function(config,win){
		 win=win||window;
		 win.$.omMessageTip.show(config);
	  },
	  upload:function(id,config){
		Util.uploadFile(id,config);  
	  },
	  download:function(storeName,realName,debug){
		Util.downloadFile(storeName,realName,debug);
	  },
	  //下拉
	  select:function(id,config){
		   $('#'+id).omCombo(config);
	  },
	  //联动
	  combo:function(combos){
		for(var i=0,l=combos.length;i<l;i++){
			obj=combos[i];
			var valueField=obj.valueField||'value';
			var optionField=obj.optionField||'text';
			var dataSource=(i==0?obj.dataSource:undefined);
			if(i<l-1){
				$('#'+obj.id).omCombo({
					dataSource:dataSource,
				    valueField:valueField,
	                optionField:optionField,
	                onValueChange :(function(i,curobj){
	                   return function(target, newValue, oldValue) {
	                   $('#'+combos[i+1].id).val('').omCombo('setData', curobj.dataSource+'?'+curobj.paramName+'='+newValue);
	                   for(var j=i+1;j<l;j++){
	                	     $('#'+combos[j].id).val('').omCombo( 'setData', []);
	                   }
	                }
	                })(i,obj) 
				});
			}else{
				$('#'+obj.id).omCombo({
					dataSource:dataSource,
				    valueField:valueField,
	                optionField:optionField
				});
			}
		}
	  },
	  //对应元素下面所有的input|select|textarea
	  resetValue:function(id){
		  $("#"+id).find("input,select,textarea").each(function(){
			 $(this).val("");
			 if($(this).attr("type")=="radio"){
				 $(this).attr("checked");
				 $(this).removeAttr("checked");
			 }
			 if($(this)[0].tagName.toUpperCase() == "TEXTAREA"){
				 $(this).html("");
			 }
			 if($(this)[0].tagName.toUpperCase() == "SELECT"){
				 //$(this).val("");
				 var ops = $(this).find("option");
				 for ( var i = 0; i < ops.length; i++) {
					if($(ops[i]).attr("selected")=="selected"){
						$(this).val($(ops[i]).val());
						break;
					}
				}
			 }
		  });
	  }
  }

  //缓存require的组件GRIDOBJ到 如one_select 组件，避免每次都require
  var REQUIRE_OBJ={};
  //后台返回的自定义数据
  var custData={};
  //后台返回的json自定义数据
  var JSON_OBJ={};
  //获取组件
  function getRequireObj(dwType){
	  return REQUIRE_OBJ[dwType];
  }
  var BUI = function(){
  	this.iss = Iss;
  };
  
  win.iss=new BUI().iss;
})(window)