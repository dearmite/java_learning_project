(function(exports, mvvm) {
	var leo = {
		version : "0.1"
	};

	String.prototype.trim = function() {
        return this.replace(/^[\s\xA0]+/, "").replace(/[\s\xA0]+$/, '')
    };

    mvvm.filters.exchangeValue = function() {
    	var x = 10;
    	var y = 10;
    }

    function CompsCore() {
    	this.datagrid = [];
		this.pageSize = 12;
		this.rowCount = 293;
		this.pageCount= 23;
		this.pageNo = 1;
		this.isShowGird = false;
		this.openOrClose = function() {
			this.$vmodel.isShowGird = !this.$vmodel.isShowGird;
		};
		this.isShowAdvanceQuery = false;
		this.showAdvanceQuery = function() {
			this.$vmodel.isShowAdvanceQuery = !this.$vmodel.isShowAdvanceQuery;
		};
		this.clickRow = function() {
			this.$vmodel.el.selected = !this.$vmodel.el.selected;
    	};
    }

    var comps = {};
    comps.ONE_SELECT = {
		clickRow : function(dwName, dwType) {
			mvvm.each(mvvm.vmodels[dwName+"_"+dwType].datagrid, function(inx, value){
				value.selected = false;
			});
			this.$vmodel.el.selected = !this.$vmodel.el.selected;
		}
    };
    comps.GRID_EDIT = {
    	clickRow : function(dwName, dwType) {
    		mvvm.each(mvvm.vmodels[dwName+"_"+dwType].datagrid, function(inx, value){
				value.selected = false;
			});
			this.$vmodel.el.selected = !this.$vmodel.el.selected;
    	}
    };

	(function(jInit){
		jInit.init = function(id, type, opts){
			id = id + "_" + type;
			opts = this.extend(this.extend(new CompsCore(), comps[type]), opts);
			mvvm.define(id, function(vm){
				for(var name in opts) {
					vm[name] = opts[name];
				}
				// vm.$skipArray = ["datagrid"];
				vm.$watch("queryer", function(n, o){
					console.log(n + " ~ " + o);
				});
			});
		};
	})(leo);

	(function(jUtils){
		jUtils.extend = function(target, options, deep){
			target = target || {};
			for(var key in options) {
				var copy = options[key];
				if(deep && copy instanceof Array) {
					target[key] = this.extend([], copy, deep);
				} else if(deep && copy instanceof Object) {
					target[key] = this.extend({}, copy, deep);
				} else {
					target[key] = options[key];
				}
			}
			return target;
		};
		jUtils.string2JSON = function(data) {
			var rvalidchars = /^[\],:{}\s]*$/,
	            rvalidescape = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
	            rvalidtokens = /"[^"\\\r\n]*"|true|false|null|-?(?:\d+\.|)\d+(?:[eE][+-]?\d+|)/g,
	            rvalidbraces = /(?:^|:|,)(?:\s*\[)+/g;

			if (typeof data === "string") {
				data = data.trim();
			}
			if (window.JSON && JSON.parse) {
                return JSON.parse(data)
            }
            if (rvalidchars.test(data.replace(rvalidescape, "@").replace(rvalidtokens, "]").replace(rvalidbraces, ""))) {
                //使用new Function生成一个JSON对象
                return (new Function("return " + data))();
            }
		};
		jUtils.json2String = function(obj) {
			var S = [];
			var J = "";
			if (Object.prototype.toString.apply(obj) === "[object Array]") {
				for (var i = 0; i < obj.length; i++) {
					S.push(this.json2String(obj[i]));
				}
				J = "[" + S.join(",") + "]";
			} else if (Object.prototype.toString.apply(obj) === '[object Date]') {
				J = "new Date(" + obj.getTime() + ")";
			} else if (Object.prototype.toString.apply(obj) === '[object RegExp]'
					|| Object.prototype.toString.apply(obj) === '[object Function]') {
				J = obj.toString();
			} else if (Object.prototype.toString.apply(obj) === '[object Object]') {
				for (var i in obj) {
					obj[i] = typeof(obj[i]) == 'string'
							? '"' + obj[i] + '"'
							: (typeof(obj[i]) === 'object' ? this.json2String(obj[i]) : obj[i]);
					S.push('"' + i + '":' + obj[i]);
				}
				J = '{' + S.join(',') + '}';
			}
			return J;
		};
	})(leo);
	
	(function(jAjax){
		var ajaxConverters = {
			text : function(data) {
				return text || "";
			},
			json : function(data) {
				return jAjax.string2JSON(data);
			}
		};

		var processData = function(data){
			mvvm.each(data.DW_DATA, function(inx1, v1){
				//vm name is dwName_dwType
				var vm = mvvm.vmodels[v1.dwName + "_" + v1.dwType];
				vm.pageCount = v1.pageCount;
				vm.pageNo = v1.pageNo;
				vm.pageSize = v1.pageSize;
				vm.rowCount = v1.rowCount;
				var datagrid = [];
				mvvm.each(v1.dataObjVoList, function(inx2, v2){
					var tmpjson={};
					tmpjson['selected'] = v2.selected;
					tmpjson["index"] = v2.index;
					mvvm.each(v2.attributeVoList, function(inx3, v3){
						tmpjson[inx3] = v3.text;
						tmpjson[inx3+"_leo"] = v3.newValue;
					});
					datagrid.push(tmpjson);
				});
				vm.datagrid = datagrid;
			});
		};
		
		jAjax.ajax = function(opts) {
			opts = this.extend({
				type:"post",
				url : "/v7/actionservice.ai",
				dataType:"json",
				isAsync:false,
				data: {
					ADAPTER_TYPE : "JSON_TYPE",
					SERVICE_TYPE : "ACTION_SERVIC",
					CODE_TYPE : "CODE_TYPE",
					BEAN_HANDLE : "baseAction",
					ACTION_HANDLE : "perform",
					SERVICE_NAME : "dwComponentDataTestAction",
					SERVICE_MOTHOD : "getTaskList",
					DW_DATA : opts.requestData,
					HELPCONTROLMETHOD : "common",
					SCENE : "UNDEFINED",
					BIZ_SYNCH_CONTINUE : "false"
				}
			}, opts);
			var xhr = false;
			if(window.XMLHttpRequest) {
				//w3c
				xhr = new XMLHttpRequest();
			} else if(window.ActiveXObject) {
				try {
					//new ie
					xhr = new ActiveXObject("Msxml2.XMLHTTP");
				} catch(error1) {
					try {
						//old ie
						xhr = new new ActiveXobject("Microsoft.XMLHttp");
					} catch(error2) {
						xhr = false;
					}
				}
			}
			xhr.onreadystatechange = function() {
				if(xhr.readyState === 4) {
					if(xhr.status === 200) {
						var jsonDataModel = ajaxConverters[opts.dataType](xhr.responseText);
						processData(jsonDataModel);
						if(opts.success) {
							opts.success(jsonDataModel);
						}
						if(opts.error) {
							opts.error(xhr.status);
						}
					}
				}
			};
			var data = "";
			if(typeof opts.data === "string") {
				data = opts.data;
				if(data.charAt(0) === "?") {
					data = data.substring(1, data.length);
				}
			} else if(typeof opts.data === "object") {
				for(key in opts.data) {
					data += key + "=" + opts.data[key] + "&";
				}
				data = data.replace(/&$/,"");
			}

			if(opts.type === "get") {
				xhr.open("get", opts.url + "?" + data, opts.isAsync);
				xhr.send();
			}
			if(opts.type === "post") {
				xhr.open("post", opts.url, opts.isAsync);
				xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
				xhr.send(data);
			}
		};
	})(leo);

	(function(jAPI){
		jAPI.sendXmlByService = function(dws, action, method, cust) {
			var vm = mvvm.vmodels["common.work_list_DW_GRID_EDIT"].$model;;
			var filterMapList = [];
			for(var key in vm.fieldOrFilter) {
				/^\D+_leo$/.test(key) ? (function(k, v){
					k = k.replace(/^(\D+)_leo$/,"$1");
					filterMapList.push({name:k,defaultValue:v});
				})(key, vm.fieldOrFilter[key]) : "";
			}

			var dataObjVoList = [],datas = vm.datagrid;
			for(var i = 0, len = datas.length; i < len; i++) {
				var data = datas[i];
				var dataObjVo = {index : i+1, selected : data.selected, status : data.status};
				var attrs = [];
				for(var key in data) {
					/^\D+_leo&/.test(key) ? (function(k, v){
						k = k.replace(/^(\D+)_leo$/,"$1");
						attrs.push({name:k, value:v});
					})(key, data[key]) : "";
				}
				dataObjVo.attributeVoList = attrs;
				dataObjVoList.push(dataObjVo);
			}
			var dataModel = {isFilter : false, dwType : "GRID_EDIT", dwName : "common.work_list_DW",
					rowCount : vm.rowCount, pageSize : vm.pageSize, pageNo : vm.pageNo, pageCount : vm.pageCount,
				filterMapList : filterMapList, dataObjVoList : dataObjVoList};

			var requestData = this.json2String([dataModel]);

			this.ajax({requestData:requestData});
		};
	})(leo);

	exports.leo = leo;
	
})(this,avalon);

//test ajax

var opts = {};
opts.type = "post";	

leo.testAjax = function(sync) {
	leo.sendXmlByService();
	opts["requestData"] = encodeURIComponent("[{\"isFilter\":false,\"dwType\":\"ONE_SELECT\",\"dwName\":\"common.work_list_DW\",\"rowCount\":\"0\",\"pageSize\":\"15\",\"pageNo\":\"1\",\"pageCount\":\"1\",\"maxCount\":\"1000\",\"filterMapList\":[{\"index\":\"1\",\"selected\":\"true\",\"status\":\"UNCHANGED\",\"attributeVoList\":{\"CRptNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CLcnNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CInsNme\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CTaskType\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CStatus0\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"未接受\"},\"CStatus1\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"已接收\"},\"CStatus3\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"暂存\"},\"CStatus2\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"已完成\"},\"CStatus4\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"改派\"},\"CStatus5\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"终止\"},\"TAdtStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TAdtEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyDptCde\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CVhlFrm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CEngNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CIDCard\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"}}}],\"dataObjVoList\":[]}]");
	opts.isAsync = sync;
	// leo.ajax(opts);
	
	opts["requestData"] = encodeURIComponent("[{\"isFilter\":false,\"dwType\":\"MULTI_SELECT\",\"dwName\":\"common.work_list_DW\",\"rowCount\":\"0\",\"pageSize\":\"10\",\"pageNo\":\"1\",\"pageCount\":\"1\",\"maxCount\":\"1000\",\"filterMapList\":[{\"index\":\"1\",\"selected\":\"true\",\"status\":\"UNCHANGED\",\"attributeVoList\":{\"CRptNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CLcnNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CInsNme\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CTaskType\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CStatus0\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"未接受\"},\"CStatus1\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"已接收\"},\"CStatus3\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"暂存\"},\"CStatus2\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"已完成\"},\"CStatus4\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"改派\"},\"CStatus5\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"终止\"},\"TAdtStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TAdtEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyDptCde\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CVhlFrm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CEngNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CIDCard\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"}}}],\"dataObjVoList\":[]}]");
	// leo.ajax(opts);

	opts["requestData"] = encodeURIComponent("[{\"isFilter\":false,\"dwType\":\"GRID_EDIT\",\"dwName\":\"common.work_list_DW\",\"rowCount\":\"0\",\"pageSize\":\"10\",\"pageNo\":\"1\",\"pageCount\":\"1\",\"maxCount\":\"1000\",\"filterMapList\":[{\"index\":\"1\",\"selected\":\"true\",\"status\":\"UNCHANGED\",\"attributeVoList\":{\"CRptNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CLcnNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CInsNme\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CTaskType\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TRptEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CStatus0\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"未接受\"},\"CStatus1\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"已接收\"},\"CStatus3\":{\"newValue\":\"1\",\"value\":\"1\",\"bakValue\":\"1\",\"text\":\"暂存\"},\"CStatus2\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"已完成\"},\"CStatus4\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"改派\"},\"CStatus5\":{\"newValue\":\"0\",\"value\":\"0\",\"bakValue\":\"0\",\"text\":\"终止\"},\"TAdtStartTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"TAdtEndTm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CPlyDptCde\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CVhlFrm\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CEngNo\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"},\"CIDCard\":{\"newValue\":\"\",\"value\":\"\",\"bakValue\":\"\",\"text\":\"\"}}}],\"dataObjVoList\":[]}]");
	// leo.ajax(opts);
}

//other test
// var username = "jut";
// var leo_obj = {
// 	username : "leo",
// 	getUsername : function() {
// 		return function() {
// 			return this.username;
// 		}
// 	}
// }

// console.log(leo_obj.getUsername()());

// var username = "jut";
// var leo_obj = {
// 	username : "leo",
// 	getUsername : function() {
// 		var that = this;
// 		return function() {
// 			return that.username;
// 		}
// 	}
// }

// console.log(leo_obj.getUsername()());